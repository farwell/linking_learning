<?php

use Illuminate\Support\Facades\Route;

// Register Twill routes here eg.
// Route::module('posts');

Route::group(['prefix' => 'content'], function(){
    Route::module('pages');
    Route::module('events');
    Route::module('testimonials');
    Route::module('clients');
    Route::module('blogs');
    Route::module('socialmedia');
    Route::module('projecttopics');
   
});

Route::group(['prefix' => 'resources'], function(){
    
    Route::module('resources');
    Route::module('resourceTypes');
    Route::module('resourceThemes');
   
});

Route::group(['prefix' => 'sessions'], function(){
    Route::module('sessions');
    Route::module('sessionResources');
    
});

Route::group(['prefix' => 'groups'], function(){
    Route::module('groups');
    Route::module('groupTypes');
    Route::module('groupFormats');
    Route::module('groupThemes');
    
});

Route::group(['prefix' => 'forums'], function(){
    Route::module('forums');
    Route::module('forumTopics');
    
});


 Route::get('/courses', 'CoursesController@index')->name('courses.index');
 Route::get('/courses/create', 'CoursesController@create')->name('courses.create');
 Route::post('/courses','CoursesController@store')->name('courses.store');
 Route::get('/courses/{course}/edit', 'CoursesController@edit')->name('courses.edit');
 Route::post('/courses/bulk-destroy','CoursesController@bulkDestroy')->name('courses.bulk-destroy');
 Route::post('/courses/{course}','CoursesController@update')->name('courses.update');
 Route::delete('/courses/{course}', 'CoursesController@destroy')->name('courses.destroy');

 
 Route::get('/user-licenses','UserLicensesController@index')->name('user-licenses.index');
 Route::get('/user-licenses/create','UserLicensesController@create')->name('user-licenses.create');
 Route::post('/user-licenses','UserLicensesController@store')->name('user-licenses.store');
 Route::get('/user-licenses/{userLicense}/edit','UserLicensesController@edit')->name('user-licenses.edit');
 Route::post('/user-licenses/bulk-destroy','UserLicensesController@bulkDestroy')->name('user-licenses.bulk-destroy');
 Route::post('/user-licenses/{userLicense}','UserLicensesController@update')->name('user-licenses.update');
 Route::delete('/user-licenses/{userLicense}','UserLicensesController@destroy')->name('user-licenses.destroy');
// //  Route::name('about.overview')->get('overview', 'PageController@overview');
//  Route::get('/{course}/edit', 'CourseController@edit')->where(['course' => '[a-z]+'])->name('courses.edit');

Route::get('/sessionmeetings','SessionMeetingsController@index')->name('sessionmeetings.index');
Route::get('/sessionmeetings/create','SessionMeetingsController@create')->name('sessionmeetings.create');
Route::post('/sessionmeetings','SessionMeetingsController@store')->name('sessionmeetings.store');
Route::get('/sessionmeetings/{sessionMeeting}/edit','SessionMeetingsController@edit')->name('sessionmeetings.edit');
Route::post('/sessionmeetings/bulk-destroy','SessionMeetingsController@bulkDestroy')->name('sessionmeetings.bulk-destroy');
Route::post('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@update')->name('sessionmeetings.update');
Route::delete('/sessionmeetings/{sessionMeeting}','SessionMeetingsController@destroy')->name('sessionmeetings.destroy');