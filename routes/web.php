<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\SessionController;
use App\Http\Controllers\Admin\SessionResourceController;
use App\Http\Controllers\Front\PageController;
use App\Http\Controllers\Front\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Front\CourseController;
use App\Http\Controllers\Front\UserLicenseController;
use App\Http\Controllers\Front\CommentController;
use App\Http\Controllers\Front\BlogCommentController;
use App\Http\Controllers\Front\GroupController;
use App\Http\Controllers\Front\ConnectController;
use App\Http\Controllers\Front\UserActivityReactionsController;
use App\Http\Controllers\Front\UserMessagesController;
use App\Http\Controllers\Front\ForumController;
use App\Http\Controllers\Front\NotificationsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'index'])->name('home');
Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/faqs', [PageController::class, 'faqs'])->name('faqs');



Route::post('user/create', [UserController::class, 'create'])->name('user.create');
Route::get('/user/verify', [UserController::class, 'verifyForm'])->name('user.verify');
Route::post('/user/verification', [UserController::class, 'verify'])->name('user.verification');
Route::post('/user/login', [LoginController::class, 'login'])->name('user.login');
Route::get('/user/resend', [UserController::class, 'resendCode'])->name('resend.code');
Route::post('/user/resend', [UserController::class, 'resendStore'])->name('resend.store');



Auth::routes(['verify' => true]);

Route::group(['prefix'=>'profile', 'middleware'=>['auth', 'verified']], function () {
    Route::get('/sessions', [PageController::class, 'sessions'])->name('sessions');
    Route::get('/session-details/{id}', [PageController::class, 'sessionDetail'])->name('session.details');
    Route::post('/session/edit/{resourceDetail}', [PageController::class, 'updateSession'])->name('session.update');
    Route::post('/session/delete/{id}', [PageController::class, 'deleteResource'])->name('session.delete');

    Route::get('/session-calendar/{id}', [PageController::class, 'sessionCalendar'])->name('session.calendar');

    Route::any('/session/resource/', [PageController::class, 'sessionResource'])->name('session.resource');

    Route::get('/posts', [CommentController::class,'posts'])->name('posts');
    Route::post('comments', [CommentController::class,'storeComment'])->name('comment.store');
    Route::post('loadcomments', [CommentController::class,'loadComment'])->name('comment.load');
    Route::post('replies', [CommentController::class,'storeReplies'])->name('replies.store');
    Route::get('repliesview', [CommentController::class,'viewReplies'])->name('replies.view');
    //likes routes Like Or Dislike
    // Like Or Dislike
    Route::post('save-likedislike', [CommentController::class,'save_likedislike'])->name('save_likedislike');

    Route::get('/course/detail/{slug}', [CourseController::class, 'detail'])->name('course.detail');
    Route::get('/courses/enroll/{id}', [CourseController::class, 'enroll'])->name('course.enroll');
    Route::any('/resources', [PageController::class, 'resources'])->name('resources');
    Route::get('/resources/article/{id}', [PageController::class, 'resourceArticle'])->name('resource.article');
    Route::any('/resources/participants', [PageController::class, 'participantsUpload'])->name('resource.participants');
    Route::post('/resources/rating', [PageController::class, 'resourcesRating'])->name('rate.resource');
    Route::get('/resources-details/{id}', [PageController::class, 'resourceDetails'])->name('resource.details');


    Route::any('/events', [PageController::class, 'events'])->name('events');
    Route::get('/events-details/{id}', [PageController::class, 'eventDetails'])->name('event.details');


    Route::get('/blogs', [PageController::class, 'blogs'])->name('blogs');
    Route::get('/blog-details/{id}', [PageController::class, 'blogDetails'])->name('blog.details');
    Route::post('/blogs/create', [PageController::class, 'uploadBlog'])->name('blog.create');



    Route::post('blog_comments', [BlogCommentController::class,'storeComment'])->name('blog.comment.store');
    Route::post('blog_loadcomments', [BlogCommentController::class,'loadComment'])->name('blog.comment.load');
    Route::post('blog_replies', [BlogCommentController::class,'storeReplies'])->name('blog.replies.store');
    Route::get('blog_repliesview', [BlogCommentController::class,'viewReplies'])->name('blog.replies.view');
    Route::post('blog_save-likedislike', [BlogCommentController::class,'save_likedislike'])->name('blog.save_likedislike');


    Route::any('community', [UserLicenseController::class, 'community'])->name('community');
    Route::any('all-members', [UserLicenseController::class, 'members'])->name('members');
    Route::get('/community/participants', [UserLicenseController::class, 'communityParticipants'])->name('community.participants');
    Route::get('/community/countries', [UserLicenseController::class, 'communityCountries'])->name('community.countries');
    Route::any('/community/{name}', [UserLicenseController::class, 'communityCountry'])->name('myCommunity');


    Route::any('/my-profile', [UserLicenseController::class, 'profile'])->name('profile.courses');
    Route::any('/achievements', [UserLicenseController::class, 'achievements'])->name('profile.achievements');
    Route::any('/myCommunity/follow/{follow}/{id}/', [UserLicenseController::class, 'myFollow'])->name('profile.myFollow');
    Route::any('/myCommunity/followRequests/', [UserLicenseController::class, 'myFollowRequests'])->name('profile.myFollowRequests');
    Route::any('/myCommunity/accept/{id}/{follow}', [UserLicenseController::class, 'myFollowAccept'])->name('profile.myFollowAccept');


    Route::any('/trainingFeed', [UserLicenseController::class, 'trainingFeed'])->name('profile.trainingFeed');
    Route::any('/like/{follow}/{id}/', [UserLicenseController::class, 'myLike'])->name('profile.myLike');
    Route::post('/comment', [UserLicenseController::class, 'myComment'])->name('profile.myComment');
    Route::get('/comments/{follow}/{course}/', [UserLicenseController::class, 'myComments'])->name('profile.myComments');
    Route::get('/subCommentCount/{follow}/{course}/{id}/', [UserLicenseController::class, 'subCommentCount'])->name('profile.subCommentCount');
    Route::get('/subComment/{follow}/{course}/{id}/', [UserLicenseController::class, 'subComment'])->name('profile.subComment');
    Route::any('/course/evaluate/{courseid}', [UserLicenseController::class, 'evaluate'])->name('course.evaluate');
    Route::any('/course/certificate/{id}', [UserLicenseController::class, 'certificate'])->name('course.certificate');
    Route::any('/edit', [UserController::class, 'profile'])->name('profile.edit');
    Route::any('/password/set', [UserController::class, 'password'])->name('profile.password.set');
    Route::any('/ppic/change', [UserController::class, 'picture'])->name('profile.ppic.change');

    Route::any('/session/certificate/{id}', [UserLicenseController::class, 'certificate'])->name('session.certificate');
 
    Route::post('/connect/profile/create', [ConnectController::class, 'createAtivity'])->name('activity.profile.create');

    Route::any('/connect', [ConnectController::class, 'activity'])->name('connect.activity');
    Route::post('/connect/create', [ConnectController::class, 'createAcivity'])->name('activity.create');
  
    Route::post('/connect/edit/{userActivity}', [ConnectController::class, 'updateActivity'])->name('connect.update');
    Route::post('/connect/delete/{userActivity}', [ConnectController::class, 'deleteActivity'])->name('activity.delete');
    Route::post('/connect/reply/create/{userActivity}', [ConnectController::class, 'createAcivityReply'])->name('reply.create');
    Route::post('/connect/reactions/{activityId}', [UserActivityReactionsController::class, 'reaction'])->name('reactions.create');
    Route::post('/connect/reply/reply/create/{userActivity}/{userActivityReply}', [UserActivityReactionsController::class, 'replyOnActionReply'])->name('reply.reply.create');
    Route::post('/connect/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [UserActivityReactionsController::class, 'reactOnReply'])->name('react.reply.create');

    Route::post('/messages/create', [UserMessagesController::class, 'sentMessage'])->name('message.create');
    Route::get('/messages/all', [UserMessagesController::class, 'index'])->name('message.index');
    Route::post('/messages/users/search/{name?}', [UserMessagesController::class, 'searchForUser'])->name('message.search');
    Route::get('/messages/chat/{user}', [UserMessagesController::class, 'getUserChat'])->name('message.chat');
    Route::post('/messages/delete/{userMessage}', [UserMessagesController::class, 'deleteMessage'])->name('message.delete');
    Route::post('/messages/convo/delete/{convo}', [UserMessagesController::class, 'deleteConvo'])->name('convo.delete');

    Route::any('/connect-profile/{id}', [ConnectController::class, 'profile'])->name('connect.profile');
    Route::any('/connect-profile', [ConnectController::class, 'profile'])->name('connect.my_profile');


    
    Route::any('/forum', [ForumController::class, 'index'])->name('connect.forum');
    Route::any('/connect/forum/topic', [ForumController::class, 'storeTopic'])->name('forum.topic.store');
    Route::any('/connect/forum/create', [ForumController::class, 'store'])->name('forum.store');

    Route::any('/connect/forum/follow/{id}', [ForumController::class, 'follow'])->name('forum.follow');
    Route::any('/connect/forum/{id}', [ForumController::class, 'details'])->name('forum.details');
    Route::post('/connect/forum/edit/{userActivity}', [ForumController::class, 'updateForumActivity'])->name('connect.forum.update');
    Route::post('/connect/forum/delete/{userActivity}', [ForumController::class, 'deleteForumActivity'])->name('connect.forum.delete');
    Route::post('/connect/forum/reply/create/{userActivity}', [ForumController::class, 'createAcivityReply'])->name('forum.reply.create');
    Route::post('/connect/forum/reactions/{activityId}', [ForumController::class, 'reaction'])->name('forum.reactions.create');
    Route::post('/connect/forum/reply/edit/{userActivity}', [ForumController::class, 'updateForumReplyActivity'])->name('connect.forum.reply.update');
    Route::post('/connect/forum//reply/delete/{userActivity}', [ForumController::class, 'deleteForumReplyActivity'])->name('connect.forum.reply.delete');
    Route::post('/connect/forum/reply/reply/create/{userActivity}/{userActivityReply}', [ForumController::class, 'replyOnActionReply'])->name('forum.reply.reply.create');
    Route::post('/connect/forum/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [ForumController::class, 'reactOnReply'])->name('forum.react.reply.create');

    Route::any('/connect/forum/getforum/{id}', [ForumController::class, 'getForums'])->name('get.forums');
    Route::any('/connect/forum/getStatus/{id}/{user}', [ForumController::class, 'getStatus'])->name('get.status');

    Route::any('/groups', [GroupController::class, 'list'])->name('connect.groups');
    Route::get('/group/activate/{id}', [GroupController::class, 'activate'])->name('group.activate');
    Route::get('/group/decline/{id}', [GroupController::class, 'decline'])->name('group.decline');
    Route::get('/group/request/{id}', [GroupController::class, 'request'])->name('group.request');
     
    Route::get('/groups/details/{id}', [GroupController::class, 'details'])->name('group.details');
    Route::any('/myGroup/followRequests/{id}', [GroupController::class, 'myFollowRequests'])->name('group.profile.myFollowRequests');
    Route::any('/myGroup/accept/{id}/{follow}', [GroupController::class, 'myFollowAccept'])->name('group.profile.myFollowAccept');

    Route::get('/group/create', [GroupController::class, 'form'])->name('group.form');
    Route::post('/group/store', [GroupController::class, 'store'])->name('group.store');
    Route::get('/group/edit/{id}', [GroupController::class, 'edit'])->name('group.edit');
    Route::post('/group/edit/{id}', [GroupController::class, 'update'])->name('group.update');
    Route::any('/group/{id}', [GroupController::class, 'delete'])->name('group.delete');

    Route::post('/group/activity/create', [GroupController::class, 'createAcivity'])->name('group.activity.create');
    Route::post('/connect/group/delete/{userActivity}', [GroupController::class, 'deleteAcivity'])->name('group.activity.delete');
    Route::post('/connect/group/reply/create/{userActivity}', [GroupController::class, 'createAcivityReply'])->name('group.reply.create');
    Route::post('/connect/group/reactions/{activityId}', [GroupController::class, 'reaction'])->name('group.reactions.create');
    Route::post('/connect/group/reply/reply/create/{userActivity}/{userActivityReply}', [GroupController::class, 'replyOnActionReply'])->name('group.reply.reply.create');
    Route::post('/connect/group/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [GroupController::class, 'reactOnReply'])->name('group.react.reply.create');
    
    Route::any('/connect/group/forum/getforum/{id}', [GroupController::class, 'getForums'])->name('get.forums');
    Route::any('/connect/group/forum/getStatus/{id}/{user}', [GroupController::class, 'getStatus'])->name('get.status');
    Route::any('/connect/group/forum/{groupid}', [GroupController::class, 'forumIndex'])->name('group.connect.forum');
   

    Route::any('/connect/group/resource', [GroupController::class, 'groupResource'])->name('group.resource'); 
    Route::any('/connect/group/forum/topic', [GroupController::class, 'groupStoreTopic'])->name('group.forum.topic.store');
    Route::any('/connect/group/forum/create', [GroupController::class, 'storeForum'])->name('group.forum.store');
    Route::any('/connect/group/forum/follow/{id}', [GroupController::class, 'follow'])->name('group.forum.follow');
    Route::any('/connect/group/forum/details/{id}', [GroupController::class, 'detailForum'])->name('group.forum.details');
    Route::post('/connect/group/forum/reply/create/{userActivity}', [GroupController::class, 'forumCreateAcivityReply'])->name('group.forum.reply.create');
    Route::post('/connect/group/forum/reactions/{activityId}', [GroupController::class, 'forumReaction'])->name('group.forum.reactions.create');
    Route::post('/connect/group/forum/reply/reply/create/{userActivity}/{userActivityReply}', [GroupController::class, 'forumReplyOnActionReply'])->name('group.forum.reply.reply.create');
    Route::post('/connect/group/forum/reply/reacttions/create/{userActivityId}/{userActivityReplyId}', [GroupController::class, 'forumReactOnReply'])->name('group.forum.react.reply.create');

    
   
    Route::get('/markAsRead', function(){
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    })->name('markAsRead');

    Route::get('/single/notification/{id}', [NotificationsController::class, 'singleNotification'])->name('single.notification');
    
    Route::post('/ck/upload', [PageController::class, 'ckUpload'])->name('ck.upload');
    
});
Route::middleware(['auth', 'verified','admin'])->prefix('facilitator')->group(function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('facilitator.dashboard');

    Route::get('/meetings', [SessionController::class, 'list'])->name('sessions.index');
    // Create meeting room using topic, agenda, start_time.
    Route::get('/meetings/create', [SessionController::class, 'form'])->name('sessions.form');
    Route::post('/meetings/store', [SessionController::class, 'store'])->name('sessions.store');
    Route::get('/meetings/edit/{id}', [SessionController::class, 'edit'])->name('sessions.edit');
    Route::post('/meetings/edit/{id}', [SessionController::class, 'update'])->name('sessions.update');
    // Get information of the meeting room by ID.
    Route::get('/meetings/{id}', [SessionController::class, 'get'])->where('id', '[0-9]+');
    Route::delete('/meetings/{id}', [SessionController::class, 'delete'])->where('id', '[0-9]+')->name('sessions.delete');

    Route::get('/session/resource/json', [SessionResourceController::class, 'json'])->name('sessionsResource.json');
    Route::get('/session/resource', [SessionResourceController::class, 'list'])->name('sessionsResource.index');

    Route::get('/group/json', [GroupController::class, 'json'])->name('group.json');
    Route::get('/group', [GroupController::class, 'list'])->name('group.index');
   
    
});

// /* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('user-licenses')->name('user-licenses/')->group(static function() {

//         });
//     });
// });

// /* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('courses')->name('courses/')->group(static function() {
//             Route::get('/',                                             'CoursesController@index')->name('index');
//             Route::get('/create',                                       'CoursesController@create')->name('create');
//             Route::post('/',                                            'CoursesController@store')->name('store');
//             Route::get('/{course}/edit',                                'CoursesController@edit')->name('edit');
//             Route::post('/bulk-destroy',                                'CoursesController@bulkDestroy')->name('bulk-destroy');
//             Route::post('/{course}',                                    'CoursesController@update')->name('update');
//             Route::delete('/{course}',                                  'CoursesController@destroy')->name('destroy');
//         });
//     });
// });

/* Auto-generated admin routes */
// Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(static function () {
//     Route::prefix('admin')->namespace('Admin')->name('admin/')->group(static function() {
//         Route::prefix('session-meetings')->name('session-meetings/')->group(static function() {
//             Route::get('/',                                             'SessionMeetingsController@index')->name('index');
//             Route::get('/create',                                       'SessionMeetingsController@create')->name('create');
//             Route::post('/',                                            'SessionMeetingsController@store')->name('store');
//             Route::get('/{sessionMeeting}/edit',                        'SessionMeetingsController@edit')->name('edit');
//             Route::post('/bulk-destroy',                                'SessionMeetingsController@bulkDestroy')->name('bulk-destroy');
//             Route::post('/{sessionMeeting}',                            'SessionMeetingsController@update')->name('update');
//             Route::delete('/{sessionMeeting}',                          'SessionMeetingsController@destroy')->name('destroy');
//         });
//     });
// });
