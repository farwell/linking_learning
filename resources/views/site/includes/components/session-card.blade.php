<div class="course-card mt-3 sessions-card">
    <article>
      <div class="thumbnail">

        @if( $session->hasImage('session_image'))
        <img src="{{$session->image("session_image", "default")}}" alt="{{$session->topic}}" />
       
        @endif
          <div class="overlay">
            <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$session->topic}}">
              <i class="fas fa-share"></i>
            </div>
          </div>
      </div>
  
      <div class="course-card-content session-card-content">
       

        <p class=" text-grey session-card-calendar">
          
          <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($session->start_time)->isoFormat('Do MMMM YYYY')}} </span>
          <span class="session-time"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($session->start_time)->format('H:i')}} Hrs EAT</span>
         
        </p>
        <p class="title ">
          <a href="{{route('session.details',[$session->id])}}" class=" text-purple">{{ $session->topic}}</a></p>
        
          <?php
          $str = $session['agenda'];
          if (strlen($str) > 60) {
            $str = substr($str, 0, 100) ;
          }
          
          ?>
        {!!$str!!}
        @if($session->start_time >= Carbon\Carbon::today()->toDateString())
        <p class="readmore ">
          <a href="{{route('session.details',[$session->id])}}" class="btn btn-overall btn_more_details">More Details </a>
        </p>
        @else 
         
        <p class="readmore ">
          <span><a href="{{route('session.details',[$session->id])}}" class="btn btn-overall btn_watch_video">Watch Video</a></span> 

          <span><a href="{{route('session.details',[$session->id])}}" class="btn btn-overall btn_take_course">Self Paced Session</a></span>
        </p>
        <p class="readmore ">
          
        </p>


        @endif
      </div>

    </article>
  </div>
  

  