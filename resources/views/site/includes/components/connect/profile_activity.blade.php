

      <form action="{{ route('activity.profile.create') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="{{ $user->id }}" name="user_id"> 
        <div class="card shadow connectCard">
            <div class="card-body connectCardBody">
                <div class="">
                    <div class="connectImgSection">
                        <div class="connectImgLeft">
                            @if ($user->profile_pic)
                                <img src="{{ asset('uploads/' . $user->profile_pic) }}"
                                    class="img-fluid connectImg mr-3" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}"
                                    class="img-fluid connectImg" />
                            @endif
                        </div>
                        <div class="connectRight">
                            <div class="main-message">
                                <p class="lead emoji-picker-container"> <textarea type="text" name="body" class="connectInput connect-font form-control"
                                    placeholder="Write something to {{ ucfirst($user->first_name) }}"id="example1"
                                    data-emojiable="true" data-emoji-input="unicode"></textarea></p>
                               
                            </div>
                            <div class="card-footer">
                                <div>
                                    <label for="imageInput" title="attach images"><img src="{{ asset('svgs/images.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input type="file" name="images[]" id="imageInput" multiple>
                                    <label for="gifInput" title="gif"><img src="{{ asset('svgs/gif.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input type="file" name="gifs[]" id="gifInput" multiple>
                                    {{-- <img src="{{ asset('svgs/emoji.svg') }}" class="img-fluid inputImg"
                                        width="20"/> --}}
                                    <label for="fileInput" title="attach files"><img src="{{ asset('svgs/attach.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input type="file" name="files[]" id="fileInput" multiple>
                                </div>
                                <div>
                                    <span><button type="submit"
                                            class="btn connect-button">Post</button></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @unless(empty($activities))
        @foreach ($activities as $activity)
            <div class="card shadow connectCard mt-3">
                <div class="card-body d-flex connectCardBody">
                    <div class="d-flex">
                        <div class="commentImg">
                            @if ($activity->user->profile_pic)
                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                    class="img-fluid connectImg" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                            @endif
                        </div>
                        <div class="upperRow">
                            <div class="commentsContainer">
                                <div class="upperCommentRow">
                                    <span class="name">
                                        {{ $activity->user->username }}
                                        <small class="time">
                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }}
                                        </small>
                                    </span>
                                    @if($activity->posted_by == Auth::user()->id || $activity->posted_by == $user->id)
                                    <div class="dropdown">
                                        <i id="dropdownMenuButton" class="fa fa-ellipsis-h"data-toggle="dropdown"></i>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item edit-item" href="#"><i class="fas fa-edit" data-toggle="modal"
                                            data-target="#practice_modal-{{ $activity->id }}"><span class="icon-text">Edit Post</span></i></a>
                                            <a class="dropdown-item delete-item"  href="{{ route('connect.activity') }}"
                                            onclick="event.preventDefault();
                                             document.getElementById(
                                             'delete-form-{{ $activity->id }}').submit();">@if(count($activity->replies) ==0 )
                                            <i class="fas fa-trash" data-toggle="tooltip"
                                                data-placement="bottom" title="Delete">
                                               <span class="icon-text">Delete Post</span> 
                                            </i>@endif
                                            </a>
                                    <form id="delete-form-{{ $activity->id }}" +
                                        action="{{ route('activity.delete', [$activity->id]) }}"
                                        method="post">
                                        @csrf
                                    </form>
                                        </div>
                                    </div>
                             @endif
                                </div>
                                <div class="commentMain">
                                    <p class="connect-font">{{ $activity->body }}</p>
                                </div>
                                <div class="commentImgInner">
                                    @unless(empty($activity->images))
                                        @foreach (json_decode($activity->images) as $image)
                                            <img src="{{ asset('images/activity/' . $image) }}"
                                                class="img-fluid main-Commentimg" />
                                        @endforeach
                                    @endunless
                                </div>
                                <div class="d-flex align-items-center" style="">
                                    <small class="float-left">
                                        <span title="Likes" class=""
                                            onclick="sendLike({{ $activity->id }}, 'like')">
                                            <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span
                                                class="activity-like-count " id="like-{{ $activity->id }}"
                                                style="font-size:14px">{{ $activity->reactions->sum('likes') }}</span>
                                        </span>
                                        <span title="Dislikes" class=""
                                            onclick="sendLike({{ $activity->id }}, 'dislike')">
                                            <i class="fa fa-thumbs-down" aria-hidden="true"></i> <span
                                                class="activity-like-count " id="dislike-{{ $activity->id }}"
                                                style="font-size:14px">{{ $activity->reactions->sum('dislikes') }}</span>
                                        </span>
                                        {{-- <a href="javascript:;"onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');" class="replylink" >Reply</a> --}}
                                        <span>
                                            <img src="{{ asset('svgs/comment.svg') }}" id=""
                                                class="img-fluid inputImg"
                                                onclick="showHide('reply-{{ $activity->id }}');showHideReply('response-{{ $activity->id }}');"
                                                width="20" /><span
                                                class="like-count">{{ $activity->replies->count() }}</span>
                                        </span>

                                        </form>
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{ route('reply.create', [$activity->id]) }}" method="POST"
                        enctype="multipart/form-data" style="width: 100%">
                        @csrf
                        {{-- <div class="replySection flex-column" id="reply-{{ $activity->id }}"
                            style="display: none">
                            <div class="replySection">
                                <div class="connectImgLeft">
                                  @if ($activity->user->profile_pic)
                                      <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                          class="img-fluid connectImg" />
                                  @else
                                      <img src="{{ asset('uploads/images.jpg') }}"
                                          class="img-fluid connectImg" />
                                  @endif
                                </div>
                                <div class="inputRight">
                                    <input type="text" name="comment" class="connectInput"
                                        aria-label="Sizing example input"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="Reply"
                                        data-emojiable="true" data-emoji-input="unicode">
                                    <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input name="images[]" type="file" id="imageInput" multiple>
                                    <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                            class="img-fluid inputImg" width="20" /></label>
                                    <input name="gifs[]" type="file" id="gifInput" multiple>
                                    <img src="{{ asset('svgs/emoji.svg') }}"
                                        class="img-fluid inputImg inputIcon" width="20" />
                                </div>
                            </div>
                        </div> --}}
                        <div class="container-fluid mt-3" id="reply-{{ $activity->id }}"
                            style="display: none;">
                            <div class="main  d-flex">
                                <div class="card " style="border: 0px;width:100%">
                                    <div class="row no-gutters">
                                        <div class="col-2 col-md-1">
                                            @if ($activity->user->profile_pic)
                                                <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}"
                                                    class="card-img avatar-small rounded-circle disc-img" />
                                            @else
                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                    class="card-img avatar-small rounded-circle disc-img" />
                                            @endif
                                        </div>
                                        <div class="col-10">

                                            <div class="card-body card-small pb-0">
                                                <div class="card-text">
                                                    <input type="text" name="id" value="5" hidden=""
                                                        id="replycomment_id">
                                                    <textarea id="replycomment_reply"
                                                        class="form-control text-input" name="comment"
                                                        placeholder="Reply" data-emojiable="true"
                                                        data-emoji-input="unicode"></textarea>
                                                </div>
                                                <label for="imageInput" title="attach images"><img
                                                        src="{{ asset('svgs/images.svg') }}"
                                                        class="img-fluid inputImg" width="20" /></label>
                                                <input name="images[]" type="file" id="imageInput" multiple>
                                                <label for="gifInput" title="gif"><img src="{{ asset('svgs/gif.svg') }}"
                                                        class="img-fluid inputImg" width="20" /></label>
                                                <input name="gifs[]" type="file" id="gifInput" multiple>
                                                {{-- <img src="{{ asset('svgs/emoji.svg') }}"
                                                    class="img-fluid inputImg inputIcon" width="20" /> --}}
                                                <div class="submitbuttons">
                                                    <button type="button"
                                                        class="btn btn-outline-dark btn-cancel"
                                                        onclick="showHide('reply-{{ $activity->id }}')">Cancel</button>
                                                    <button type="submit"
                                                        class="btn connect-button">Reply</button>
                                                    {{-- <a href="javascript:;" onclick="submitReply()"
                                                        class="button button-submit"> Submit Comment</a> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card-body pt-0" id="response-{{ $activity->id }}" style="display: none">
                        {{-- <summary>Show Replies</summary> --}}
                        <span class="showReplies pt-0">
                            @include('site.pages.connect.activity_replies')
                        </span>
                    </div>
                </div>
            </div>

            <form id="update-form-{{ $activity->id }}" +
                action="{{ route('connect.update', $activity->id) }}" method="post">
                @csrf
                <div class="modal fade edit-modal" id="practice_modal-{{ $activity->id }}" tabindex="-1"
                    role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header edit-modal-header">
                                <p class="modal-title name" id="exampleModalLabel">Edit Post</p>
                                <button type="button" class="close" data-dismiss="modal"
                                    aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <textarea type="text" name="body" id="name" value="" class="form-control"
                                    placeholder="">{{ $activity->body }}</textarea>
                            </div>
                            <div class="modal-footer justify-content-end">
                                {{-- <button type="button" class="btn btn-secondary"
                                    data-dismiss="modal">Close</button> --}}
                                <button type="submit" class="btn btn-primary">Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach
    @endunless