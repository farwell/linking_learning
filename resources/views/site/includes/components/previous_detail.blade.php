@php

$details_panels = [['id' => '1', 'name' => '<i class="fa fa-eye" aria-hidden="true"></i> View comments', 'level' => '0'], ['id' => '2', 'name' => '<i class="fa fa-file" aria-hidden="true"></i> Session resources', 'level' => '0'], ['id' => '3', 'name' => '<i class="fa fa-book" aria-hidden="true"></i> Self paced session', 'level' => '1']];
@endphp


<section class="general-section" style="padding-left: 15px;">
    <div class="row">
        <div class="col-md-12">
            <div class="video-wrapper">
                <video controls playsinline muted id="bgvid">
                    <source src="{{ $pageItem->file('session_video') }}" type="video/mp4">
                </video>

            </div>

        </div>


        <div class="col-md-12">
            <h2 class="previous_h2 mt-3">{{ $pageItem->topic }}</h2>

            <p class="previous_p"> {{ Carbon\Carbon::parse($pageItem->start_time)->isoFormat('Do MMMM YYYY') }}
            </p>

            <hr>
        </div>
        <div class="col-lg-12">
            <div class="whatYouGetListing ">

                <div class="row session_details-panel">
                    <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                            <ul class="nav nav-pills nav-fill session-tabs" role="tablist" id="session-tabs">
                                @foreach ($details_panels as $key => $item)
                                    @if ($key === array_key_last($details_panels))
                                        @if ($item['level'] == 1)
                                            @if ($pageItem->course)
                                                <li role="presentation"
                                                    class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                    <a
                                                        href="{{ route('course.detail', [$pageItem->course->slug]) }}">{!! $item['name'] !!}</a>
                                                </li>
                                            @else
                                                <li role="presentation"
                                                    class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                    <a href="#">{!! $item['name'] !!}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li role="presentation"
                                                class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                    data-toggle="tab">{!! $item['name'] !!}</a>

                                            </li>
                                        @endif
                                    @elseif($key === array_key_first($details_panels))
                                        <li role="presentation"
                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                                            <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                data-toggle="tab" style="text-align: center;
                                ">{!! $item['name'] !!}</a>
                                        </li>
                                    @else
                                        <li role="presentation"
                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                                            <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                data-toggle="tab">{!! $item['name'] !!}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @elseif((new \Jenssegers\Agent\Agent())->isMobile())
                            <ul class="nav nav-pills session-tabs" role="tablist">
                                @foreach ($details_panels as $key => $item)
                                    @if ($key === array_key_last($details_panels))
                                        @if ($item['level'] == 1)
                                            @if ($pageItem->course)
                                                <li role="presentation"
                                                    class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                    <a
                                                        href="{{ route('course.detail', [$pageItem->course->slug]) }}">{!! $item['name'] !!}</a>
                                                </li>
                                            @else
                                                <li role="presentation"
                                                    class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                    <a href="#">{!! $item['name'] !!}</a>
                                                </li>
                                            @endif
                                        @else
                                            <li role="presentation"
                                                class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                    data-toggle="tab">{!! $item['name'] !!}</a>

                                            </li>
                                        @endif
                                    @elseif($key === array_key_first($details_panels))
                                        <li role="presentation"
                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                                            <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                data-toggle="tab" style="text-align: center;
                                ">{!! $item['name'] !!}</a>
                                        </li>
                                    @else
                                        <li role="presentation"
                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                                            <a href="#home{{ $item['id'] }}" aria-controls="home" role="tab"
                                                data-toggle="tab">{!! $item['name'] !!}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
                <div role="tabpanel " class="session_details-panel mt-4">


                    <div class="tab-content" id="tab-content">
                        @foreach ($details_panels as $item)
                            <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}"
                                id="home{{ $item['id'] }}" class="active">


                                @if ($item['id'] == 1)
                                    @include('site.includes.components.discuss')

                                    @include(
                                        'site.includes.components.discussview'
                                    )
                                @elseif($item['id'] == 2)
                                    @include(
                                        'site.includes.components.session_resource',
                                        ['pageItem' => $pageItem]
                                    )
                                @endif


                            </div>
                        @endforeach
                    </div>
                </div>

            </div>


        </div>

    </div>


</section>
