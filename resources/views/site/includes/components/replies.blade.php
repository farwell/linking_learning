{{-- discussion index --}}
<div id="include-replies">
<div class="container-fluid" id="SectionName-{{$comments->id}}" style="display: none;">
    <div class="main  d-flex" >
        <div class="card " style="border: 0px;width:100%">
            <div class="row no-gutters">
                <div class="col-2 col-md-1" >
                    {{-- kutakua na if statement ya kama mtu ako na image kwa db --}}
                    <img class="card-img avatar-small rounded-circle disc-img" src="{{ asset('uploads/images.jpg') }}" alt="avatar">
                </div>
                <div class="col-10">
                    <form method="POST" action="" id="reply-discussion">
                       
                        <div class="card-body card-small">
                            {{-- <h5 class="card-title">TITLE</h5> --}}
                            <div class="card-text">
                                <input type="text" name="id" value="{{$comment->id}}" hidden id="replycomment_id">
                                {{-- <input type="text"  class="form-control text-input" placeholder="What are your thoughts?" name="reply"> --}}

                                <textarea class="form-control text-input" name="reply" placeholder="What are your thoughts?" data-emojiable="true" data-emoji-input="unicode"></textarea>
                                {{-- <textarea class="form-control "name="reply" id="comment" rows="1" cols="76" placeholder="What are your thoughts?" style="border-top:0px;border-left:0px;border-right:0px;"></textarea>                         --}}
                            </div>
                            <div class="submitbuttons">
                                <button type="button" class="btn btn-outline-dark btn-cancel" onclick="ShowAndHide({{$comment->id}})">Cancel</button>
                               
                                <button type="submit" class="button button-submit">Submit Comment</button>
                            </div>                                       
                        </div>                    
                    </form>                  
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{{-- end discussion index --}}



