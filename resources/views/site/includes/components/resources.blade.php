
<div class="container-fluid">
 
<div class="card resources mb-3" style="height:380px">
  
  
    @if( $item->hasImage('resource_image'))
    <div class="card-img" style="background-image:url('{{$item->image("resource_image", "default")}}');" style="height:80%"></div> 
    @else 
    <div class="card-img"></div>
    @endif
    <div class="card-img-overlay d-flex flex-column  justify-content-center">
        <h4 class="card-title">{{$item->title}} </h4>

        @if( $item->file('download_resource'))

    @php $doc = $item->file("download_resource"); @endphp

    <a href="{{$doc}}" class="btn btn-overall btn_resource" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download </a>

    @elseif (!empty($item->external_link))

    <a href="{{$item->external_link}}" class="btn btn-overall btn_resource" target="_blank"><i class="fas fa-globe" ></i> Visit Website </a>
    
   @endif
    </div>
    </div>
    @if (Route::current()->getName() === 'home')
  
    @else
    <div class="container">
 <form action="{{ route('rate.resource') }}" method="POST">
 {{ csrf_field() }}
    <div class="rating">
    <input id="input-1" name="rate" class="rating rating-loading" data-min="0" data-max="5" data-step="0.1" value="{{ $item->userAverageRating }}" data-size="xs">
      <input type="hidden" name="id" required="" value="{{ $item->id }}">
       
      <button class="btn btn-success btn-sm">Submit Rating</button>
      </div>
</form>
      
    </div>
@endif

</div>



<script>
 $("#input-id").rating();

    </script>
