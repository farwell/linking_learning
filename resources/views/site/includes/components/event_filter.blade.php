@if ((new \Jenssegers\Agent\Agent())->isDesktop())

<form action="{{route('events')}}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-row align-items-center">
      <div class="col-auto col-3">
        <select name="event_filter" id="" class="form-control filter_select">
          <option value=""> Filter Events</option>
          <option value="1"> Happening This Month </option>
          <option value="2"> Upcoming Events </option>
          <option value="3"> Past Events</option>
          {{-- <option value="4"> All Events </option> --}}
        </select>
      </div> 
      <div class="col-auto col-5 ">
        <button type="submit" class="btn btn-overall btn_event_filter ">Filter</button>

        <button type="reset" class="btn btn-overall green_bg" style="padding:0.1rem 2rem !important"
                                            onClick="window.location.href=window.location.href">Reset</button>
      </div>
    </div>
  </form>

@else 
<form action="{{route('events')}}" method="POST">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="form-row align-items-center">
    <div class="col-auto col-12">
      <select name="event_filter" id="" class="form-control filter_select">
        <option value=""> Filter Events</option>
        <option value="1"> Happening This Month </option>
        <option value="2"> Upcoming Events </option>
        <option value="3"> Past Events</option>
        {{-- <option value="4"> All Events </option> --}}
      </select>
    </div> 
    <div class="col-auto col-12 mt-3">
      <button type="submit" class="btn btn-overall btn_event_filter ">Filter</button>

      <button type="reset" class="btn btn-overall green_bg" style="padding:0.1rem 2rem !important"
                                          onClick="window.location.href=window.location.href">Reset</button>
    </div>
  </div>
</form>


@endif