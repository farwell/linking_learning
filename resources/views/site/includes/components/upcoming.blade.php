

<section class="general-section">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2  class="header-with-line about-sidebar-title"> Upcoming Session </h2>
            <hr>
		</div>
    @if($upcoming)
		<div class="row">
			<div class="col-lg-12">
				<div class="card upcoming-card upcoming-side mb-3" >
                    <div class="row g-0">
                      <div class="col-md-12 ">
                        @if( $upcoming->hasImage('session_image'))
                        <img src="{{$upcoming->image("session_image", "default")}}" alt="{{$upcoming->topic}}" class="img-fluid upcoming-image"/>
                      
                        @endif
                      </div>
                      <div class="col-md-12 ">
                        <div class="card-body">
                            <p class=" text-grey session-card-calendar">
          
                                <span class="session-calendar"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($upcoming->start_time)->isoFormat('Do MMMM YYYY')}} </span>
                                
                      
                                <span class="session-time"> <i class="fa fa-calendar" aria-hidden="true"></i> {{Carbon\Carbon::parse($upcoming->start_time)->format('H:i')}} Hrs EAT</span>
                               
                              </p>
                          <p class="card-title">{{ $upcoming->topic}}</p>
                          
                    <p class="readmore ">
                      <a href="{{route('session.details',[$upcoming->id])}}" id="more_details_for_sessions"class="btn btn-overall btn_more_details">More Details </a>
                    </p>
                  
                        </div>
                      </div>
                    </div>
                  </div>
			</div>
		</div>
    @endif
	</div>

</section>