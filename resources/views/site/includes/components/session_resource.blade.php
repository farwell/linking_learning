{{-- <div class="alert alert-danger print-error-msg" style="display:none">
    <ul></ul>
</div> --}}

<div class="col-md-12 no-padding-left single-principle-page" id="single-principle">
    <div class="panel-group principle_content  resources-load" id="accordion">
        <?php
        $expand = (int) (app('request')->input('collapse') ?: 0);
        ?>
        @foreach ($resourceForm as $section)
            <div class="panel panel-default" id="panel{{ $loop->index }}">
                <div class="panel-heading" role="tab" id="heading{{ $loop->index }}">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $loop->index }}"
                            class="{{ $loop->index === $expand ? '' : 'collapsed' }}">
                            {{ $section->title }}</a>
                    </h4>
                </div>
                <div id="collapse{{ $loop->index }}"
                    class="panel-collapse collapse {{ $loop->index === $expand ? 'in' : null }}">
                    <div class="panel-body">

                        @if (count($section->sessionResource) > 0)
                            <table style="max-width: 100% !important; width: 1512px;" class="hidden-xs">
                                <tbody>
                                    <tr>
                                        <td style="width: 611px;">
                                            <p class="m-0"><b>Name of Resource</b></p>
                                        </td>
                                        <td style="width: 683.183px;">
                                            <p class="m-0"><b>Description</b></p>
                                        </td>
                                        <td style="width: 325.817px;">
                                            <p class="m-0"><b>Source</b></p>
                                        </td>
                                        <td style="width: 325.817px;">
                                            <p class="m-0"><b>Edit</b><small> ( You can only edit resources uploaded by yourself ) </small></p>
                                        </td>
                                    </tr>

                                    {!! $section->getResource() !!}

                                </tbody>
                            </table>
                        @endif

                    </div>
                </div>
            </div>
        @endforeach

    </div>
</div>
<!-- Modal -->
@foreach ($sessionResource as $resource)
    <form action="" method="POST" enctype="multipart/form-data" id="file-update-form-{{ $resource->id }}"
        class="file-upload-form">
        {{-- @csrf --}}
        <div class="modal fade session-modal" id="practice_modal-{{ $resource->id }}" tabindex="-1" role="dialog"
            aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header edit-modal-header">
                        <p class="modal-title name" id="exampleModalLabel">Edit Resource</p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body d-flex flex-column">
<div class="form-group row success-display" id="success-display-{{ $resource->id }}" style="display:none">
    <div class="col-md-12">
        <div class="form-check">
            <div class="alert alert-success alert-dismissible fade  print-success-msg" role="alert"
                id="print-success-upload-{{ $resource->id }}">

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>
                        <div class="form-group row error-display" id="error-display-{{ $resource->id }}" style="display:none">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <div class="alert alert-danger alert-dismissible fade print-error-msg " role="alert"
                                        id="print-error-upload-{{ $resource->id }}">
                                        <ul></ul>
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input id="session_id" type="hidden" name="session_id" value="{{ $pageItem->id }}" />

                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label> Title </label>
                                    <input id="title" type="text" class="form-control" name="title" autocomplete="off"
                                        value="{{ $resource->title }}" autofocus>
                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label> Description </label>
                                    <textarea id="description" name="description" class="md-textarea form-control"
                                        rows="5">{{ $resource->description }}</textarea>
                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <div class="form-check">
                                    <label> Resource Type</label>
                                    <select name="resource_type_id" id="resource_type" class="form-control">
                                        <option>Select Resource Type</option>
                                        @foreach ($resourceForm as $type)
                                            <option value="{{ $type->id }}"> {{ $type->title }}</option>
                                        @endforeach
                                    </select>
                                    @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>


                        <div class="form-group row justify-content-center">
                            <div class="col-md-12">
                                <div class="file-background-modal">
                                    <div class="preview-zone-modal hidden"
                                        id="preview-zone-modal-{{ $resource->id }}">
                                        <div class="box-modal box-solid">
                                            <div class="box-header with-border">
                                                <div><b>Preview</b></div>
                                                <div class="box-tools pull-right">
                                                    <button type="button"
                                                        class="btn btn-overall btn_take_course remove-preview-modal" id="remove-preview-modal-{{ $resource->id }}">
                                                        Remove upload
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="box-body-modal" id="box-{{ $resource->id }}"></div>
                                        </div>
                                    </div>
                                    <div class="dropzone-wrapper-modal"
                                        id="dropzone-wrapper-modal-{{ $resource->id }}">
                                        <label class="dropzone-desc-modal" for="file-upload-modal-{{ $resource->id }}">
                                            <h3> Upload your files </h3>
                                            Supports: PDF, MP4, PNG, JPEG and Word files
                                            <br /><br />Drag and drop files here
                                            <br />or
                                            <br /><br /><span id="file-upload-btn-modal-{{ $resource->id }}" class="button"
                                                data-id="{{ $resource->id }}">Browse files</span>
                                            <br /><br />Maximum file size: 100Mb
                                        </label>
                                        <input type="file" name="fileUpload"
                                            class="dropzone-modal dropzone-modal-{{ $resource->id }}"
                                            id="file-upload-modal-{{ $resource->id }}">
                                    </div>
                                </div>
                                <span class="invalid-feedback" role="alert" id="image_error" style="display:none">
                                </span>
                            </div>
                        </div>



                        <div class="row modal-footer align-self-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-overall btn_upload" id="saveLevels"
                                    ><i
                                        class="loading-icon fa-lg fas fa-spinner fa-spin session-hide"></i>Save</button>
                            </div>
                           
                        </div>

                        <div class="col-md-12">
                            <p style="font-size:13px; color:red;">Please note that you have to refresh the page to see the edited version of the resource.</p>
                        </div>


                    </div>
                    {{-- <div class="modal-footer justify-content-end"> --}}
                    {{-- <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button> --}}
                    {{-- <button type="submit" class="btn btn-primary">Post</button> --}}
                    {{-- </div> --}}
                </div>
            </div>
        </div>
    </form>
@endforeach
<div class="row justify-content-center">
    <h1 class="text-center">Share Resources</h1>
    <h4 class="text-center">Share tools or resources that you think will be useful to other
        participants</h4>

    <button type="button" class="btn btn-overall btn_upload mt-3 mb-3" id="uploadResource" onclick="showUpload()"
        style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8"> <i class="fa fa-upload"
            aria-hidden="true"></i> Upload Resources</button>
</div>




<div class="form-group row success-display" style="display:none">
    <div class="col-md-12">
        <div class="form-check">
            <div class="alert alert-success alert-dismissible fade  print-success-msg" role="alert"
                id="print-success-upload">

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
</div>

<form action="" method="POST" enctype="multipart/form-data" id="file-upload-form" style="display: none">
    <div class="form-group row error-display" style="display:none">
        <div class="col-md-12">
            <div class="form-check">
                <div class="alert alert-danger alert-dismissible fade print-error-msg " role="alert"
                    id="print-error-upload">
                    <ul></ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input id="session_id" type="hidden" name="session_id" value="{{ $pageItem->id }}" />

    <div class="form-group row justify-content-center">
        <div class="col-md-12">
            <div class="form-check">
                <label> Title </label>
                <input id="title" type="text" class="form-control" name="title" autocomplete="off"
                    value="{{ old('title') }}" autofocus>
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>


    <div class="form-group row justify-content-center">
        <div class="col-md-12">
            <div class="form-check">
                <label> Description </label>
                <textarea id="description" name="description" class="md-textarea form-control" rows="5"></textarea>

                @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-12">
            <div class="form-check">
                <label> Resource Type</label>
                <select name="resource_type_id" id="resource_type" class="form-control">
                    <option>Select Resource Type</option>
                    @foreach ($resourceForm as $type)
                        <option value="{{ $type->id }}"> {{ $type->title }}</option>
                    @endforeach
                </select>
                @error('type')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-12">
            <div class="file-background">
                <div class="preview-zone hidden">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <div><b>Preview</b></div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-overall btn_take_course remove-preview">
                                    Remove upload
                                </button>
                            </div>
                        </div>
                        <div class="box-body"></div>
                    </div>
                </div>
                <div class="dropzone-wrapper">

                    <label class="dropzone-desc" for="file-upload">
                        <h3> Upload your files </h3>
                        Supports: PDF, MP4, PNG, JPEG and Word files
                        <br /><br />Drag and drop files here
                        <br />or
                        <br /><br /><span id="file-upload-btn" class="button">Browse files</span>
                        <br /><br />Maximum file size: 100Mb
                    </label>

                    <input type="file" name="fileUpload" class="dropzone" id="file-upload">
                </div>
            </div>

            <span class="invalid-feedback" role="alert" id="image_error" style="display:none">

            </span>

        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-overall btn_upload" id="saveLevels" style="left: 38%;"><i
                    class="loading-icon fa-lg fas fa-spinner fa-spin session-hide"></i>Submit
                Resource</button>
        </div>
    </div>

</form>
