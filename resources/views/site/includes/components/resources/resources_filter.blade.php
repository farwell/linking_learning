@if((new \Jenssegers\Agent\Agent())->isDesktop())
<div class="container-fluid">
 
    <div class="card resource-filter mb-3">
      <div class="col-12">
        <div class="row">
          <div class="col-12">
                <form action="{{route('resources')}}" method="POST">
                    @csrf
                    <div class="row">
                     
                      <div class="col-3 col-spacing">
                      <select name="uploader" id="" class="form-control mt-4 mb-4 minimal">
                        <option value="">Select Category</option>
                        <option value="1">Facilitator Resources</option>
                        <option value="2">Participant Resources</option>
                      </select>
                    </div>
                      <div class="col-2 col-spacing">
                        <select name="country" id="" class="form-control mt-4 mb-4 minimal">
                            <option value="">Filter by Country</option>
                            @foreach($countries as $key => $country)
                             <option value="{{$key}}"> {{$country}}</option>
                            @endforeach
                          </select>
                      </div>
                      <div class="col-3 col-spacing">
                        <select name="theme" id="" class="form-control mt-4 mb-4 minimal">
                            <option value="">Filter by Theme</option>
                            @foreach($resourceTheme as $theme)
                            <option value="{{$theme->id}}"> {{$theme->title}}</option>
                           @endforeach
                          </select>
                      </div>
                      <div class="col-2 col-spacing">
                        <input type="text" class="form-control mt-4 mb-4" placeholder="Keyword search" name="keyword">
                      </div>
                      <div class="col-2 col-spacing">
                        <button type="submit" class="btn btn-overall btn_register mt-4 mb-4" style="padding: 1.2% 5%; !important">Search</button>
                        <button type="reset" class="btn btn-overall green_bg" style="padding: 1.2% 5%; !important"
                        onClick="window.location.href=window.location.href">Reset</button>
                      </div>
                    </div>
                  </form>
    
                </div>
           </div>

      </div>
      
    </div>
</div>

@else 
<div class="container-fluid">
 
  <div class="card resource-filter mb-3">
    <div class="col-12">
      <div class="row">
     
          <div class="col-12">
             
              <select name="" id="" class="form-control mt-4 mb-4 minimal">
                <option>Select Category</option>
                <option value="1">Facilitator Resources</option>
                <option value="2">Participant Resources</option>
              </select>
  
            </div>
            <div class="col-12">
              <form action="{{route('resources')}}" method="POST">
                  @csrf
                  <div class="row">
                   
                    <div class="col-12">
                      <select name="country" id="" class="form-control mt-4 mb-4 minimal">
                          <option>Filter by Country</option>
                          @foreach($countries as $key => $country)
                           <option value="{{$key}}"> {{$country}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-12">
                      <select name="theme" id="" class="form-control mt-4 mb-4 minimal">
                          <option>Filter by Theme</option>
                          @foreach($resourceTheme as $theme)
                          <option value="{{$theme->id}}"> {{$theme->title}}</option>
                         @endforeach
                        </select>
                    </div>
                    <div class="col-12">
                      <input type="text" class="form-control mt-4 mb-4" placeholder="Keyword search" name="keyword">
                    </div>
                    <div class="col-12">
                      <button type="submit" class="btn btn-overall btn_register mt-4 mb-4">Search</button>
                      <button type="reset" class="btn btn-overall green_bg" style="padding: 1.2% 5%; !important"
                      onClick="window.location.href=window.location.href">Reset</button>
                    </div>
                  </div>
                </form>
  
            </div>
         </div>

    </div>
    
  </div>
</div>


@endif