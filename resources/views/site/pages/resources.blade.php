@extends('layouts.app_no_js')

@section('title','Resources')

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    $description = $pageItem->description;
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text,
    'description'=> $description
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        {{-- <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li> --}}
    </ol>
    @endcomponent
    @if(Session::has('course_success'))
    <script>
    jQuery(document).ready(function($){
      console.log("it has");
     $("#CourseSuccess").addClass('show');
  });
  </script>
  @elseif(Session::has('course_errors'))
  <script>
  jQuery(document).ready(function($){
   $("#CourseErrors").addClass('show');
  });
  </script>
  @else
      {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  
  </script> --}}
  @endif
    <div class="clearfix">
        <br /> <br />
    </div>

 
    <div class="col-md-12">
        @include('site.includes.components.all_resources', [
        'resourceType' => $resourceType,
        'articles' => $articles,
        'videos' => $videos,
        'audios' => $audios,
        'webinars' => $webinars,
        'publications' => $publications,
        'resourceForm' => $resourceForm
        ])
     
    </div>



@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace( 'description');
    CKEDITOR.replace( 'description1' );
    CKEDITOR.replace( 'description2' );
    CKEDITOR.replace( 'description3' );
    CKEDITOR.replace( 'description4' );
</script>

 
<script>
function showUpload(){
$('.resource-upload-form').css('display','block');
}
</script>


<script>
    $('#resource_type').on('change', function() {
     var type = $(this).val();
     if (type == 1) {
       document.getElementById("articles_additional").style.display = "block";
       document.getElementById("videos_additional").style.display = "none";
       document.getElementById("audios_additional").style.display = "none";
       document.getElementById("publications_additional").style.display = "none";
       document.getElementById("webinars_additional").style.display = "none";

     }else if (type == 2) {
       document.getElementById("articles_additional").style.display = "none";
       document.getElementById("videos_additional").style.display = "none";
       document.getElementById("audios_additional").style.display = "block";
       document.getElementById("publications_additional").style.display = "none";
       document.getElementById("webinars_additional").style.display = "none";

     }else if (type == 3) {
       document.getElementById("articles_additional").style.display = "none";
       document.getElementById("videos_additional").style.display = "block";
       document.getElementById("audios_additional").style.display = "none" ;
       document.getElementById("publications_additional").style.display = "none";
       document.getElementById("webinars_additional").style.display = "none";

     }else if (type == 4) {
       document.getElementById("articles_additional").style.display = "none";
       document.getElementById("videos_additional").style.display = "none";
       document.getElementById("audios_additional").style.display = "none" ;
       document.getElementById("publications_additional").style.display = "block";
       document.getElementById("webinars_additional").style.display = "none";

     }
     else {
       document.getElementById("additional_text").style.display = "none";
     }
   
   })
   
   </script>
<script>
function changeTab(id){

$("#resource-nav-tabs li.active").removeClass("active");

var navtab = $('.resource-tabs li').each(function () {

  if($(this).data('id') == id){
   $(this).addClass("active");

  }
  // console.log($(this).data('id'));
});

$(window).scrollTop(0);

}


</script>
@endsection
