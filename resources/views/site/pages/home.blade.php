@extends('layouts.app')

@section('content')
@php
$variant = $pageItem->variant;
$url = $pageItem->url;
$linkText = $pageItem->link_text;
@endphp


@if( $pageItem->hasImage('hero_image'))
<section class='device-banner' style="background:url('{{ $pageItem->image("hero_image", "default") }}');height: auto;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">

<button onclick="myFunction()" title="" class="btn-arrow arrow bounce d-none d-md-block" aria-hidden="true" style="" id="arrow-button">
  <i class="fas fa-chevron-down"></i>
</button>
@else 
<section class='device-banner'>  
@endif

  <div class="row ">
      <div class="col-md-5 offset-md-1">
          <div class="banner-section-text">
              <h1><strong>{!! $pageItem->header_title !!}</strong></h1>
              <p>{!! $pageItem->description !!}</p>
              @if ($variant && ($variant === 'orange_bg'))
           
                <a href="{{ $url }}" class="btn btn-overall btn_register">
                  {{ $linkText }}
                  <i class="fa fa-arrow-right "></i>
                </a>
            @elseif ($variant && ($variant === 'white_bg_orange_border'))
                <a href="{{ $url }} " class="btn btn-overall btn_login">
                  {{ $linkText }}
                  <i class="fa fa-arrow-right "></i>
                </a>
            @else 
            <a href="{{ $url }}" class="btn btn-overall btn_register">
              {{ $linkText }}
              <i class="fa fa-arrow-right "></i>
            </a>

            @endif
          </div>
      </div>
     
  </div>

</section>

@if(!Auth::check())

@else
  {!! $pageItem->renderBlocks(false) !!}

@endif

@endsection