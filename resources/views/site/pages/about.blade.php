@extends('layouts.app')

@section('title','Background')

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>



        <div class="row alignment-class-about">

            @if(!Auth::check())
            <div class="col-md-12">
                {!! $pageItem->renderBlocks(false) !!}

            </div> 

            @else
            <div class="col-md-9 col-12">
                {!! $pageItem->renderBlocks(false) !!}

            </div> 

            <div class="col-md-3 d-none d-md-block">
                


                @include('site.includes.components.upcoming',
                [
                   'upcoming'=>$upcoming,
               ])
                
                
                @include('site.includes.components.previous',
                [
                'previous'=>$previous,
                ])
   
            </div>
            @endif
        </div>

    


@endsection
