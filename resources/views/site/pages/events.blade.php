
@extends('layouts.app')

@section('title','Events')

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>

    <section  class="general-section">

        <div class="col-md-12">
            <div class="container">

                @include('site.includes.components.event_filter')
            </div>

        </div>
        <div class="clearfix">
            <br /> 
        </div>
    <div class="col-md-12">
        
     @foreach($events as $event)

     @include('site.includes.components.event',
                [
                'item'=>$event,
                ])
     @endforeach  
      
     <div class="row justify-content-center  pagination-spacing">{!! $events->render() !!}</div>
     
    </div>
    </section>
@endsection