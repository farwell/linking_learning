
@php
use Carbon\Carbon;
@endphp

@extends('layouts.app')

@section('title','Session')


@section('content')
<div class="background-page">
 
    @php 
    $text = $pageItem->topic;
    @endphp

    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/session_details.png"),
    'text'=> $text
    ])

    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('resources') }}">Resources</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{{$pageItem->title}}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8 offset-md-1">
                <h2>{!!$pageItem->title!!}</h2>

            </div> 
          
            <div class="col-md-8 offset-md-1">
                {!!$pageItem->description!!}

            </div> 
           
        </div>


       
    </div>



@endsection
