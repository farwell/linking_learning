@extends('layouts.app')

@section('title','Edit My Profile')

@section('css')
<style>
  label{
    font-size:15px;
  }
</style>
@endsection

@section('content')
<div class="courses-page">
  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/profile_header.png"),
  'text'=>'My Profile'
  ])

  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="#" class="active">My Profile</a>
    </li>
  </ol>
  @endcomponent

<div class="container-fluid">
  <div class="clearfix"><br /></div>
  <div class="clearfix"><br /></div>
  <div class="row  profile-row">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          @if (session()->has('register_errors'))
              <div class="alert alert-danger" role="alert">
                  {{ session()->get('register_errors') }}
              </div>
          @endif
          @if (session()->has('register_success'))
              <div class="alert alert-success" role="alert">
                  {{ session()->get('register_success') }}
              </div>
          @endif
    </div>

    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ppic-change-div">

      <ppic-form current="{{Auth::user()->profile_pic ? asset('uploads/'.Auth::user()->profile_pic)  : asset('images/profile/images.jpg') }}" action="{{ route('profile.ppic.change') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </ppic-form>

    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 profile-details-form">
      <h1 class="text-purple"><strong>Personal Information</strong></h1>

      <p class="text-purple">Edit your details and click submit to finish the process.</p>
      <form method="POST" action="{{route('profile.edit')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

          <div class="form-group row">
            <div class="col-md-10">
              <label>First Name </label>
                  <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ $user['first_name'] }}" required placeholder="First Name">

                  @error('first_name')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('class') }}</strong>
                      </span>
                  @enderror
                  </div>
              </div>

    <div class="form-group row">
        <div class="col-md-10">
          <label>Last Name</label>
              <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $user['last_name'] }}" required placeholder="Last Name">

              @if ($errors->has('last_name'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('last_name') }}</strong>
              </span>
              @endif
            </div>
        </div>

    <div class="form-group row">
        <div class="col-md-10">
          <label>Phone Number</label>
            <div class="input-group">
                <div class="input-group-prepend" style="width: 40%;">
                    <select class="form-control downArrow no-radius-right" aria-label="Country Code" name="mobile_number_country" id="mobile_number_country" data-behavior="customSelect" placeholder="{{__('forms.general.select_country')}}">
                        <option value selected disabled>{{__('Select Country')}}</option>
                        @foreach ((new \App\Helpers\Front)->getCountriesList() as $key => $value)
                            @if($key === $user['phone_locale'])

                            <option value="{{$key}}" selected>{{$value['label']}}</option>
                            @else
                            <option value="{{$key}}">{{$value['label']}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
    
                <input id="mobile_number" type="text"
                    class="form-control no-radius-left" name="phone"
                    value="{{ $user['phone'] }}" placeholder="700000000"  >
    
            </div>
            @if ($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('phone') }}</strong>
            </span>
            @endif
            </div>
        </div>


    <div class="form-group row">
        <div class="col-md-10">
          <label>Email</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user['email'] }}" required placeholder="Email">

            @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
            </div>
        </div>

    <div class="form-group row">
        <div class="col-md-10">
          <label>Affilliation </label>
            <input id="position" name="affiliation" type="text"  class="form-control" value="{{ $user['affiliation'] }}" placeholder="Affiliation">
            </div>
        </div>


        <div class="form-group row">
          <div class="col-md-10">
            <label> My Super Power </label>
            <textarea id="superpower" class="form-control wysiwyg" name="super_power" rows="8">{{$user->super_power}}</textarea>
              </div>
          </div>

          <div class="form-group row">
            <div class="col-md-10">
              <label>My projects fall under the following topic(s) </label>
              <select class="form-control" multiple name="topics[]">
                @foreach($project_topics as $topic)
               
                <option value={{$topic->id}}>{{$topic->title}}</option>

                @endforeach
              </select>
                </div>
            </div>

          <div class="form-group row">
            <div class="col-md-10">
              <label>My Current Projects </label>
              <textarea id="currentproject" class="form-control current-wysiwyg" name="current_project"  rows="8">{{$user->current_project}}</textarea>
                </div>
            </div>

        <div class="form-group row">
            <div class="col-md-10">
              <label>User Name</label>
                <input id="username" type="text" readonly="readonly" class="form-control" value="{{ $user['username'] }}" placeholder="Username" disabled>
                </div>
            </div>


    <div class="form-group row">
        <div class="col-md-10">
          <button type="submit" class="btn btn-overall purple">Submit</button>
        </div>
    </div>
      
      </form>
    </div>
  </div>
  <div class="clearfix"><br /></div>
</div>
</div>
@endsection
