@extends('layouts.app_no_js')

@section('title','Resources')

@section('content')
<div class="background-page">
    @if( $pageItem->hasImage('hero_image'))
    @php $image = $pageItem->image("hero_image", "default") ; 
    $text = $pageItem->header_title;
    @endphp
    @include('site.includes.components.parallax',[
    'image'=> $image,
    'text'=>$text
    ])
    @endif
    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">{!! $pageItem->title !!}</a>
        </li>
    </ol>
    @endcomponent

    <div class="clearfix">
        <br /> <br />
    </div>

    <div class="col-md-12">
     
        {!! $pageItem->renderBlocks(false) !!}
    </div>
     <div class="container-fluid">
            <div class="col-12">
                <div class="row justify-content-center">
                    <h1 class="text-center">Share Blog</h1>
                    {{-- <h4 class="text-center">Share tools or resources that you think will be useful to other
                        participants</h4> --}}
                    <button type="button" class="btn btn-overall btn_upload mt-3 mb-3" id="uploadResource"
                        onclick="showUpload()" style="flex-grow: 0; background:#6FC6B8; border-color:#6FC6B8"> <i
                            class="fa fa-upload" aria-hidden="true"></i> Upload Blog</button>
                </div>
                <div class="row justify-content-center">
                    <form action="{{ route('blog.create') }}" method="POST" enctype="multipart/form-data"
                        id="file-upload-form" style="display:none" class="resource-upload-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">            
                        <div id="articles_additional" style="display:block">
                            <div class="form-group row justify-content-center">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label> Title </label>
                                        <input id="title" type="text" class="form-control" name="title"
                                            autocomplete="off" value="{{ old('title') }}" autofocus>
                                        @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label> Description  &nbsp;&nbsp;&nbsp;<small
                                            style="font-size:11px">Square and landscape pictures allowed. Dimensions (Max 200 * 200px)</small> </label>
                                        <textarea id="description" type="text" name="description"
                                            class="md-textarea form-control" rows="5"></textarea>

                                        @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row justify-content-center">
                                <div class="col-md-12">
                                    <div class="form-check">
                                        <label> Blog Cover Image &nbsp;&nbsp;&nbsp;<small
                                                style="font-size:11px">supports: png | jpg | jpeg</small></label>
                                        <input type="file" name="blog_image" class="form-control"
                                            id="file-upload">

                                        @error('fileUpload')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-overall btn_upload" id="saveLevels"
                                    style="left: 45%;">Submit Blog</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>


@endsection


@section('js')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

<script>
    CKEDITOR.replace('description', {
    filebrowserUploadUrl: "{{route('ck.upload', ['_token' => csrf_token() ])}}",
    filebrowserUploadMethod: 'form'});
</script>

<script>
    function showUpload(){
    $('.resource-upload-form').toggle();
    $('#group_resources').css('display','none');
    }

    function showResources(){
    $('#group_resources_create').css('display','none');
    $('#group_resources').css('display','block');

    }
    </script>

    <script>
            $(document).ready(function() {

                // Get current page URL
                var url = window.location.href;



                // remove # from URL
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

                // remove parameters from URL
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

                // select file name
                url = url.split('/')[4];


                // console.log(url);

                // Loop all menu items
                $('.navbar-nav .nav-item').each(function() {

                    // select href
                    var href = $(this).find('a').attr('href');

                    link = href.split('/')[4];

                    // Check filename
                    if (link === 'resources') {

                        // Add active class
                        $(this).addClass('active');
                    }
                });
            });
        </script>
@endsection