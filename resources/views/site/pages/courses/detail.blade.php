@extends('layouts.app')

@section('title','Courses')
@php


$details = [
['id' => '1', 'name' => 'Description' ],
['id' => '2', 'name' => 'Course Outline'], 

];
@endphp
@section('content')
<div class="courses-page">
  @include('site.includes.components.parallax',[
  'image'=>asset("images/banners/course_header.png"),
  'text' => $course->name
  ])


  @component('site.includes.components.breadcrumbs')
  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="{{ route('home') }}">Home</a>
    </li>
    <li class="breadcrumb-item " aria-current="page">
      <a href="{{ route('session.details',[$course->sessions_id]) }}" class="active">Session</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
      <a href="/course/detail/{{$course->id}}" class="active">{{$course->name}}</a>
    </li>
  </ol>
  @endcomponent

  @if(Session::has('course_success'))
  <script>
  jQuery(document).ready(function($){
    console.log("it has");
   $("#CourseSuccess").addClass('show');
});
</script>
@elseif(Session::has('course_errors'))
<script>
jQuery(document).ready(function($){
 $("#CourseErrors").addClass('show');
});
</script>
@else
    <script>
window.addEventListener('load', function() {
    if(!window.location.hash) {
        window.location = window.location + '#/';
        window.location.reload();
    }
});
</script>
@endif

  <div class="row courses-detail-row">
    <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
      <ul class="nav nav-pills course-tabs" role="tablist">
        @foreach ($details as $key => $item)
        @if($key === array_key_first($details))
         <li class="nav-item active">
        <a class="nav-link first-tab" data-toggle="pill" href="#home">Description</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="pill" href="#menu1">Session Outline</a>
      </li>
      @endif
     
      @endforeach
    </ul>

    </div>
  </div>
   <div class="container-fluid">
    <div class="row column-reverse">
      <div class="col-lg-3 col-md-3 col-sm-12 col-12">
        @include('site.includes.components.course-actions')


      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-12">
    <div class="tab-content">
    <div id="home" class="container-fluid tab-pane active">
    <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="course-card detail-card">
        <article>
          <div class="course-card-content">
              <div class="row-description">
                  {!! $course->overview !!}
              </div>
          </div>
        </article>
      </div>
    </div>
    </div>
  </div>

    <div id="menu1" class="container-fluid tab-pane fade">
    <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
      <div class="course-card detail-card">
        <article>
          <div class="course-card-content">
              <div class="row-description">
                 {!! $course->more_info !!}
              </div>
          </div>
        </article>
      </div>
    </div>
    </div>
    </div>
    {{-- <div id="menu2" class="container-fluid tab-pane fade">
    <div class="row">
  
    </div>
    </div> --}}

  </div>

  

  </div>
</div>
</div>
</div>

@endsection
