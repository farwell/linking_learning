@extends('layouts.app_no_js')

@section('title', 'My Courses')

@section('css')
    <style>
        .messageInput .emoji-wysiwyg-editor {
            height: 150px;
            border: 1px solid #00000029;
        }

        .emoji-wysiwyg-editor::before {
            color: grey;
        }

        .emoji-wysiwyg-editor[data-placeholder]:not([data-placeholder=""]):empty::before {
            content: attr(data-placeholder);
        }

        .emoji-wysiwyg-editor::before {
            content: 'Write a message…';
        }

        .lead.emoji-picker-container {
            /* width: 300px; */
            margin-bottom: 0;
            display: block;

            input {
                width: 100%;
                height: 50px;
            }
        }

    </style>

@endsection

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax',[
        'image'=>asset("images/banners/profile_header.png"),
        'text'=>'Connect'
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ route('message.index') }}" class="active">Messages</a>
                </li>
            </ol>
        @endcomponent
        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="profile-row">

                <div class="row alignment-class-connect">

                    <div class="col-md-3">
                        @include('site.includes.components.connect.side_menu')
                    </div>

                    <div class="col-md-9">
                        {{-- <div class="card row">
                        <div class="card-body d-flex align-items-center"> --}}
                        {{-- <div class="flex-6">
                                <select class="form-control" id="messageSelect">
                                    <option>Search Messages</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div> --}}
                        {{-- <div class="flex-2">
                                <button class="btn messageSearchBtn">
                                    Search
                                </button>
                            </div> --}}
                        {{-- <div class="nav flex-4 ml-auto nav-pills" id="v-pills-tab" role="tablist">
                                <button class="newMessageBtn nav-link" id="v-pills-messages-tab-0" data-bs-toggle="pill"
                                    data-bs-target="#v-pills-messages-0" type="button" role="tab"
                                    aria-controls="v-pills-messages" aria-selected="false">
                                    New Message
                                    <i class="far fa-envelope"></i>
                                </button>
                            </div>
                        </div>
                    </div> --}}
                        <div class="card">
                            <div class="d-flex align-items-start mt-1">
                                <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist"
                                    aria-orientation="vertical" style="width: 30%;">
                                    <button class="newMessageBtn nav-link active" id="v-pills-messages-tab-0"
                                        data-bs-toggle="pill" data-bs-target="#v-pills-messages-0" type="button" role="tab"
                                        aria-controls="v-pills-messages" aria-selected="false" onclick="getValue(this,0)">
                                        New Message
                                        <i class="far fa-envelope"></i>
                                    </button>
                                    @unless(empty($received))
                                        @foreach ($received->flatten() as $msg)
                                            
                                            {{-- <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</button> --}}
                                            @if(Auth::user()->id == $msg->sender_id)
                                            <div class="card d-flex p-2 nav-link flex-row message-pills"
                                            data-id="{{ $msg->sender->id }}" onclick="getValue(this,{{ $msg->id }})"
                                            id="v-pills-home-tab-{{ $msg->id }}" data-bs-toggle="pill"
                                            data-bs-target="#v-pills-home-{{ $msg->id }}" type="button" role="tab"
                                            aria-controls="v-pills-home" aria-selected="false">
                                            <div class="messageImg 1">
                                                @if ($msg->receiver->profile_pic)
                                                    <img src="{{ asset('uploads/' . $msg->receiver->profile_pic) }}"
                                                        class="img-fluid connectImg" />
                                                @else
                                                    <img src="{{ asset('uploads/images.jpg') }}"
                                                        class="img-fluid connectImg" />
                                                @endif
                                            </div>
                                            <div class="upperRow">
                                                <div class="commentsContainer">
                                                    <div class="upperMessageRow">
                                                        <span class="name">
                                                            {{ $msg->receiver->name }}
                                                        </span>
                                                        <span class="time">
                                                            {{ \Carbon\Carbon::parse($msg->created_at)->diffForHumans() }}
                                                        </span>
                                                    </div>
                                                    <div class="commentMain">
                                                        <p>{{ substr($msg->message, 0, 30) . '....' }}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                            @else
                                            
                                            <div class="card d-flex p-2 nav-link flex-row message-pills"
                                                data-id="{{ $msg->sender->id }}" onclick="getValue(this,{{ $msg->id }})"
                                                id="v-pills-home-tab-{{ $msg->id }}" data-bs-toggle="pill"
                                                data-bs-target="#v-pills-home-{{ $msg->id }}" type="button" role="tab"
                                                aria-controls="v-pills-home" aria-selected="false">
                                                <div class="messageImg 1">
                                                    @if ($msg->sender->profile_pic)
                                                        <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                                            class="img-fluid connectImg" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="img-fluid connectImg" />
                                                    @endif
                                                </div>
                                                <div class="upperRow">
                                                    <div class="commentsContainer">
                                                        <div class="upperMessageRow">
                                                            <span class="name">
                                                                {{ $msg->sender->name }}
                                                            </span>
                                                            <span class="time">
                                                                {{ \Carbon\Carbon::parse($msg->created_at)->diffForHumans() }}
                                                            </span>
                                                        </div>
                                                        <div class="commentMain">
                                                            <p>{{ substr($msg->message, 0, 30) . '....' }}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            @endif
                                        @endforeach
                                    @endunless
                                    {{-- <button class="nav-link" id="v-pills-profile-tab-{{ $msg->id }}" data-bs-toggle="pill" data-bs-target="#v-pills-profile-{{ $msg->id }}" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</button> --}}
                                    <button class="nav-link d-none" id="v-pills-messages-tab-0" data-bs-toggle="pill"
                                        data-bs-target="#v-pills-messages-0" type="button" role="tab"
                                        aria-controls="v-pills-messages" aria-selected="false">Messages</button>
                                    {{-- <button class="nav-link" id="v-pills-settings-tab-{{ $msg->id }}" data-bs-toggle="pill" data-bs-target="#v-pills-settings-{{ $msg->id }}" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</button> --}}
                                </div>
                                <div class="tab-content message-tab-content" id="v-pills-tabContent">
                                    @unless(empty($received))
                                        @foreach ($received->flatten() as $msg)
                                            <div class="tab-pane fade show" id="v-pills-home-{{ $msg->id }}"
                                                role="tabpanel" aria-labelledby="v-pills-home-tab-{{ $msg->id }}"
                                                data-message="v-pills-home-{{ $msg->id }}">
                                                <div class="card-body d-flex align-items-start flex-column">
                                                    <div class="card-body message-body">
                                                        <div class="chat-container sss">
                                                          
                                                            @if ($msg->sender->profile_pic)
                                                                <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                                                    class="img-fluid connectImg" />
                                                            @else
                                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                                    class="img-fluid connectImg" />
                                                            @endif
                                                            <p class="connect-font text-left">{{ $msg->message }}</p>
                                                            <span
                                                                class="time-left">{{ date('Y-m-d h:i A', strtotime($msg->created_at)) }}</span>
                                                            <span class="float-right"> <a
                                                                    href="{{ route('message.index') }}" onclick="event.preventDefault();
                                                                document.getElementById(
                                                                'delete-form-{{ $msg->id }}').submit();">
                                                                <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a></span>
                                                            <form id="delete-form-{{ $msg->id }}" +
                                                                action="{{ route('message.delete', $msg->id) }}" method="post">
                                                                @csrf 
                                                            </form>
                                                        </div>
                                                        {{-- Messages section --}}
                                                        @foreach ($msg->conversation as $convo)
                                                            
                                                        <div class="chat-container xxx">
                                                            @if ($convo->sender->profile_pic)
                                                                <img src="{{ asset('uploads/' . $convo->sender->profile_pic) }}"
                                                                    class="img-fluid connectImg" />
                                                            @else
                                                                <img src="{{ asset('uploads/images.jpg') }}"
                                                                    class="img-fluid connectImg" />
                                                            @endif
                                                            <p class="connect-font text-left">{{ $convo->message }}
                                                            </p>
                                                            
                                                            <span
                                                                class="time-left">{{ date('Y-m-d h:i A', strtotime($convo->created_at)) }}</span>
                                                                <div class="commentImgInner">
                                                                    @unless(empty($convo->images))
                                                                        @foreach (json_decode($convo->images) as $image)
                                                                            <img src="{{ asset('images/activity/' . $image) }}"
                                                                                class="img-fluid main-Commentimg mr-0 mb-1 mt-1"
                                                                                style="border-radius: 0;max-width:100%" />
                                                                        @endforeach
                                                                    @endunless
                                                                </div>
                                                                   <span class="float-right"> <a
                                                            href="{{ route('message.index') }}" onclick="event.preventDefault();
                                                        document.getElementById(
                                                        'delete-form-{{ $convo->id  }}').submit();">
                                                        <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a></span>
                                                                  <form id="delete-form-{{ $convo->id }}" +
                                                        action="{{ route('convo.delete', $convo->id) }}" method="post">
                                                        @csrf 
                                                    </form>
                                                        </div>

                                                          
                                                        @endforeach
                                                        {{-- end of message section --}}
                                                    </div>
                                                    <div class="messageInput conversation inner-message">
                                                        <form action="{{ route('message.create') }}" method="POST"
                                                            enctype="multipart/form-data">
                                                            @csrf
                                                            <p class="lead emoji-picker-container"> <textarea name="message"
                                                                    type="text"
                                                                    class="messageInputBox form-control connect-font" rows="4"
                                                                    aria-label="Sizing example input"
                                                                    aria-describedby="inputGroup-sizing-sm"
                                                                    placeholder="Write a message…" data-emojiable="true"
                                                                    data-placeholder="this div is actually empty"
                                                                    data-emoji-input="unicode"></textarea></p>

                                                            <div class="d-flex justify-content-around">
                                                                <div>
                                                                    <input type="text" name="receiver_id[]"
                                                                        value="{{ $msg->sender->id ?? '' }}" hidden>
                                                                    <input name="images[]" type="file" id="imageInput" multiple>
                                                                    <label for="imageInput"><img
                                                                            src="{{ asset('svgs/images.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input name="gifs[]" type="file" id="imageInput" multiple>
                                                                    <label for="gifInput"><img
                                                                            src="{{ asset('svgs/gif.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input type="file" id="gifInput" multiple>

                                                                    <label for="fileInput"><img
                                                                            src="{{ asset('svgs/attach.svg') }}"
                                                                            class="img-fluid inputImg" width="20" /></label>
                                                                    <input name="attachments[]" type="file" id="fileInput"
                                                                        multiple>
                                                                    <input type="text" name="user_message_id"
                                                                        value="{{ $msg->id ?? '' }}" hidden>
                                                                </div>
                                                                <div>
                                                                    <button type="submit"
                                                                        class="btn btn-overall green_bg mt-2">Send</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                    <div class="card d-flex justify-content-center">
                                                        <div class="card-header">
                                                        New Message
                                                        </div>
                                                    </div>
                                                    </div> --}}
                                                    {{-- <div class="tab-pane fade" id="v-pills-settings-{{ $msg->id }}" role="tabpanel" aria-labelledby="v-pills-settings-tab">.4..</div> --}}
                                                </div>
                                            </div>
                                        @endforeach
                                    @endunless
                                    <div class="tab-pane show fade active" id="v-pills-messages-0" role="tabpanel"
                                        aria-labelledby="v-pills-messages-tab">
                                        <div class="card d-flex justify-content-center">
                                            <div class="card-header msg-title">
                                                New Messages
                                            </div>
                                            <div class="card-body d-flex align-items-start flex-column">
                                                {{-- <div class="flex-6">
                                                    <form action="{{ route('message.search') }}" method="POST"
                                                        id="search" onkeydown="return event.key != 'Enter';">
                                                        @csrf
                                                        <input id="search_input" class="form-control usersearch"
                                                            autocomplete="off" type="text" name="name"
                                                            onkeyup="searchUser(this)" placeholder="Search for user ...">
                                                    </form>
                                                </div> --}}
                                                {{-- <div class="d-flex justify-content-start mt-2">
                                                    <div id="name_list">
                                                        <ul class="names-list">
                                                        </ul>
                                                    </div>
                                                </div> --}}
                                                {{-- <div class="card-body message-body" id="message-welcome">
                                                </div> --}}
                                                <div class="messageInput">
                                                    <form action="{{ route('message.create') }}" method="POST"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="d-flex align-items-center mb-5">
                                                            <span class="mr-3 msg-title">Send Message to</span>
                                                            <select id="users_select" name="receiver_id[]"
                                                                multiple="multiple" class="form-control usersearch">
                                                                @foreach ($users as $user)
                                                                    <option value="{{ $user->id }}">
                                                                        {{ $user->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        <p class="lead emoji-picker-container">
                                                            <textarea name="message" type="text"
                                                                class="messageInputBox form-control connect-font " rows="4"
                                                                aria-label="Sizing example input"
                                                                aria-describedby="inputGroup-sizing-sm"
                                                                placeholder="Write a message…" data-emojiable="true"
                                                                data-emoji-input="unicode"> </textarea>
                                                        </p>


                                                        <div class="d-flex justify-content-between">
                                                            <div>
                                                                {{-- <input id="send_new_message" type="text" name="receiver_id"
                                                                    hidden> --}}
                                                                <input name="images[]" type="file" id="imageInput" multiple>
                                                                <label for="imageInput"><img
                                                                        src="{{ asset('svgs/images.svg') }}"
                                                                        class="img-fluid inputImg" width="20" /></label>
                                                                <input name="gifs[]" type="file" id="imageInput" multiple>
                                                                <label for="gifInput"><img
                                                                        src="{{ asset('svgs/gif.svg') }}"
                                                                        class="img-fluid inputImg" width="20" /></label>
                                                                <input type="file" id="gifInput" multiple>

                                                                <label for="fileInput"><img
                                                                        src="{{ asset('svgs/attach.svg') }}"
                                                                        class="img-fluid inputImg" width="20" /></label>
                                                                <input type="file" id="fileInput" multiple>
                                                            </div>
                                                            <div>
                                                                <button type="submit"
                                                                    class="btn  btn-overall green_bg mt-2 kkk">Send</button>
                                                            </div>
                                                        </div>


                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        @else
            <div>
                <div class="card">
                    <div class="card-body d-flex align-items-center">
                        <div class="flex-6">
                            {{-- <select class="form-control messageSearchMobile" id="messageSelect">
                                <option>&#xf002;Search Messages</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select> --}}
                        </div>
                        <div class="nav flex-4 ml-auto nav-pills" id="v-pills-tab" role="tablist" onclick="newMsg()">
                            <button class="connect-button pt-2 pb-2 pr-3 pl-3">
                                New
                                <i class="far fa-envelope ml-2"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane show fade active d-none" id="mobile-new" role="tabpanel"
                    aria-labelledby="v-pills-messages-tab">
                    <div class="card d-flex justify-content-center">
                        <div class="card-header">
                            New Messageaaa
                        </div>
                        {{-- <div class="card-body d-flex align-items-start flex-column">
                            <div class="flex-6">
                                <form action="{{ route('message.search') }}" method="POST" id="search"
                                    onkeydown="return event.key != 'Enter';">
                                    @csrf
                                    <input id="search_input" class="form-control usersearch" autocomplete="off" type="text"
                                        name="name" onkeyup="searchUser(this)" placeholder="Search for user ...">
                                </form>
                            </div>
                            <div class="d-flex justify-conten-start mt-2">
                                <div id="name_list">
                                    <ul class="names-list">
                                    </ul>
                                </div>
                            </div>
                            </div> --}}
                        <div class="messageInput">
                            <form action="{{ route('message.create') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex align-items-center">
                                    <span class="mr-3">Send Message to</span>
                                    <select id="users_select" name="receiver_id[]" multiple="multiple"
                                        class="form-control usersearch">
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">
                                                {{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <p class="lead emoji-picker-container">
                                    <textarea name="message" type="text" class="messageInputBox form-control connect-font "
                                        rows="4" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                                        placeholder="Write a message…" data-emojiable="true"
                                        data-emoji-input="unicode"> </textarea>
                                </p>


                                <div class="d-flex justify-content-around">
                                    <div>
                                        <input id="send_new_message" type="text" name="receiver_id" hidden>
                                        <input name="images[]" type="file" id="imageInput" multiple>
                                        <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                                class="img-fluid inputImg" width="20" /></label>
                                        <input name="gifs[]" type="file" id="imageInput" multiple>
                                        <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                class="img-fluid inputImg" width="20" /></label>
                                        <input type="file" id="gifInput" multiple>

                                        <label for="fileInput"><img src="{{ asset('svgs/attach.svg') }}"
                                                class="img-fluid inputImg" width="20" /></label>
                                        <input type="file" id="fileInput" multiple>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn sendMessage-button kkk">Send</button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="accordion">
                @unless(empty($received))
                    @foreach ($received->flatten() as $msg)
                        {{-- <div id="mobile-pills" class=""> --}}
                        <div class="card d-flex p-2 nav-link flex-row message-pills" data-id="{{ $msg->sender->id }}"
                            onclick="getValue(this,{{ $msg->id }})" id="v-pills-home-tab-{{ $msg->id }}"
                            role="button" data-toggle="collapse" href="#collapse-1-{{ $msg->id }}" aria-expanded="true"
                            aria-controls="collapse-1"
                            class="d-block position-relative text-gt text-uppercase collapsible-link py-2">
                            <div class="messageImg">
                                @if ($msg->sender->profile_pic)
                                    <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                        class="img-fluid connectImg" />
                                @else
                                    <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                                @endif
                            </div>
                            <div class="upperRow">
                                <div class="commentsContainer">
                                    <div class="upperMessageRow">
                                        <span class="name">
                                            {{ $msg->sender->name }}
                                        </span>
                                        <span class="time">
                                            {{ \Carbon\Carbon::parse($msg->created_at)->diffForHumans() }}
                                        </span>
                                    </div>
                                    <div class="commentMain">
                                        <p>{{ substr($msg->message, 0, 30) . '....' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- @unless(empty($received))
                                        @foreach ($received->flatten() as $msg) --}}
                        <div id="collapse-1-{{ $msg->id }}" class="collapse" data-parent="#accordion"
                            aria-labelledby="heading-1">
                            <div class="tab-pane fade show" id="accordion-1" role="tabpanel"
                                aria-labelledby="v-pills-home-tab-{{ $msg->id }}"
                                data-message="v-pills-home-{{ $msg->id }}">
                                <div class="card-body d-flex align-items-start flex-column">
                                    <div class="card-body message-body">
                                        <div class="chat-container ">
                                            @if ($msg->sender->profile_pic)
                                                <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                                    class="img-fluid connectImg" />
                                            @else
                                                <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                                            @endif
                                            <p class="connect-font text-left">{{ $msg->message }}</p>
                                            <span
                                                class="time-left">{{ date('Y-m-d h:i A', strtotime($msg->created_at)) }}</span>
                                                 <span class="float-right"> <a
                                                                    href="{{ route('message.index') }}" onclick="event.preventDefault();
                                                                document.getElementById(
                                                                'delete-form-{{ $msg->id }}').submit();">
                                                                <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a></span>
                                                            <form id="delete-form-{{ $msg->id }}" +
                                                                action="{{ route('message.delete', $msg->id) }}" method="post">
                                                                @csrf 
                                                            </form>
                                        </div>
                                        {{-- Messages section --}}
                                        @foreach ($msg->conversation as $convo)
                                            @if (Auth::id() == $convo->sender_id)

                                                <div class="chat-container">
                                                    @if ($msg->sender->profile_pic)
                                                        <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                                            class="img-fluid connectImg" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="img-fluid connectImg" />
                                                    @endif
                                                    <p class="connect-font text-left">{{ $convo->message }}
                                                    </p>
                                                    <div class="commentImgInner">
                                                        @unless(empty($convo->images))
                                                            @foreach (json_decode($convo->images) as $image)
                                                                <img src="{{ asset('images/activity/' . $image) }}"
                                                                    class="img-fluid main-Commentimg mr-0 mb-1 mt-1"
                                                                    style="border-radius: 0;max-width:100%" />
                                                            @endforeach
                                                        @endunless
                                                    </div>
                                                    <span
                                                        class="time-left">{{ date('Y-m-d h:i A', strtotime($convo->created_at)) }}</span>
                                                </div>
                                                            <span class="float-right"> <a
                                                                    href="{{ route('message.index') }}" onclick="event.preventDefault();
                                                                document.getElementById(
                                                                'delete-form-{{ $convo->id  }}').submit();">
                                                                <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a></span>
                                                                          <form id="delete-form-{{ $convo->id }}" +
                                                                action="{{ route('convo.delete', $convo->id) }}" method="post">
                                                                @csrf 
                                                            </form>
                                            @else
                                                <div class="chat-container">
                                                    {{-- <div class="commentImg"> --}}
                                                    @if ($msg->sender->profile_pic)
                                                        <img src="{{ asset('uploads/' . $msg->sender->profile_pic) }}"
                                                            class="img-fluid connectImg" />
                                                    @else
                                                        <img src="{{ asset('uploads/images.jpg') }}"
                                                            class="img-fluid connectImg" />
                                                    @endif
                                                    {{-- </div> --}}
                                                    <p class="connect-font text-left">{{ $convo->message }}
                                                    </p>
                                                    @unless(empty($convo->images))
                                                        @foreach (json_decode($convo->images) as $image)
                                                            <img src="{{ asset('images/activity/' . $image) }}"
                                                                class="img-fluid main-Commentimgmr-0 mb-1 mt-1"
                                                                style="border-radius: 0;max-width:100%" />
                                                        @endforeach
                                                    @endunless
                                                    <span
                                                        class="time-left">{{ date('h:i A', strtotime($convo->created_at)) }}</span>
                                                </div>
                                                            <span class="float-right"> <a
                                                                    href="{{ route('message.index') }}" onclick="event.preventDefault();
                                                                document.getElementById(
                                                                'delete-form-{{ $convo->id  }}').submit();">
                                                                <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></a></span>
                                                                          <form id="delete-form-{{ $convo->id }}" +
                                                                action="{{ route('convo.delete', $convo->id) }}" method="post">
                                                                @csrf 
                                                            </form>
                                            @endif
                                        @endforeach
                                        {{-- end of message section --}}
                                    </div>
                                    <div class="messageInput conversation inner-message">
                                        <form action="{{ route('message.create') }}" method="POST"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <p class="lead emoji-picker-container"> <textarea name="message" type="text"
                                                    class="messageInputBox form-control connect-font" rows="4"
                                                    aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm"
                                                    placeholder="Write a message…" data-emojiable="true"
                                                    data-placeholder="this div is actually empty"
                                                    data-emoji-input="unicode"></textarea></p>

                                            <div class="d-flex justify-content-around">
                                                <div>
                                                    <input type="text" name="receiver_id"
                                                        value="{{ $msg->sender->id ?? '' }}" hidden>
                                                    <input name="images[]" type="file" id="imageInput" multiple>
                                                    <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}"
                                                            class="img-fluid inputImg" width="20" /></label>
                                                    <input name="gifs[]" type="file" id="imageInput" multiple>
                                                    <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                                            class="img-fluid inputImg" width="20" /></label>
                                                    <input type="file" id="gifInput" multiple>

                                                    <label for="fileInput"><img src="{{ asset('svgs/attach.svg') }}"
                                                            class="img-fluid inputImg" width="20" /></label>
                                                    <input name="attachments[]" type="file" id="fileInput" multiple>
                                                    <input type="text" name="user_message_id" value="{{ $msg->id ?? '' }}"
                                                        hidden>
                                                </div>
                                                <div>
                                                    <button type="submit" class="btn sendMessage-button">Send</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                    <div class="card d-flex justify-content-center">
                                                        <div class="card-header">
                                                        New Message
                                                        </div>
                                                    </div>
                                                    </div> --}}
                                    {{-- <div class="tab-pane fade" id="v-pills-settings-{{ $msg->id }}" role="tabpanel" aria-labelledby="v-pills-settings-tab">.4..</div> --}}
                                </div>
                            </div>
                        </div>
                        {{-- @endforeach
                                    @endunless --}}
                        {{-- </div> --}}

                    @endforeach
                @endunless
            </div>
    </div>
    @endif
    </div>
@endsection
@section('js')
    <script>
        $(function() {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: '{{ asset('emojis/lib/img/') }}',
                popupButtonClasses: 'fa fa-smile-o',
                position: "top"
            });

            window.emojiPicker.discover();
        });
    </script>
    <script type="text/javascript">
        function getValue(id, ele) {

            var all = $(id).attr('id');
            link = all.split('-')[4];
            $('.message-tab-content .tab-pane').map(function() {
                var childid = $(this).attr('id');
                a = childid.split('-')[3];

                if (link == a) {
                    //   console.log($(this));
                    $(this).addClass('active');

                } else {
                    $(this).removeClass('active');
                }

            }).get();

            $('.nav-pills .nav-link').map(function() {
                var linkid = $(this).attr('id');
                b = linkid.split('-')[4];

                if (link == b || b == 0) {
                    //   console.log($(this));
                    $(this).addClass('active');

                } else {
                    $(this).removeClass('active');
                }
            });

            //  children.each(function(){
            //     console.log($(this).find('div').id);

            //  })
            // console.log(children);
        }

        function getval(sel) {
            // console.log(sel.value);
        }

        function searchUser(ele) {
            var typingTimer;
            var doneTypingInterval = 200;
            var $input = $('#search_input');
            var keyword = $("#search_input").val();
            var dataString = 'keyword=' + keyword;
            $input.on('keyup', function() {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            });
            // console.log($input)

            $input.on('keydown', function() {
                clearTimeout(typingTimer);
            });

            function doneTyping(e) {
                var form = $('#search')[0];
                var data = new FormData(form);


                $.ajax({
                    type: "POST",
                    enctype: 'multipart/form-data',
                    url: "{{ route('message.search') }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 1000,
                    success: function(data) {
                        console.log("SUCCESS : ", data);
                        // console.log(data)
                        var names = data

                        document.body.querySelector(".names-list").innerHTML = names.map(function(val) {
                            return `
                                            <li class = "suggested-name"onclick="showself(this)"data-id="${val.id}"data-post="${val.name}">
                                             <span class="badge rounded-pill  bg-light text-dark d-flex align-items-center">${val.name}<i class="fas fa-times"></i></span></li>
                                            `;
                        }).join("");
                    },
                    error: function(e) {
                        $("#name_list").text(e.responseText);
                        console.log("ERROR : ", e);
                        // $("#btnSubmit").prop("disabled", false);
                    }
                });
            }
        }

        function showself(ele) {
            var input = document.getElementById("search_input")
            var id = $(ele).attr('data-id');
            var name = $(ele).attr('data-post');
            document.getElementById("search_input").value = name;
            if (document.getElementById("search_input").value != "") {
                document.body.querySelector(".names-list").innerHTML = ""

            }
            // $("#name_list").hide()
            // input.onkeyup=$("#name_list").show()
            $("#message-welcome").toggle()
            $('#send_new_message').val(id)
        }

        function newMsg() {
            console.log("here")
            $("#mobile-new").toggleClass("d-none")
            $("#accordion").toggleClass("d-none")
            // $("#mobile-pills").addClass("d-none")
        }
    </script>
    <script>
        $(function() {
            $('#users_select').multiselect({
                enableFiltering: true,
                includeSelectAllOption: true,
                buttonWidth: '100%',
                widthSynchronizationMode: 'always',
                nonSelectedText: 'Select Participant ...',
                enableCaseInsensitiveFiltering: true,
                maxHeight: 400,
                filterPlaceholder: 'Search for participant...',
                onChange: function(option, checked) {
                    //  alert('Changed option ' + $(option).text() + '.');
                    // Get selected options.
                    var selectedOptions = $(option).text();
                    // document.getElementById("search_input").value = selectedOptions;
                    // document.getElementById("send_new_message").value = $(option).val();

                    console.log(selectedOptions)



                    //close dropdown after click
                }
            });
        });
    </script>

@endsection
