@extends('layouts.app_no_js')

@section('title','Background')

@section('content')
<div class="background-page">
    @include('site.includes.components.parallax',[
    'image'=> asset("images/banners/connect_banner.png"),
     'text'=> "Connect"
    ])

    @component('site.includes.components.breadcrumbs')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{ route('home') }}">Home</a>
        </li>
        <li class="breadcrumb-item">
            <a href="{{ route('connect.activity') }}">Connect</a>
        </li>
        @if((new \Jenssegers\Agent\Agent())->isDesktop())

        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">Issue Based Collaborative Learning Networks</a>
        </li>
        @else 

        <li class="breadcrumb-item active" aria-current="page">
            <a href="{{ url()->current() }}" class="active">IBCLN</a>
        </li>
        @endif
    </ol>
    @endcomponent
    @if (Session::has('course_success'))
    <script>
        jQuery(document).ready(function($) {

            $("#CourseSuccess").addClass('show');
        });
    </script>
@elseif(Session::has('course_errors'))
    <script>
        jQuery(document).ready(function($) {
            $("#CourseErrors").addClass('show');
        });
    </script>
@else
    {{-- <script>
window.addEventListener('load', function() {
if(!window.location.hash) {
  window.location = window.location + '#/';
  window.location.reload();
}
});
</script> --}}
@endif
<div class="clearfix">
    <br /> 
</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
  
    <div class="col-md-12">     
        <div class="row alignment-class-connect profile-row">
            <div class="d-flex justify-content-start">
                <div class="top-msg top_message">
                    Create a learning network by posting a question or issue or join a learning network by clicking the Follow button.  You’re welcome to engage by replying to participants questions, liking comments on threads!
                </div>
            </div>

            <div class="col-12 mt-3">
            <div class="row">
                <div class="col-3">
                    <div class="card forum-sidebar sidebar-card">
                        <article>
                            <div class="row mt-1 mr-3 ml-2">
                              
                                <div class="col-12">
                                    <a href="javascript:;" class="btn btn-overall btn_create_group btn-add_topic btn_not_inline" onclick="showTopicForm()" style="float:left"><i class="fa fa-plus"></i> Add Topic</a>
                                </div>
                            </div>
                    <ul class="list-group mt-2 ">
                    @foreach($topics as $topic)
                    <li class="list-unstyled border-0 p-2 list-nav">

                        <a href="#"  onclick="forums(<?php echo $topic->id; ?>)" class="ml-3">{{ $topic->title }}</a>
                    </li> <hr class="side-hr">

                    @endforeach
                    </ul>
                        </article>
                    </div>
              

                </div>

                <div class="col-9">

                    <div class="add-topic" id="add-topic" style="display:none">
                        <div class="card sidebar-card add-topic-card">
                            <article>
                        <form class="kt-form" method="POST" action="{{ route('forum.topic.store') }}" enctype="multipart/form-data" style="padding: 2rem;">
                            <div class="kt-portlet__body">
                        
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                              <div class="form-group">
                                <label for="name">{{ __('Topic Title') }}</label>
                                <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>
                        
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                              </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                  <button type="submit" class="btn btn-overall btn_register">Submit</button>
                                  
                                </div>
                              </div>
                        </form>
                            </article>
                    </div>
                    </div>
                    <div id="all-forums"> 
                        <div class="card sidebar-card add-topic-card">
                            <article>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-8">
                                            <form action="{{ route('connect.forum') }}"
                                                method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div
                                                        class="col-1  mt-4 mb-3 col-spacing">
                                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                                            style="float: right; width:30px" />
                                                    </div>
                                                    <div class="col-7 col-spacing">
                                                        <select name="theme" id=""
                                                            class="form-control mt-3 mb-3 minimal">
                                                            <option>Filter by Topic</option>
                                                            @foreach ($topics as $value)
                                                                <option value="{{ $value->id }}">
                                                                    {{ $value->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-2 col-spacing">
                                                        <button type="submit"
                                                            class="btn btn-overall btn_register mt-3 mb-3">Search</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="col-4">

                                            <a href="javascript:;"
                                                class="btn btn-overall btn_create_group" onclick="createForum()"> <i class="fa fa-plus"></i>
                                                Create IBCLN </a>
                                        </div>
                                    </div>

                                </div>
                            </article>
                        </div>

                        <div id="topic-forums" class="topic-forums">
                        @foreach($forums as $forum)
                        <div class="card sidebar-card add-topic-card mt-3">
                            <article>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="forum-content p-3">
                                            <p class="topic">Topic: {{$forum->topic->title}}</p>
                                            <h4>{{$forum->title}}</h4>
                                            <?php
                                            $str = $forum['description'];
                                            if (strlen($str) > 60) {
                                              $str = substr($str, 0, 180) . '...';
                                            }
                                            
                                            ?>
                                          {!!$str!!}
                                        </div>
                                    </div>
    
                                   
                                    <div class="col-4 pr-4 pt-4">

                                        @if(empty(Auth::user()->getForumStatus($forum->id)))

                                        <a href="{{route('forum.follow',['id'=>$forum->id])}}" class="btn btn-overall btn_create_group btn_forum_position" > <i class="fa fa-star"></i> Follow Discussion </a>
                            
                                        @else
                            
                                        <a href="{{route('forum.details',['id'=>$forum->id])}}" class="btn btn-overall btn_create_group btn_forum_position btn_forum_view"> <i class="fa fa-eye"></i> View More</a>
                                      @endif
                                       
                                    </div>

                                </div>
                           
                              
                              
                            </article>
                        </div>

                       
                       @endforeach
                    </div>
                       
                    </div>

                    <div id="create-forum" style="display:none">
                        <div class="card sidebar-card add-topic-card">
                            <article>
                        <form class="kt-form" method="POST" action="{{ route('forum.store') }}" enctype="multipart/form-data" style="padding: 2rem;">
                            <div class="kt-portlet__body">
                        
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                              <div class="form-group">
                                <label for="name">{{ __('Title') }}</label>
                                <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>
                        
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                              </div>

                              <div class="form-group">
                                <label for="email">{{ __('IBCLN Type') }}</label>
                                <select name="type" id="" class="form-control" required>
                                    <option value="">Select Type</option>
                                    @foreach($types as $type)
                                     <option value="{{$type['id']}}">{{$type['name']}}</option>
                        
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                @endif
                              </div>

                              <div class="form-group">
                                <label for="email">{{ __('IBCLN Topic') }}</label>
                                <select name="topic" id="" class="form-control" required>
                                    <option value="">Select Topic</option>
                                    @foreach($topics as $theme)
                                     <option value="{{$theme['id']}}">{{$theme['title']}}</option>
                        
                                    @endforeach
                                </select>
                                @if ($errors->has('theme'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('theme') }}</strong>
                                </span>
                                @endif
                              </div>


                              <div class="form-group">
                                <label for="email">{{ __('Description') }}</label>
                                <textarea id="description" class="form-control" name="description" required rows="10"></textarea>
                        
                                @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                              </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                  <button type="submit" class="btn btn-overall btn_register">Submit</button>
                                  
                                </div>
                              </div>
                        </form>
                            </article>
                    </div>
                    </div>
                  
    
            </div>
            </div>
 

            </div>

        </div>
    </div>

@else 
<div class="col-12">
    @include('site.includes.components.connect.side_menu')
  </div>
<div class="col-md-12">     
    <div class="row alignment-class-connect ">
        <p>Create a learning network by posting a question or issue or join a learning network by clicking the Follow button.  You’re welcome to engage by replying to participants questions, liking comments on threads!</p>
        <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <a href="javascript:;" class="btn btn-overall btn_create_group btn_not_inline" onclick="showTopicForm()" style="float:left"> <i class="fa fa-plus"></i> Add Topic</a>
    
                </div>
                <div class="col-6">
                    <a href="javascript:;" class="btn btn-overall btn_create_group" onclick="createForum()"> <i class="fa fa-plus"></i> Create IBCLN </a>
                </div>
            </div>
          
        </div>
        <div class="col-12 mt-3">
            <div class="row">
              

                <div class="col-12">

                    <div class="add-topic" id="add-topic" style="display:none">
                        <div class="card sidebar-card add-topic-card">
                            <article>
                        <form class="kt-form" method="POST" action="{{ route('forum.topic.store') }}" enctype="multipart/form-data" style="padding: 2rem;">
                            <div class="kt-portlet__body">
                        
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                              <div class="form-group">
                                <label for="name">{{ __('Title') }}</label>
                                <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>
                        
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                              </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                  <button type="submit" class="btn btn-overall btn_register">Submit</button>
                                  
                                </div>
                              </div>
                        </form>
                            </article>
                    </div>
                    </div>
                    <div id="all-forums"> 
                        <div class="card sidebar-card add-topic-card">
                            <article>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12">
                                            <form action="{{ route('connect.forum') }}"
                                                method="POST">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-2  mt-4 mb-3 ">
                                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                                            style="width:30px" />
                                                    </div>
                                                    <div class="col-7 ">
                                                        <select name="theme" id=""
                                                            class="form-control mt-3 mb-3 minimal">
                                                            <option>Filter by Topic</option>
                                                            @foreach ($topics as $value)
                                                                <option value="{{ $value->id }}">
                                                                    {{ $value->title }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="col-3">
                                                        <button type="submit"
                                                            class="btn btn-overall btn_register mt-3 mb-3" style="right: 17px;">Search</button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                       
                                    </div>

                                </div>
                            </article>
                        </div>

                        <div id="topic-forums" class="topic-forums">
                        @foreach($forums as $forum)
                        <div class="card sidebar-card add-topic-card mt-3">
                            <article>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="forum-content p-3">
                                            <p class="topic">Topic: {{$forum->topic->title}}</p>
                                            <h4>{{$forum->title}}</h4>
                                            <?php
                                            $str = $forum['description'];
                                            if (strlen($str) > 60) {
                                              $str = substr($str, 0, 180) . '...';
                                            }
                                            
                                            ?>
                                          {!!$str!!}
                                        </div>
                                    </div>
    
                                   
                                    <div class="col-12">

                                        @if(empty(Auth::user()->getForumStatus($forum->id)))

                                        <a href="{{route('forum.follow',['id'=>$forum->id])}}" class="btn btn-overall btn_create_group mb-4 btn_forum_details" > <i class="fa fa-plus"></i> Follow IBCLN </a>
                            
                                        @else
                            
                                        <a href="{{route('forum.details',['id'=>$forum->id])}}" class="btn btn-overall btn_create_group mb-4 btn_forum_details btn_forum_view"> <i class="fa fa-eye"></i> View More</a>
                                      @endif
                                       
                                    </div>

                                </div>
                           
                              
                              
                            </article>
                        </div>

                       
                       @endforeach
                    </div>
                       
                    </div>

                    <div id="create-forum" style="display:none">
                        <div class="card sidebar-card add-topic-card">
                            <article>
                        <form class="kt-form" method="POST" action="{{ route('forum.store') }}" enctype="multipart/form-data" style="padding: 2rem;">
                            <div class="kt-portlet__body">
                        
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        
                              <div class="form-group">
                                <label for="name">{{ __('Title') }}</label>
                                <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>
                        
                                @if ($errors->has('title'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('title') }}</strong>
                                </span>
                                @endif
                              </div>

                              <div class="form-group">
                                <label for="email">{{ __('IBCLN Type') }}</label>
                                <select name="type" id="" class="form-control" required>
                                    <option value="">Select Type</option>
                                    @foreach($types as $type)
                                     <option value="{{$type['id']}}">{{$type['name']}}</option>
                        
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('type') }}</strong>
                                </span>
                                @endif
                              </div>

                              <div class="form-group">
                                <label for="email">{{ __('IBCLN Topic') }}</label>
                                <select name="topic" id="" class="form-control" required>
                                    <option value="">Select Topic</option>
                                    @foreach($topics as $theme)
                                     <option value="{{$theme['id']}}">{{$theme['title']}}</option>
                        
                                    @endforeach
                                </select>
                                @if ($errors->has('theme'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('theme') }}</strong>
                                </span>
                                @endif
                              </div>


                              <div class="form-group">
                                <label for="email">{{ __('Description') }}</label>
                                <textarea id="description" class="form-control" name="description" required rows="10"></textarea>
                        
                                @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                              </div>
                            </div>
                            <div class="kt-portlet__foot">
                                <div class="kt-form__actions">
                                  <button type="submit" class="btn btn-overall btn_register">Submit</button>
                                  
                                </div>
                              </div>
                        </form>
                            </article>
                    </div>
                    </div>
                  
    
            </div>
            </div>
 

            </div>

    </div>
</div>
@endif
</div>
@endsection

@section('js')
<script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>

    <script>
        CKEDITOR.replace( 'description');
   
    
    </script>
<script>
    
function showTopicForm(){
    $('#add-topic').css('display','block');
    $('#all-forums').css('display','none');
    $('#create-forum').css('display','none');
    }

function createForum(){
    $('#add-topic').css('display','none');
    $('#all-forums').css('display','none');
    $('#create-forum').css('display','block');
    }
</script>

<script>
 function forums(article) {

    $('#add-topic').css('display','none');
    $('#all-forums').css('display','block');
    $('#create-forum').css('display','none');

var url = "{{ url('/profile/connect/forum/getforum') }}" + '/' + article;

$.ajax({
    url: url,
    type: 'GET',
    success: function(res) {
    $("#topic-forums").empty('');
    $.each(res.forums, function(key, value) {
   
        var status = getStatus(value.id);
         var link = '';
        if(status === 0){
            var route = "{{url('/profile/connect/forum/follow')}}" + '/' + value.id;

        link+= ' <a href="'+ route +'" class="btn btn-overall btn_create_group btn_forum_position" > <i class="fa fa-star"></i> Follow Discussion </a>';
         }else{

         var route = "{{url('/profile/connect/forum')}}" + '/' + value.id;
        link = '<a href="'+ route +'" class="btn btn-overall btn_create_group btn_forum_position"> <i class="fa fa-eye"></i> View More</a>'
         }
  
        console.log(status);
     
        $(".topic-forums").append('<div class="card sidebar-card add-topic-card mt-3"><article><div class="row"><div class="col-8"><div class="forum-content p-3">'+
              '<p class="topic">Topic:' + value.topic.title+ '</p>'+
              '<h4>'+value.title+'</h4>'+ value.description.slice(0,180) +
              ' </div></div>'+
              ' <div class="col-4 pr-4 pt-4">'+link+'</div></div></article></div>');
    });


    }

});
}
</script>

<script>

function getStatus(id){
var user = {{Auth::user()->id}}
var url = "{{ url('/profile/connect/forum/getStatus') }}" + '/' + id + '/' + user;

$.ajax({
    url: url,
    type: 'GET',
    success: function(res) {
    console.log(res);
     
    return res;
    }

});

}

</script>
@if((new \Jenssegers\Agent\Agent())->isDesktop())
<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[5];
  
  
  console.log(url);
  
  // Loop all menu items
  $('.list-group .list-nav').each(function(){
  
  // select href
  var href = $(this).find('a').attr('href');

  
 
  link = href.split('/')[4];
 
 
  console.log(link);
  
  // Check filename
  if(link === url){
  console.log($(this))
   // Add active class
    $(this).addClass('active');
   $(this).find('a').addClass('active')
  
  }
 });
  });
  </script>


<script>
    $(document).ready(function() {
  
        // Get current page URL
        var url = window.location.href;
  
  
  
        // remove # from URL
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
        // remove parameters from URL
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
        // select file name
        url = url.split('/')[4];
  
  
        // console.log(url);
  
        // Loop all menu items
        $('.navbar-nav .nav-item').each(function() {
  
            // select href
            var href = $(this).find('a').attr('href');
  
            link = href.split('/')[4];
  
            // Check filename
            if (link === 'connect') {
  
                // Add active class
                $(this).addClass('active');
            }
        });
    });
  </script>
@else 


<script>
    $(document).ready(function() {
  
  // Get current page URL
  var url = window.location.href;
  
  // remove # from URL
  url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
  
  // remove parameters from URL
  url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
  
  // select file name
  url = url.split('/')[5];
  
  
  console.log(url);
  
  // Loop all menu items
  $('.connect_mobile_menu .list-nav').each(function(){
  
  // select href
  var href = $(this).find('a').attr('href');

  
 
  link = href.split('/')[4];
 
 
  console.log(link);
  
  // Check filename
  if(link === url){
  console.log($(this))
   // Add active class
    $(this).addClass('active');
   $(this).find('a').addClass('active')
  
  }
 });
  });
  </script>



@endif
@endsection
