@extends('layouts.app_no_js')

@section('title', 'Groups')

@section('content')

    @php
    $details_panels = [['id' => '1', 'name' => 'All CoPs', 'level' => '0'], ['id' => '2', 'name' => 'AAC CoPs', 'level' => '0'], ['id' => '3', 'name' => 'Participants CoPs', 'level' => '0']];
    @endphp
    <div class="background-page">
        @include('site.includes.components.parallax',[
        'image'=> asset("images/banners/connect_banner.png"),
        'text'=> "Connect"
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ url()->current() }}" class="active"> Communities of Practice</a>
                </li>
            </ol>
        @endcomponent

        @if (Session::has('course_success'))
            <script>
                jQuery(document).ready(function($) {

                    $("#CourseSuccess").addClass('show');
                });
            </script>
        @elseif(Session::has('course_errors'))
            <script>
                jQuery(document).ready(function($) {
                    $("#CourseErrors").addClass('show');
                });
            </script>
        @else
            {{-- <script>
  window.addEventListener('load', function() {
      if(!window.location.hash) {
          window.location = window.location + '#/';
          window.location.reload();
      }
  });
  </script> --}}
        @endif
        <div class="clearfix">
            <br />
        </div>

        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-md-12">
                <div class="row alignment-class-connect profile-top">
                    <p style="margin-left:1rem;margin-top: 3rem;">In this space you can share ideas, start conversations and upload resources
                        for access by other participants in the  Communities of Practice.</p>
                </div>
                <div class="row alignment-class-connect profile-lower">
                    <div class="col-3">
                        @include('site.includes.components.connect.side_menu')
                    </div>

                    <div class="col-md-9 col-12">
                        
                            <div class="container-fluid" style="padding-left:0">
                                <div class="row">
                                    <div class="col-12 d-none d-sm-block pr-3 ">
                                        <div class="card sidebar-card group-card">
                                            <article>
                                                <div class="col-12">
                                                    <div class="row">
                                                        <div class="col-5">
                                                            <ul class="nav nav-pills session-tabs group-tabs pl-3"
                                                                role="tablist">
                                                                @foreach ($details_panels as $key => $item)

                                                                    @if ($key === array_key_last($details_panels))

                                                                        <li role="presentation"
                                                                            class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                                            <a href="#home{{ $item['id'] }}"
                                                                                aria-controls="home" role="tab"
                                                                                data-toggle="tab">{!! $item['name'] !!}</a>

                                                                        </li>

                                                                    @elseif($key === array_key_first($details_panels))
                                                                        <li role="presentation"
                                                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right">
                                                                            <a href="#home{{ $item['id'] }}"
                                                                                aria-controls="home" role="tab"
                                                                                data-toggle="tab" style="text-align: center;
                                                              ">{!! $item['name'] !!}</a>
                                                                        </li>

                                                                    @else
                                                                        <li role="presentation"
                                                                            class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right tabz-spacing">
                                                                            <a href="#home{{ $item['id'] }}"
                                                                                aria-controls="home" role="tab"
                                                                                data-toggle="tab">{!! $item['name'] !!}</a>
                                                                        </li>
                                                                    @endif
                                                                @endforeach
                                                            </ul>
                                                        </div>

                                                        <div class="col-7">

                                                        

                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        <div class="col-8">
                                                                            <form action="{{ route('connect.groups') }}"
                                                                                method="POST">
                                                                                @csrf
                                                                                <div class="row">
                                                                                    <div
                                                                                        class="col-1  mt-4 mb-3 col-spacing">
                                                                                        <img src="{{ asset('images/icons/filter_icon.svg') }}"
                                                                                            style="float: right; width:30px" />
                                                                                    </div>
                                                                                    <div class="col-7 col-spacing">
                                                                                        <select name="theme" id=""
                                                                                            class="form-control mt-3 mb-3 minimal">
                                                                                            <option>Filter by Theme</option>
                                                                                            @foreach ($groupTheme as $value)
                                                                                                <option
                                                                                                    value="{{ $value->id }}">
                                                                                                    {{ $value->title }}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>

                                                                                    <div class="col-2 col-spacing">
                                                                                        <button type="submit"
                                                                                            class="btn btn-overall btn_register mt-3 mb-3">Search</button>
                                                                                    </div>
                                                                                </div>
                                                                            </form>

                                                                        </div>
                                                                        <div class="col-4">

                                                                            <a href="{{ route('group.form') }}"
                                                                                class="btn btn-overall btn_join_community btn_create_group">
                                                                                Create Group </a>
                                                                        </div>
                                                                    </div>

                                                                </div>






                                                        </div>

                                                    </div>

                                                </div>




                                            </article>
                                        </div>
                                    </div>

                                </div>
                            </div>
                  

                        <div class="row">

                            <div role="tabpanel " class="session_details-panel mt-4">

                                <div class="tab-content">
                                    @foreach ($details_panels as $item)
                                        <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}"
                                            id="home{{ $item['id'] }}" class="active">


                                            @if ($item['id'] == 1)
                                                <div class="row">
                                                    @foreach ($groups as $group)

                                                        <div class="col-md-4 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>

                                            @elseif($item['id'] == 2)
                                                <div class="row">
                                                    @foreach ($AACgroups as $group)

                                                        <div class="col-md-4 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>
                                            @elseif($item['id'] == 3)
                                                <div class="row">
                                                    @foreach ($Partgroup as $group)

                                                        <div class="col-md-3 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>
                                            @endif


                                        </div>
                                    @endforeach
                                </div>
                            </div>


                        </div>

                    </div>


                </div>

            </div>


        @else
            <div class="col-md-12">
                <div class="row ">
                    <div class="col-12">
                         @include('site.includes.components.connect.side_menu')
                    </div>

                    <div class="col-md-12 col-12">
               
                        
                            <div class="container-fluid">
                                <div class="row group-row">
                                  
                                        <div class="card sidebar-card group-card">
                                            <article>
                                                <a href="{{ route('group.form') }}"
                                                class="btn btn-overall btn_join_community btn_not_inline mt-3 mb-4"> Create Group
                                            </a>

                                                <ul class="nav nav-pills nav-fill session-tabs group-tabs" role="tablist">
                                                    @foreach ($details_panels as $key => $item)

                                                        @if ($key === array_key_last($details_panels))

                                                            <li role="presentation"
                                                                class=" nav-item {{ $item['id'] == 1 ? 'active' : '' }} tabz-spacing">
                                                                <a href="#home{{ $item['id'] }}" aria-controls="home"
                                                                    role="tab" data-toggle="tab">{!! $item['name'] !!}</a>

                                                            </li>

                                                        @elseif($key === array_key_first($details_panels))
                                                            <li role="presentation"
                                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right-group">
                                                                <a href="#home{{ $item['id'] }}" aria-controls="home"
                                                                    role="tab" data-toggle="tab" style="text-align: center;
                                                      ">{!! $item['name'] !!}</a>
                                                            </li>

                                                        @else
                                                            <li role="presentation"
                                                                class="nav-item {{ $item['id'] == 1 ? 'active' : '' }} border-right-group tabz-spacing">
                                                                <a href="#home{{ $item['id'] }}" aria-controls="home"
                                                                    role="tab" data-toggle="tab">{!! $item['name'] !!}</a>
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>

                                              

                                            </article>
                                        </div>
                                   

                                </div>
                            </div>
                

                        <div class="row">

                            <div role="tabpanel " class="session_details-panel mt-4">

                                <div class="tab-content">
                                    @foreach ($details_panels as $item)
                                        <div role="tabpanel" class="tab-pane {{ $item['id'] == 1 ? 'active' : '' }}"
                                            id="home{{ $item['id'] }}" class="active">


                                            @if ($item['id'] == 1)
                                                <div class="row">
                                                    @foreach ($groups as $group)

                                                        <div class="col-md-4 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>

                                            @elseif($item['id'] == 2)
                                                <div class="row">
                                                    @foreach ($AACgroups as $group)

                                                        <div class="col-md-4 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>
                                            @elseif($item['id'] == 3)
                                                <div class="row">
                                                    @foreach ($Partgroup as $group)

                                                        <div class="col-md-3 col-12">
                                                            @include('site.includes.components.connect.groups-cards',
                                                            ['group' => $group])
                                                        </div>

                                                    @endforeach
                                                </div>
                                            @endif


                                        </div>
                                    @endforeach
                                </div>
                            </div>


                        </div>


                    </div>


                </div>

            </div>
        @endif
    @endsection

    @section('js')

    <script>
        $(document).ready(function() {
      
            // Get current page URL
            var url = window.location.href;
      
      
      
            // remove # from URL
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
      
            // remove parameters from URL
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
      
            // select file name
            url = url.split('/')[4];
      
      
            // console.log(url);
      
            // Loop all menu items
            $('.navbar-nav .nav-item').each(function() {
      
                // select href
                var href = $(this).find('a').attr('href');
      
                link = href.split('/')[4];
      
                // Check filename
                if (link === 'connect') {
      
                    // Add active class
                    $(this).addClass('active');
                }
            });
        });
      </script>

    @endsection
