<div class="reply-reply" id="response-reply-{{ $reply->id }}" style="display:none ">
<form action="{{ route('forum.reply.reply.create', [$activity->id, $reply->id]) }}" method="POST">
    @csrf
    
        {{-- <div class="d-flex">
    <div class="connectImgLeft">
            <img src="{{ asset('uploads/' . $activity->user->profile_pic) }}" class="img-fluid connectImg"/>
        </div>
            <div class="inputRight">
            <input type="text" name="comment" class="connectInput" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="Reply" data-emojiable="true" data-emoji-input="unicode">
            <label for="imageInput"><img src="{{ asset('svgs/images.svg') }}" class="img-fluid inputImg" width="20"/></label>
                <input name="images[]" type="file" id="imageInput" multiple>
            <label for="gifInput"><img src="{{ asset('svgs/gif.svg') }}" class="img-fluid inputImg" width="20"/></label>
                <input name="gifs[]" type="file" id="gifInput" multiple>
            <img src="{{ asset('svgs/emoji.svg') }}" class="img-fluid inputImg inputIcon" width="20"/>
        </div>
    </div> --}}
        <div class="container-fluid mt-3" id="response-reply-{{ $reply->id }}" style="display: block;">
            <div class="main  d-flex">
                <div class="card " style="border: 0px;width:100%">
                    <div class="row no-gutters">
                        <div class="col-2 col-md-1">
                            @if ($reply->user->profile_pic)
                                <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}"
                                    class="card-img avatar-small rounded-circle disc-img" />
                            @else
                                <img src="{{ asset('uploads/images.jpg') }}"
                                    class="card-img avatar-small rounded-circle disc-img" />
                            @endif
                        </div>
                        <div class="col-10">

                            <div class="card-body card-small pb-0">
                                <div class="card-text">
                                    <input type="text" name="id" value="5" hidden="" id="replycomment_id">
                                    <textarea id="replycomment_reply" class="form-control text-input" name="comment" placeholder="Reply"
                                        data-emojiable="true" data-emoji-input="unicode"></textarea>
                                </div>
                                <label for="replyimageInput"><img src="{{ asset('svgs/images.svg') }}"
                                        class="img-fluid inputImg" width="20" /></label>
                                <input name="images[]" type="file" id="replyimageInput" multiple>
                                <label for="replygifInput"><img src="{{ asset('svgs/gif.svg') }}"
                                        class="img-fluid inputImg" width="20" /></label>
                                <input name="gifs[]" type="file" id="replygifInput" multiple>

                                <div id="reply-upload-filename" style="font-size: 12px;"></div>
                                {{-- <img src="{{ asset('svgs/emoji.svg') }}"
                                                                    class="img-fluid inputImg inputIcon" width="20" /> --}}
                                <div class="submitbuttons">
                                    <button type="button" class="btn btn-outline-dark btn-cancel"
                                        onclick="showHide('response-reply-{{ $reply->id }}">Cancel</button>
                                    <button type="submit" class="btn connect-button">Reply</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

</form>

        @unless(empty($reply->replyReplies))
            @foreach ($reply->replyReplies as $reply)
                <div class="card-body d-flex pt-0">
                    <div class="commentImg">
                        @if ($reply->user->profile_pic)
                            <img src="{{ asset('uploads/' . $reply->user->profile_pic) }}" class="img-fluid connectImg" />
                        @else
                            <img src="{{ asset('uploads/images.jpg') }}" class="img-fluid connectImg" />
                        @endif
                    </div>
                    <div class="upperRow">
                        <div class="commentsContainer">
                            <div class="responseCommentRow">
                                <span class="name">
                                    {{ $reply->user->username }}
                                    <small class="time">
                                        {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
                                    </small>
                                </span>
                                @if ($reply->user_id == Auth::user()->id)
                                    <div class="dropdown">
                                        <i id="dropdownMenuButton" class="fa fa-ellipsis-h" data-toggle="dropdown"></i>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item edit-item" href="#"><i class="fas fa-edit"
                                                    data-toggle="modal"
                                                    data-target="#practice_modal-{{ $reply->id }}"><span
                                                        class="icon-text">Edit Post</span></i></a>
                                            <a class="dropdown-item delete-item" href="{{ route('connect.activity') }}"
                                                onclick="event.preventDefault(); document.getElementById('deletes-form-{{ $reply->id }}').submit();">
                                                    <i class="fas fa-trash" data-toggle="tooltip" data-placement="bottom"
                                                        title="Delete">
                                                        <span class="icon-text">Delete Post</span>
                                                    </i>
                                            </a>
                                            <form id="deletes-form-{{ $reply->id }}" action="{{ route('connect.forum.reply.delete', [$reply->id]) }}" method="post">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                @endif
                                {{-- <span class="time">
        {{ \Carbon\Carbon::parse($reply->created_at)->diffForHumans() }}
        </span> --}}
                            </div>
                            <div class="commentMain">
                                <p>{{ $reply->comment }}</p>
                            </div>

                            <div class="commentImgInner">
                                @unless(empty($reply->images))
                                    @foreach (json_decode($reply->images) as $image)
                                        <img src="{{ asset('images/activity/' . $image) }}"
                                            class="img-fluid main-Commentimg" />
                                    @endforeach
                                @endunless
                            </div>
                        </div>
                    </div>
                </div>

                <form id="update-form-{{ $reply->id }}" +
                    action="{{ route('connect.forum.reply.update', $reply->id) }}" method="post">
                    @csrf
                    <div class="modal fade edit-modal" id="practice_modal-{{ $reply->id }}" tabindex="-1"
                        role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header edit-modal-header">
                                    <p class="modal-title name" id="exampleModalLabel">Edit Post</p>
                                    <button type="button" class="close" data-dismiss="modal"
                                        aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <textarea type="text" name="body" id="name" value="" class="form-control"
                                        placeholder="">{{ $reply->comment }}</textarea>
                                </div>
                                <div class="modal-footer justify-content-end">
                                    {{-- <button type="button" class="btn btn-secondary"
                                        data-dismiss="modal">Close</button> --}}
                                    <button type="submit" class="btn btn-primary">Post</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
        
            @endforeach
        @endunless
  
    </div>