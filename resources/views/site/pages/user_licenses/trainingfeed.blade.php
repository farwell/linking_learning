@php
$configLms = config()->get("settings.lms.live");
 @endphp

@extends('layouts.app')

@section('title', 'My Courses')

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/profile_header.png'),
            'text' => 'My Profile',
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ route('profile.courses') }}" class="active">My Training Feed</a>
                </li>
            </ol>
        @endcomponent
        @if (Session::has('redirectMessage'))
            <script>
                jQuery(document).ready(function($) {
                    $("#notificationModal").addClass('show');
                });
            </script>
        @else
            <script>
                window.addEventListener('load', function() {
                    if (!window.location.hash) {
                        window.location = window.location + '#training-feed';
                        window.location.reload();
                    }
                });
            </script>
        @endif

        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('includes.components.timeline')

                </div>
            </div>


            <div class="row no-gutters">

                <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                    <div class="container-fluid">

                        <!-- Filter section -->

                        <!-- End of filter section -->
                        <!-- Course list -->
                        <div class="clearfix"><br /></div>
                        <!-- Course list -->
                        <div class="tab-content">
                            <div id="home" class="container-fluid tab-pane active">
                                <div class="course-card detail-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help">
                                            </div>
                                            @if (!count($licenses))
                                                <div class="text-center col-12">
                                                    <br /><br /><br />

                                                    <p style="text-align:center">
                                                        Follow colleagues and engage each other on your training progress.
                                                    </p>

                                                </div>
                                            @endif

                                            @foreach ($licenses as $license)
                                                @php
                                                    
                                                    $name = ucwords($license->user->first_name . ' ' . $license->user->last_name);
                                                    
                                                    if (empty($license->user->profile_pic)) {
                                                        $image = '/images/profile/images.jpg';
                                                    } else {
                                                        $image = '/uploads/' . $license->user->profile_pic;
                                                    }
                                                    if (empty($license->user->affiliation)) {
                                                        $position = 'Position';
                                                    } else {
                                                        $position = $license->user->affiliation;
                                                    }
                                                    
                                                    $unique = uniqid();
                                                @endphp

                                                <training-feed class="d-none d-lg-flex" title="{{ $name }}"
                                                    image="{{ $image }}" course="{{ $license->course->name }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_date)->format('M dS Y') }}"
                                                    position="{{ $position }}" action="{{ $license->action }}"
                                                    link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M dS Y') }}"
                                                    end="{{ Carbon\Carbon::parse($license->course->start)->format('M dS Y') }}"
                                                    enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                    likes="{{ $license->getLikes($license->user->id, $license->course->id) }}"
                                                    likeaction="{{ $license->getLikeAction($license->user->id, $license->course->id) }}"
                                                    unique="{{ $unique }}" follow="{{ $license->user->id }}"
                                                    courseid="{{ $license->course->id }}"
                                                    followers="{{ $license->getfollowers($license->user->id) }}"
                                                    commentcount="{{ $license->getCommentCount($license->user->id, $license->course->id) }}"
                                                    commentaction="{{ $license->getCommentAction() }}"
                                                    status="{{ $license->getStatuses($license->user->email) }}"
                                                    progress="{{ $license->getGrades($license->user->email) * 100 }}%"
                                                    prev-score="0"
                                                    current-score="{{ $license->getGrades($license->user->email) }}">
                                                </training-feed>

                                                <training-feed-mobile class="d-block d-lg-none" title="{{ $name }}"
                                                    image="{{ $image }}" course="{{ $license->course->name }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_date)->format('M dS Y') }}"
                                                    position="{{ $position }}" action="{{ $license->action }}"
                                                    link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M dS Y') }}"
                                                    end="{{ Carbon\Carbon::parse($license->course->start)->format('M dS Y') }}"
                                                    enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                    likes="{{ $license->getLikes($license->user->id, $license->course->id) }}"
                                                    likeaction="{{ $license->getLikeAction($license->user->id, $license->course->id) }}"
                                                    unique="{{ $unique }}" follow="{{ $license->user->id }}"
                                                    courseid="{{ $license->course->id }}"
                                                    followers="{{ $license->getfollowers($license->user->id) }}"
                                                    commentcount="{{ $license->getCommentCount($license->user->id, $license->course->id) }}"
                                                    commentaction="{{ $license->getCommentAction() }}"
                                                    status="{{ $license->getStatuses($license->user->email) }}"
                                                    progress="{{ $license->getGrades($license->user->email) * 100 }}%"
                                                    prev-score="0"
                                                    current-score="{{ $license->getGrades($license->user->email) }}">
                                                </training-feed-mobile>
                                            @endforeach
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>


    </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
