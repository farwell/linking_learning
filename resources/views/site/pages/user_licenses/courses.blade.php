@php
$configLms = config()->get("settings.lms.live");
 @endphp
@extends('layouts.app')

@section('title', 'My Profile')

@section('content')
    <div class="courses-page">
        @include('site.includes.components.parallax', [
            'image' => asset('images/banners/profile_header.png'),
            'text' => 'My Profile',
        ])

        @component('site.includes.components.breadcrumbs')
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    <a href="{{ route('profile.courses') }}" class="active">My Sessions</a>
                </li>
            </ol>
        @endcomponent



        <div class="profile-row">

            <div class="row">

                <div class="col-lg-12  col-md-12 col-sm-12 col-12 scroll-tabs">
                    @include('site.includes.components.timeline')

                </div>
                <div class="offset-md-2 col-lg-10  col-md-10 col-sm-12 col-12">
                    @include(
                        'site.includes.components.user-courses-filter'
                    )
                </div>
            </div>

            <div class="row no-gutters">


                <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-flex p-3">
                    <div class="container-fluid">

                        <!-- Filter section -->
                        <!-- End of filter section -->
                        <!-- Course list -->
                        <div class="tab-content">
                            <div id="home" class="container-fluid tab-pane active">
                                <div class="course-card detail-card">
                                    <article>
                                        <div class="course-content">
                                            <div class="help">
                                            </div>
                                            @if (!count($licenses))
                                                <div class="row justify-content-md-center no-gutters">
                                                    <div class="text-center col-12">
                                                        <p>
                                                            Your list of courses is currently empty.
                                                        </p>

                                                    </div>
                                                </div>
                                            @endif
                                            @php $count = 0;
                                                
                                                $today = Carbon\Carbon::today()->format('d-m-y');
                                                
                                            @endphp
                                            @foreach ($licenses as $license)
                                                @php $count ++; 
                                               
                                                
                                                @endphp

                                                <my-course class="d-none d-lg-flex"
                                                    cert_title="{{ $license->course->name }}"
                                                    title="{{ $license->course->name }}"
                                                    image="{{ $license->course->course_image_uri }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_date)->format('M jS Y') }}"
                                                    status="{{ $license->status }}" progress="{{ $license->grade * 100 }}%"
                                                    prev-score="0" current-score="{{ $license->grade }}"
                                                    action="{{ $license->action }}"
                                                    link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    end="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                    owner="{{ Auth::user()->name }}" count="{{ $count }}"
                                                    evaluate="{{ $license->course->name }}" evalaction="#"
                                                    today="{{ $today }}"></my-course>


                                                <my-course-mobile class="d-inline-block d-lg-none "
                                                    cert_title="{{ $license->course->name }}"
                                                    title="{{ $license->course->name }}"
                                                    image="{{ $license->course->course_image_uri }}"
                                                    date="{{ Carbon\Carbon::parse($license->enrolled_date)->format('M jS Y') }}"
                                                    status="{{ $license->status }}" progress="{{ $license->grade * 100 }}"
                                                    prev-score="0" current-score="{{ $license->grade }}"
                                                    action="{{ $license->action }}"
                                                    link="{{ $configLms['LMS_BASE'] . '/courses/' . $license->course->id . '/courseware' }}"
                                                    start="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    end="{{ Carbon\Carbon::parse($license->course->start)->format('M jS Y') }}"
                                                    enrolled="{{ $license->getEnrollments($license->course->id) }}"
                                                    evaluate="{{ $license->course->name }}" evalaction="#"
                                                    count="{{ $count }}" today="{{ $today }}">
                                                </my-course-mobile>
                                            @endforeach

                                        </div>
                                    </article>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>
    <!-- End of future courses list -->

    <div class="clearfix"><br /></div>
@endsection
