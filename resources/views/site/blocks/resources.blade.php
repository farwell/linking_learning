@php
use App\Models\Resource;

 if((new \Jenssegers\Agent\Agent())->isDesktop()){
    $resources  = Resource::published()->orderBy('position', 'desc')->limit(4)->get();
 }elseif((new \Jenssegers\Agent\Agent())->isMobile()){
    $resources  = Resource::published()->orderBy('position', 'desc')->limit(1)->get();
 }

@endphp

@if (Route::current()->getName() === 'home')
<section class="general-section">
    <div class="container-fluid">
    <div class="col-12 ">
        <div class="">
		<div class="hrHeading">
			<h2 class="line-header text-center">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="whatYouGetListing text-center">
					{!! $block->translatedinput('description') !!}
					
				</div>
			</div>
            @if((new \Jenssegers\Agent\Agent())->isDesktop())
            <div class="col-10"></div>
            <div class="col-2 ">
              
                <div class="row alignment-class">
               
                <a href="{{route('resources')}}" class="text-grey full-list mb-3">See Full Resources List > </a> 
               
                </div>
              
            </div>
            @else 
            <div class="col-6"></div>
            <div class="col-6">
              
                <div class="row">
               
                <a href="{{route('resources')}}" class="text-grey full-list mb-3">See Full Resources List > </a> 
               
                </div>
              
            </div>


            @endif
           
		</div>
    </div>
	</div>
    @if((new \Jenssegers\Agent\Agent())->isDesktop())
    <div class="row alignment-class">
    @else 
    <div class="row ">
    @endif

        @foreach($resources as $resource)
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            @include('site.includes.components.resources',['item'=>$resource])
        </div>
       @endforeach
    </div>
    
   
</div>
</section>
@endif