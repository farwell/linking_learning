@php
use App\Repositories\ClientRepository;
use App\Models\Client;
//$sponsors = app(SponsorRepository::class)->getItems([], ['position' => 'asc'], 10);
$sponsors  = Client::published()->orderBy('position', 'asc')->get();
$ratio = $image->ratio ?? 1/2;
$ratioPercentage = ($ratio * 100);

@endphp

<section class="main-page clients">
	<div class="container carousel-container" >
		<div class="hrHeading">
			<h4 class="heading line-header light">
				{{ $block->translatedinput('title') }}
			</h4>
		</div>
		@if ($sponsors && (count($sponsors) > 0))
		<div class="carousel-wrapper" data-behavior="carousel">
			<div class="owl-carousel" data-carousel-list id="sponsorsCarousel">
				@foreach ($sponsors as $item)
					<div class="item" data-carousel-item>
						<a href="#" data-behavior="modal" id="successTrigger" data-modal="modal-sponsor-{{($item->slug)}}" class="carouselLink">
							
							<div class="captionImage">
								@if($imageSponsor = $item->present()->image)
									{!! (new \App\Helpers\Front)->responsiveImage([
										'options' => [
											'src' => $imageSponsor->src ?? '',
											'w' => $imageSponsor->width ?? 20,
											'h' => $imageSponsor->height ?? 20,
											'ratio' => $imageSponsor->ratio ?? 9/10,
											'fit' => 'crop'
										],
										'html' => [
											'alt' => $imageSponsor	->altText ?? ''
										],
										'sizes' => [
											[ 'media' => 'small', 'w' => 50 ],
											[ 'media' => 'medium',  'w' => 50 ],
											[ 'media' => 'large', 'w' => 50 ],
											[ 'media' => 'xlarge',  'w' => 50 ],
											[ 'media' => 'xxlarge',  'w' => 300 ]
										]
									]) !!}
									@endif
							</div>
						</a>
					</div>

				@endforeach
			</div>
			
		</div>
		@endif

	</div>
	
</section>
