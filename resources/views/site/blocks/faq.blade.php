@if((new \Jenssegers\Agent\Agent())->isDesktop())
<section class="general-section" style="margin:0;">
    <div class="col-md-12 ">
	
			<div class="col-lg-12">
				<div class="row mt-3 mb-5" >
                    <div class="col-12 ">
						<div class="ml-5" style="margin-top:2rem">
							
                            @foreach ($block->children as $item)
                            @include('site.blocks.faq_repeater', $item)
                        @endforeach

						{{-- @php dd($block->translatedinput('url_video'));@endphp --}}
						</div>
					</div>
					
			</div>
		</div>
	</div>

</section>

@else 

<section class="general-section" style="margin:0;">

		
			<div class="col-lg-12">
				<div class="row mt-3 mb-5" >
                    <div class="col-12 col-12">
						<div >
							
							@foreach ($block->children as $item)
                            @include('site.blocks.faq_repeater', $item)
                        @endforeach

						</div>
					</div>
					
			</div>
		
	</div>

</section>


@endif