@if($item->input('display') === "not_authenticated" && !Auth::check())
<div class="accordion accordion__item faq-item" id="myAccordion">
    <div class="accordion-item">
        <h2 class="accordion-header" id="{{'heading'.$item->id}} ">
            <button type="button" class="accordion-button accordion__item-button white-bg collapsed" data-bs-toggle="collapse" data-bs-target="{{'#collapse'.$item->id}}">{{$item->translatedinput('faq_title')}}</button>									
        </h2>
        <div id="{{'collapse'.$item->id}}" class="accordion-collapse accordion-item__desc collapse" data-bs-parent="#myAccordion">
            <div class="card-body accordion__item-body">
                @if($item->file('video'))
                <video controls playsinline muted id="bgvid">
                    <source src="{{$item->file('video') }}" type="video/mp4">
                </video>


                @elseif($item->translatedinput('faq_description'))
                 

                {!! $item->translatedinput('faq_description') !!}

              

               @endif

                
            
            </div>
        </div>
    </div>
   
</div>

@elseif( $item->input('display') === "not_authenticated" ||  $item->input('display') === "authenticated"  && Auth::check())

<div class="accordion accordion__item faq-item" id="myAccordion">
    <div class="accordion-item">
        <h2 class="accordion-header" id="{{'heading'.$item->id}} ">
            <button type="button" class="accordion-button accordion__item-button white-bg collapsed" data-bs-toggle="collapse" data-bs-target="{{'#collapse'.$item->id}}">{{$item->translatedinput('faq_title')}}</button>									
        </h2>
        <div id="{{'collapse'.$item->id}}" class="accordion-collapse accordion-item__desc collapse" data-bs-parent="#myAccordion">
            <div class="card-body accordion__item-body">
                @if($item->file('video'))
                <video controls playsinline muted id="bgvid">
                    <source src="{{$item->file('video') }}" type="video/mp4">
                </video>


                @elseif($item->translatedinput('faq_description'))
                 

                {!! $item->translatedinput('faq_description') !!}

               @endif

                
            
            </div>
        </div>
    </div>
   
</div>





@endif