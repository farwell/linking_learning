@php
$variant = $cta->input('variant');
$url = $cta->translatedinput('url');
$linkText = $cta->translatedinput('link_text');
$isBanner = (isset($isBanner) && $isBanner) ? true : false;
@endphp

@if ($variant && ($variant === 'orange_bg'))
@if (Route::current()->getName() === 'home')
		<a href="{{ $url }}" class="btn btn-overall orange_bg">
			<img src="{{asset('images/icons/community-top.svg')}}" /> {{ $linkText }}
			
		</a>
@else 
<a href="{{ $url }}" class="btn btn-overall orange_bg">
	{{ $linkText }}
	<i class="fa fa-arrow-right "></i>
</a>

@endif
	

@elseif ($variant && ($variant === 'green_bg'))
@if (Route::current()->getName() === 'home')
		<a href="{{ $url }} " class="btn btn-overall green_bg">
			<img src="{{asset('images/icons/engage-icon.svg')}}" /> {{ $linkText }}
			
		</a>
@else 
<a href="{{ $url }} " class="btn btn-overall green_bg">
	{{ $linkText }}
	<i class="fa fa-arrow-right "></i>
</a>

@endif
@endif
