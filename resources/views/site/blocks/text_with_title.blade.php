
<section class="general-section">
	<div class="row">
		<div class="col-lg-12">
			<div class="whatYouGetListing">
    <div class="col-md-12 ">
		<div class="hrHeading">
			<h2 class="line-header ">
				{{ $block->translatedinput('title') }}
			</h2>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="whatYouGetListing ">

					{!! $block->translatedinput('description') !!}
					
				</div>
			</div>
		</div>

    
	</div>
</div>
</div>
</div>

</section>

	
