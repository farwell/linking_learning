<section class="main-page clients" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="header-section">
            <h1 class="text-white"><img src="{{asset('images/banners/4649.svg')}}" alt="PWD,persons with disabilities">&nbsp {{ $block->translatedinput('pwd_title') }}</h1>
            {!! $block->translatedinput('pwd_text') !!} 
            </div>
        </div>
    </div>
</section>