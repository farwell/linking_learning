@php
use App\Models\Testimonial;
//$sponsors = app(SponsorRepository::class)->getItems([], ['position' => 'asc'], 10);
$testimonials  = Testimonial::published()->orderBy('position', 'asc')->get();


@endphp



<section class="main-page clients" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
    
                <div class="header-section" >
                <h1 style="color:#fff ">{{ $block->translatedinput('testimonial_title') }}</h1>
                <h3 style="color:#fff ">{{$block->translatedinput('testimonial_description') }}</h3>
                </div>
    
    
    <div id="customers-testimonials" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        @if ($testimonials && (count($testimonials) > 0))
        @foreach($testimonials as $key => $item)
        <li data-target="#customers-testimonials" data-slide-to="{{$key}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
      
        @endforeach
        @endif
      </ol>
      <div class="carousel-inner">
        @if ($testimonials && (count($testimonials) > 0))
         @foreach($testimonials as $key => $item)
         
        <div class="carousel-item {{ $key == 0 ? ' active' : '' }}">
                <div class="testimonial-content">
                <div class="body-section">
                    <blockquote style="color:#fff ">
                    <span class="leftq quotes">&ldquo;</span>{!! $item->description !!}<span class="rightq quotes">&bdquo; </span>
                    </blockquote>
                </div>
    
                <div class="body-section" style="color:#fff ">
                    <span class="testimony-owner">{!! $item->title !!}</span>
                </div>
                </div>
        </div>
        @endforeach
        @endif
      </div>
      <a class="carousel-control-prev" href="#customers-testimonials" role="button" data-slide="prev" style="color:#fff ">
        <span class="carousel-control-prev-icon" aria-hidden="true" style="color:#fff "></span>
        <span class="sr-only" style="color:#fff ">Previous</span>
      </a>
      <a class="carousel-control-next" href="#customers-testimonials" role="button" data-slide="next" style="color:#fff ">
        <span class="carousel-control-next-icon" aria-hidden="true" style="color:#fff "></span>
        <span class="sr-only" style="color:#fff ">Next</span>
      </a>
    </div>
    
            </div>
        </div>
    </div>
    </section>
    