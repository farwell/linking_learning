
@extends('layouts.app')
@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection
@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login auth-register w-100 d-none d-md-block">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login register">

                <div class="card-body">
                    <h1>Register</h1>
                     <div class="col-md-12">
                     
                        <div class="form-login">
                        @include('site.includes.auth.register')
                       </div>
                      

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
$(document).on('click','#checkbox_info',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#platforminfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#platforminfo').val('0'); 
    }
  
});



$(document).on('click','#checkbox_contact',function(){
  var isChecked = $(this).is(':checked');
  if(isChecked == true){
       $('#consentinfo').val('1');
    }
    else if($(this).prop("checked") == false){
        $('#consentinfo').val('0'); 
    }
  
});
</script>


@endsection