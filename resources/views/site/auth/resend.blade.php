
@extends('layouts.app')

@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection


@section('content')

@if(session()->has('login_error'))
<div class="form-group row">
    <div class="col-md-12">
        <div class="form-check">
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {!! session()->get('login_error') !!}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        </div>
    </div>
</div>
@php session()->forget('login_error') @endphp
@endif

<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login">

                <div class="card-body">
                     <div class="col-md-12">
                      <div class="row">

                    
                       <div class="col-md-10">

                        
                        <div class="form-login mt-5">

                            <p style="margin-left:19%">Please enter registration email address</p>
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
   
                    <form method="POST" action="{{ route('resend.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-overall btn_register">
                                    {{ __('Send Activation Code') }}
                                </button>
                            </div>
                        </div>
                    </form>
                        </div>
                       </div>
                      

                       <div class="col-md-2"></div>

                      </div>

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
               $('#course_information').val('1');
            }
            else if($(this).prop("checked") == false){
                $('#course_information').val(' '); 
            }
        });
    });
</script>


@endsection