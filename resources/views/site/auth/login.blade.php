
@extends('layouts.app')

@section('js')
<script src="{{asset('js/auth.js')}}"></script>
@endsection


@section('content')
<section class="auth-page ">
<div class="bg-overlay">
    <div class="row justify-content-center">
        <div class="col-lg-6 auth-login w-100 d-none d-md-block">
            {{-- <div class="card">
            <h1 class="text-white">Register</h1>
            <div class="card-body"> 
                    @include('site.includes.auth.register')
            </div>
            </div> --}}
        </div>
        <div class="col-lg-6">
            <div class="card login">

                <div class="card-body">
                     <div class="col-md-12">
                      <div class="row">

                       <div class="col-md-2"></div>

                       <div class="col-md-8">
                        <h1>Welcome</h1>
                        <h2>Login to your account</h2>
                        <div class="form-login">
                        @include('site.includes.auth.login')
                       </div>
                       </div>
                      

                       <div class="col-md-2"></div>

                      </div>

                     </div>
                  
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
               $('#course_information').val('1');
            }
            else if($(this).prop("checked") == false){
                $('#course_information').val(' '); 
            }
        });
    });
</script>
 <script>
    // Change the type of input to password or text
        function Toggle() {
            var temp = document.getElementById("login_password");
            if (temp.type === "password") {
                temp.type = "text";
            }
            else {
                temp.type = "password";
            }
        }
</script>
<script>
    $(window ).on("load", function() {
    
      $("#notificationModal").modal('show');
     
    });
    </script>
@endsection