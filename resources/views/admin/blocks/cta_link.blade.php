@formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    'translated' => true,
])
@formField('input', [
    'name' => 'url',
    'label' => 'URL',
	'translated' => true,
])

@formField('medias', [
    'name' => 'icon',  //role
    'label' => 'Icon',
	'withVideoUrl' => false,
	'max' => 1,
])

@formField('select', [
    'name' => 'variant',
    'label' => 'Variant',
    'placeholder' => 'Select a variant',
    'options' => [
        [
            'value' => 'orange_bg',
            'label' => 'Orange Background'
        ],
        [
            'value' => 'green_bg',
            'label' => 'Green Background'
        ],
        [
            'value' => 'white_bg',
            'label' => 'White Background'
        ],
        [
            'value' => 'white_bg_orange_border',
            'label' => 'White Background Orange Border'
        ]
    ]
])
