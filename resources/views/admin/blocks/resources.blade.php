@formField('input', [
    'name' => 'title',
    'label' => 'Title',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'description',
	'label' => 'Description',
	'maxlength' => 1000,
    'translated' => true,
])