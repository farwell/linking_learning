@formField('input', [
    'name' => 'title',
	'label' => 'Video Title',
	'hint' => 'Video Title',
    'translated' => true,
])
@formField('input', [
	'name' => 'description',
	'translated' => true,
	'type' => 'textarea',
	'label' => 'Video description',
	'translated' => true,
	'maxlength' => 200,
])
@formField('files', [
	'name' => 'video',
	'label' => 'Video',
	'noTranslate' => true,
])
@formField('input', [
    'name' => 'url_video',
    'label' => 'URL',
    'translated' => true,
])

@formField('input', [
    'name' => 'textTitle',
    'label' => 'Text Title',
    'translated' => true,
])

@formField('wysiwyg', [
    'name' => 'textdescription',
	'label' => 'Text Description',
	'toolbarOptions' => [
        'bold',
        'italic',
        ['list' => 'bullet'],
        ['list' => 'ordered'],
        [ 'script' => 'super' ],
        [ 'script' => 'sub' ],
        'link',
        'clean'
    ],
	'maxlength' => 300,
	'translated' => true,

])

@formField('repeater', ['type' => 'cta_link'])