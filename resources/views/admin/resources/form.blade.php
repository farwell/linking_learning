@extends('twill::layouts.form')

@section('contentFields')

@formField('select', [
	'name' => 'resource_theme_id',
	'label' => 'Resource Theme',
	'placeholder' => 'Select Resource Theme',
	'options' => collect($resourceThemeList ?? ''),
])
@formField('select', [
	'name' => 'resource_type_id',
	'label' => 'Resource Type',
	'placeholder' => 'Select Resource Type',
	'options' => collect($resourceTypeList ?? ''),
])
@formField('select', [
	'name' => 'resource_country',
	'label' => 'Resource Country',
	'placeholder' => 'Select Resource country of origin',
	'searchable' => true,
	'options' => collect($countryList ?? ''),
])
@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Short Description',
        'placeholder' => 'Introduction Text',
		'translated' => true,
])



@formField('medias',[
    'name' => 'resource_image',
    'label' => 'Resources image',
])

@formField('files', [
	'name' => 'resource_audio',
	'label' => 'Audio',
	'note' => 'This applies for audio resources',
])
@formField('files', [
	'name' => 'resource_video',
	'label' => 'Video',
	'note' => 'This applies for video resources',
])

@formField('files', [
		'name' => 'download_resource',
		'label' => 'Downloadable PDF',
		'note' => 'This applies for articles and publications resources',
	])
@formField('input', [
		'name' => 'external_link',
		'label' => 'Link to External Resource',
		'translated' => true,
])

@formField('input', [
		'name' => 'external_text',
		'label' => 'Link text',
		'translated' => true,
])

@formField('input', [
		'name' => 'resource_category',
		'label'=> 'Resource Creator Role',
		'default' => Auth::user()->role,
		'readonly' =>true,
])
@stop