<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': fields.name && fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': fields.name && fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.course.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>



<div class="form-group row align-items-center" :class="{'has-danger': errors.has('more_info'), 'has-success': fields.more_info && fields.more_info.valid }">
    <label for="more_info" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.more_info') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <quill-editor v-model="form.more_info" :options="wysiwygConfig" name="more_info"  id="more_info"/>
            {{-- <textarea class="form-control" v-model="form.more_info" v-validate="''" id="more_info" name="more_info" :options="wysiwygConfig"></textarea> --}}
        </div>
        <div v-if="errors.has('more_info')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('more_info') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('effort'), 'has-success': fields.effort && fields.effort.valid }">
    <label for="effort" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.effort') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.effort" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('effort'), 'form-control-success': fields.effort && fields.effort.valid}" id="effort" name="effort" placeholder="{{ trans('admin.course.columns.effort') }}">
        <div v-if="errors.has('effort')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('effort') }}</div>
    </div>
</div>


<div class="form-group row align-items-center" :class="{'has-danger': errors.has('sessions_id'), 'has-success': fields.sessions_id && fields.sessions_id.valid }">
    <label for="sessions_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.course.columns.sessions_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="form.sessions_id" :options="{{ $sessions->toJson() }}" placeholder="Select Session" label="topic" track-by="id" :multiple="false"></multiselect>
        {{-- <input type="text" v-model="form.sessions_id" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('sessions_id'), 'form-control-success': fields.sessions_id && fields.sessions_id.valid}" id="sessions_id" name="sessions_id" placeholder="{{ trans('admin.course.columns.sessions_id') }}"> --}}
        <div v-if="errors.has('sessions_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('sessions_id') }}</div>
    </div>
</div>

<div class="form-check row" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="status" type="checkbox" v-model="form.status" v-validate="''" data-vv-name="status"  name="status_fake_element">
        <label class="form-check-label" for="status">
            {{ trans('admin.course.columns.status') }}
        </label>
        <input type="hidden" name="status" :value="form.status">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>


