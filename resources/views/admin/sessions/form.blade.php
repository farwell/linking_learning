@extends('twill::layouts.form')

@section('contentFields')
@formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Session Agenda',
        'toolbarOptions' => [
      ['header' => [2, 3, 4, 5, 6, false]],
      'bold',
      'italic',
      'underline',
      'strike',
      ["script" => "super"],
      ["script" => "sub"],
      "blockquote",
      "code-block",
      ['list' => 'ordered'],
      ['list' => 'bullet'],
      ['indent' => '-1'],
      ['indent' => '+1'],
      ["align" => []],
      ["direction" => "rtl"],
      'link',
      'image',
      "clean",
    ],
        'placeholder' => 'Session Agenda',
		'maxlength' => 2000,
		
])

@formField('date_picker', [
		'name' => 'start_time',
		'label' => 'Session Date',
	])

@formField('input', [
        'name' => 'duration',
        'label' => 'Duration',
        'maxlength' => 100
    ])
@formField('medias',[
    'name' => 'session_image',
    'label' => 'Session Cover Image',
])

@formField('files', [
	'name' => 'session_video',
	'label' => 'Session Video',
	'noTranslate' => true,
])


@formField('input', [
        'name' => 'session_video_url',
        'label' => 'Session video Link',
        'maxlength' => 1000
    ])
@stop
