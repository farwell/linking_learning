@extends('twill::layouts.form')

@section('contentFields')
@formField('input', [
		'name' => 'key',
		'label' => 'Key',
		'maxlength' => 100
	])

@formField('input', [
        'name' => 'header_title',
        'label' => 'Header Title',
        'translated' => true,
        'maxlength' => 500
    ])
    @formField('input', [
        'name' => 'description',
        'label' => 'Description',
        'translated' => true,
        'maxlength' => 500
    ])

@formField('medias',[
    'name' => 'hero_image',
    'label' => 'Hero image',
])

@formField('input', [
    'name' => 'link_text',
    'label' => 'Link Text',
    'maxlength' => 200,
    'translated' => true,
])
@formField('input', [
    'name' => 'url',
    'label' => 'URL',
	'translated' => true,
])

@formField('select', [
    'name' => 'variant',
    'label' => 'Variant',
    'placeholder' => 'Select a variant',
    'options' => [
        [
            'value' => 'orange_bg',
            'label' => 'Orange Background'
        ],
        [
            'value' => 'white_bg',
            'label' => 'White Background'
        ],
        [
            'value' => 'white_bg_orange_border',
            'label' => 'White Background Orange Border'
        ]
    ]
])

@formField('block_editor', [

    'blocks' => ['gallery', 'image_with_text', 'image_top_text', 'quote', 'paragraph','text_btn','text_with_title','track_record','testimonial','client','session_in_panel','resources','video_text_grid','blog','faq']

])
@stop




@section('sideFieldsets')
    @formFieldset([ 'id' => 'seo', 'title' => 'SEO'])
        @formField('input', [
            'name' => 'meta_title',
            'label' => 'Title',
            'translated' => true,
            'maxlength' => 100,
        ])

        @formField('input', [
            'name' => 'meta_description',
            'label' => 'Description',
            'translated' => true,
            'maxlength' => 200,
        ])
    @endformFieldset
@stop
