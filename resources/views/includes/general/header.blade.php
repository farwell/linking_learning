@php
$configLms = config()->get("settings.lms.live");
 @endphp

<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
  <a class="navbar-brand" href="{{ route('home') }}">
    @if($dark)
    <img src="{{asset('images/logo-white.png')}}" alt="logo" width="151" height="56" />
    @else
    <img src="{{asset('images/logo_v2.png')}}" alt="logo" width="151" height="56"/>
    @endif
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left Side Of Navbar -->
    <ul class="navbar-nav mr-auto">

    </ul>

    <!-- Right Side Of Navbar -->
    <ul class="navbar-nav ml-auto">

      <?php foreach (config('app-constants.nav') as $el) : ?>
        <li class="nav-item">
          <?php 
            $pieces = substr($_SERVER['REQUEST_URI'],1,6);
            ?>
          @if (\Route::current()->getName() == $el['route'] || (strpos($pieces, substr($el['route'],1,5)) !== false))
          <a class="nav-link active" href="{{route($el['route'])}}"> {{$el['title']}}</a>
          @else
          <a class="nav-link" href="{{route($el['route'])}}"> {{$el['title']}}</a>
          @endif
        </li>
      <?php endforeach; ?>
      <!-- Authentication Links -->
      @guest
      <li class="nav-item auth">
        <a class="nav-link" href="" data-toggle="modal" data-target="#registerLoginModal">{{ __('Register/Login') }}</a>
      </li>
      @else
      <li class="nav-item">
        <a class="nav-link wish" href="{{ route('wishlist.show') }}">My Cart @if($uw_count)<span class="wish-count"><span>{{$uw_count}}</span></span>@endif</a>
      </li>
      <li class="nav-item dropdown show">
        <a href="#" clas="dropdown-toggle" style="margin: 0;display: block; "role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <ppic current="{{ Auth::user()->ppic ?: asset('images/user.png')}}" style="width:30px;height:30px;" ></ppic>
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="{{route('profile.courses')}}"><i class="fas fa-user"></i>&nbsp;&nbsp;My Profile</a>
        @if(Auth::user()->is_admin)
        <a class="dropdown-item" href="/admin/companies" target="_blank"><i class="fas fa-users-cog"></i>&nbsp;&nbsp;Administrator Section</a>
        @endif
        @if(Auth::user()->is_ikadmin)
        <a class="dropdown-item" href="/admin/courses" target="_blank"><i class="fas fa-users-cog"></i>&nbsp;&nbsp;Administrator Section</a>
        @endif
        @if(Auth::user()->is_company_admin)
        <a class="dropdown-item" href="/admin/user_licenses/reports" target="_blank"><i class="fas fa-users-cog"></i>&nbsp;&nbsp;Company Administration</a>
        @endif
        <a class="dropdown-item logout-edx" data-logout-url="{{ $configLms['LMS_BASE'] }}/logout" href="{{ route('logout') }}"><i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;Logout</a>
      </div>
      </li>
      @endguest
      <li class="nav-item search">
        <search action="{{route('courses')}}"></search>
      </li>
    </ul>
  </div>
</nav>