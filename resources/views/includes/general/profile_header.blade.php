<div class="row mobile d-md-none">
  <div class="col-3 col-sm-3">
    <div class="col-3 col-sm-3 ppic" data-toggle="collapse" href="#sidebar" role="button" aria-expanded="false" aria-controls="sidebar">
      <img src="{{asset('images/user.png')}}"  width="151" height="56" style="margin-top:20px"/>
    </div>
  </div>
  <div class="col-6 col-sm-6 profile-brand">
    <a href="{{ route('home') }}">
      <img src="{{asset('images/icons/logo.jpg')}}" alt="logo" width="100" height="56" />
    </a>
  </div>
  <div class="col-3 col-sm-3">
  <button class="navbar-toggler" type="button" onclick="openSideNav()">
    <i class="fas fa-bars"></i>
  </button>
</div>
</div>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <ul class="main-menu">


    @if(!Auth::check())
      <li class="nav-item ">

        <a class="nav-link" href="{{route('mobile.login')}}" title="Login">{{ __('Login') }}</a>
      </li>
    @else

      <?php foreach (config('app-constants.profile') as $el) : ?>
      <li class="nav-item "><a class="nav-link" href="{{route($el['route'])}}/" title="{{$el['title']}}"><span class="text-underline">{{$el['title']}}<span></a></li>
      <?php endforeach; ?>
      <li class="nav-item "><a class="nav-link menu-breaker" href=""><span></span></a></li>

      <li class="nav-item "><a class="nav-link" href="{{ route('profile.edit') }}/" title="Edit Profile"><span class="text-underline">Edit Profile<span></a></li>
      <!-- <li class="nav-item "><a class="nav-link" href="{{ route('profile.password.set') }}/"><span class="text-underline">Reset Password<span></a></li> -->
      @if(Auth::user()->is_admin)
        <li class="nav-item "> <a class="nav-link" href="/admin/companies/" target="_blank" title="Admin Section"><span class="text-underline">Admin Section<span></a></li>
      @endif
      @if(Auth::user()->is_company_admin)
        <li class="nav-item "><a class="nav-link" href="/admin/user_licenses/reports/" target="_blank" title="Company Admin"><span class="text-underline">Company Admin<span></a></li>
      @endif
      <li class="nav-item "><a class="nav-link" href="{{ route('user.logout') }}/"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
          <span>{{ __('Logout') }}</span>
        </a></li>

      <form id="logout-form" action="{{ route('user.logout') }}" method="POST" style="display: none;">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      </form>

    @endif
  </ul>
</div>

<div class="row desktop d-md-none">
  <div class="col-lg-2 col-md-3 profile-brand d-none d-md-block">
    <a href="{{ route('home') }}">
      <img src="{{asset('images/logo-white.png')}}" alt="logo"  width="151" height="56"/>
    </a>
  </div>
  <div class="col-lg-10 col-md-9">
   
  </div>
</div>
