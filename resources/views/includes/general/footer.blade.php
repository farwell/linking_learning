
    <footer>
        <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text desktop">
                    <ul>
                        <li><span>Quick Links</span></li>
                          <?php foreach (config('app-constants.nav') as $el) : ?>
                            <li>
                              <a  class="bordered-left" href="{{route($el['route'])}}/"> {{$el['title']}}</a>
                            </li>
                          <?php endforeach; ?>
                    </ul>
                </div>
                <div class="footer-text">
                    <ul>
                        <li><span>Contact Us</span></li>
                        <li><a href="" class="bordered-left">erevuka@farwell-consultants.com</a></li>
                        <li><a href="" class="bordered-left">+254 (0) 734 22 33 88</a></li>
                        <li><a href="" class="bordered-left">Karen, Hardy, Milima Road Hse 199</a></li>
                        <li><a href="" class="bordered-left">Mon - Fri 8AM to 5PM</a></li>
                    </ul>
                </div>
                <hr class="line-footer"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text">
                    <div class="footer-span">
                    <span>Follow Us</span>
                    </div>
                    <div class="footer-right footer-icons">
                    <a href="https://facebook.com/ErevukaSmart"><img src="{{ asset('images/icons/SM_-_Facebook.png') }}"></a>
                    <a href="https://www.twitter.com/ErevukaSmart"><img src="{{ asset('images/icons/SM_-_Twitter.png') }}"></a>
                    <a href="https://www.youtube.com/channel/UCBJWF7LwTqyXZtSNQQukjiA"><img src="{{ asset('images/icons/SM_-_Youtube.png') }}"></a>
                    </div>
                </div>
                <hr class="line-footer"/>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text">
                    <ul>
                        <li><small>Copyright &copy; <?php echo date("Y"); ?> Erevuka All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="www.farwell-consultants.com" target="_blank">Farwell Innovation Ltd</a></small></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </footer>