@php
$pages_array = array(
     array(
  'image' => 'images/profile/farwell-consultants.png',
  'title' => 'Farwell Consultants',
  'content' => 'Farwell Consultants Limited was set up in 2011 and is based in Nairobi. We focus on providing technical and project management consultancy services to profit and non-profit organisations. Our clients include Airtel Group, Intel, Ericsson, Plan International, DT Dobie, Danya International and Africa Analysis.'
     ),

     array(
  'image' => 'images/profile/farwell.png',
  'title' => 'Farwell Innovations Limited',
  'content' => 'Farwell Innovations Limited is a subsidiary of Farwell Consultants Limited and specialises in the development of bespoke, cloud based software solutions. We are passionate about solving complex problems through the development of efficient, user friendly solutions. Our clients include the World Bank, African Legal Support Facility, United Nations, European Union, Sri Lanka Banks\' Association, Kenya Bankers Association, KCB and Barclays Bank. Since 2015 we have developed a number of industry leading eLearning solutions and information portals providing training courses into the banking, mining and aviation sectors.',
     ),

     array(
  'image' => 'images/profile/new.png',
  'title' => 'Lapid Leaders Africa',
  'content' => ' Lapid Leaders Africa is a community of young, outstanding leaders who are dedicated to being Africa’s Change Agents. Through the Lapid Leaders Experience, the programme unlocks the leadership potential of Africa’s youth by equipping them with the 21st century skills that enable them to be leaders in the marketplace.',
    ),
    
  );
$number = count($pages_array) + 1; 
@endphp
<section class="company-section text-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="header-section">
            <h1>Partners</h1>
            </div>
            <div class="body-section">
                <div class="row companies">
                  @php $x = 0; @endphp
                  @foreach($pages_array as $page)
                  @php $x++; @endphp
                    <div class="col-md-4">
                        <div class="card partner">
                          <div class="card-header">
                              <img src="{{asset($page['image'])}}">
                          </div>
                          <div class="card-body">
                            <h5 class="card-title">{{$page['title']}}</h5>
                            <p class="card-text">
                              @php 
                              $string = strip_tags($page['content']);
                              if (strlen($string) > 110) {

                                  // truncate string
                                  $stringCut = substr($string, 0, 110);
                                  $endPoint = strrpos($stringCut, ' ');

                                  //if the string doesn't contain any space then it will cut without word basis.
                                  $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                                  $string .= '... <br/><a href="#" data-toggle="modal" data-target="#companyModal'.$x.'">Read More</a>';
                              }
                              echo $string;
                             @endphp</p>
                          </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div></section>
@php $y = 0; @endphp
@foreach($pages_array as $page)
<!-- Modal -->
@php $y++; @endphp
<div class="modal fade bd-example-modal-lg" id="companyModal{{$y}}" tabindex="-1" role="dialog" aria-labelledby="companyModal{{$y}}" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content company-modal">
            <div class="modal-header">
              <img src="{{asset($page['image'])}}">
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
              <h3 class="modal-title">{{$page['title']}}</h3>
              <p>{{$page['content']}}</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="text-grey" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Go Back</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach