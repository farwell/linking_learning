<section class="main-page testimonials" >
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="header-section">
            <h1>Testimonials</h1>
            <h3>What learners say about our learning solutions</h3>
            </div>


<div id="customers-testimonials" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#customers-testimonials" data-slide-to="0" class="active"></li>
    <li data-target="#customers-testimonials" data-slide-to="1"></li>
    <li data-target="#customers-testimonials" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
            <div class="testimonial-content">
            <div class="body-section">
                <blockquote>
                <span class="leftq quotes">&ldquo;</span>Excellent user interface and platform! I found the registration easy.<span class="rightq quotes">&bdquo; </span>
                </blockquote>
            </div>

            <div class="body-section">
                <span class="testimony-owner">Mary Angela Siro</span>
            </div>
            </div>
    </div>
    <div class="carousel-item">
            <div class="testimonial-content">
            <div class="body-section">
                <blockquote>
                <span class="leftq quotes">&ldquo;</span>Excellent platform. I found the material useful.<span class="rightq quotes">&bdquo; </span>
                </blockquote>
            </div>

            <div class="body-section">
                <span class="testimony-owner">Jane Gititu</span>
            </div>
            </div>
    </div>
    <div class="carousel-item">
            <div class="testimonial-content">
            <div class="body-section">
                <blockquote>
                <span class="leftq quotes">&ldquo;</span>Good Platform.  I would recommend it to my colleagues.<span class="rightq quotes">&bdquo; </span>
                </blockquote>
            </div>

            <div class="body-section">
                <span class="testimony-owner">Brian Sang</span>
            </div>
            </div>
    </div>
    <div class="carousel-item">
            <div class="testimonial-content">
            <div class="body-section">
                <blockquote>
                <span class="leftq quotes">&ldquo;</span>Excellent training. The knowledge I gained was helpful.<span class="rightq quotes">&bdquo; </span>
                </blockquote>
            </div>

            <div class="body-section">
                <span class="testimony-owner">Martha Muhia</span>
            </div>
            </div>
    </div>
    <div class="carousel-item">
            <div class="testimonial-content">
            <div class="body-section">
                <blockquote>
                <span class="leftq quotes">&ldquo;</span>Good learning experience. It was a good training and I have learnt alot.<span class="rightq quotes">&bdquo; </span>
                </blockquote>
            </div>

            <div class="body-section">
                <span class="testimony-owner">Seth Cherongis</span>
            </div>
            </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#customers-testimonials" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#customers-testimonials" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

        </div>
    </div>
</div>
</section>
