<section class="device-banner">
    <div class="row justify-content-center">
        <div class="col-md-4 offset-md-1">
            <div class="banner-section-text">
                <h1><strong>Erevuka is a Swahili word meaning to acquire knowledge or to become smart or clever.</strong></h1>
                <p>Erevuka eLearning combines the best breed of learning technology, reporting and user experience to deliver a cutting edge solution in training.</p>
                <div class="body-section">
                    <a href="#" class="btn btn-overall purple-borderd" onclick="videoPlay()"><span>Take a Tour<i class="fas fa-long-arrow-alt-right"></i></span></a>
                </div>
            </div>
        </div>
        <div class="col-md-7">
                <div class="image-section">              
                <div id="mobile-orientation"><img src="{{asset('images/banners/Erevuka_-_Mobile_-_Landscape.png')}}"/></div>
                <div id="mobile-portrait"><img src="{{asset('images/banners/Erevuka_-_Mobile_-_Portrait.png')}}"/></div>
                <div id="tablet"><img src="{{asset('images/banners/Erevuka_-_Tablet_-_Portrait.png')}}"/></div>
                <div id="laptop"><img src="{{asset('images/banners/Erevuka_-_PC.png')}}"/></div>
                </div>
        </div>
    </div>
</section>