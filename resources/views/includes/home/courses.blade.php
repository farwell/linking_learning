<section class="main-page clients">
    <div class="row justify-content-center">
        <div class="col">
            <div class="header-section">
                <h1 class="text-white">&nbsp &nbsp Featured Courses</h1><br>
                    <div class="row">                                            
                             @foreach($courses as $course)
                             <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                            @include('includes.components.course-card',['course'=>$course])
                            </div>
                            @endforeach
                            @if(count($courses) > 3)
                            <div class="row">{{ $courses->links() }}</div>
                            @endif
                    </div>
                    <div class="body-section">
                       <div class="call-to-action" >
                          <a href="https://erevuka.org/courses" class="btn btn-overall purple" style="background-color: #05b4cb;"><span>More Courses<i class="fas fa-long-arrow-alt-right"></i></span></a>
                       </div>
                    </div>
            </div>
        </div>
    </div>    
</section> 


                    