<section class="main-page tour-section">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="modal-video-play">
                <div class="body-section">
                    <span class="modal-trigger" onclick="videoPlay()">
                        <div class="outline">
                        <span class="fa-wrap">
                            <i class="fas fa-play"></i>
                        </span>
                        </div>
                    </span>
                </div>
                <div class="header-section">
                <h1 class="text-white">Maximise your Learning Potential</h1>
                <h5 class="text-white">Companies across three continents are using Erevuka to seal their skill gaps. </h5>
                </div>
            </div>
        </div>
    </div>
</section>