<section class="main-page clients">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <div class="header-section">
            <h1 class="text-white">Our Clients</h1>
            </div>
            <client-carousel></client-carousel>

        </div>
    </div>
</section>