<section class="main-page track-record">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="body-section">
            <h1>Our Track Record</h1>
            <h5 class="text-purple">Giving you the tools to leverage innovation and unlock your teams potential</h5>
            </div>

            <div class="body-section">
                <div class="row">
                <div class="col-md-4 counter-section">
                  <h1><span class="count" id="count">100,000</span> +</h1>
                  <h5>Staff Trained</h5>
                </div>
                <div class="col-md-8">
                <p class="counter-right">Through our eLearning technology, we have delivered courses to 100,000+ users across three continents in the last 5 years. We have partnered with Banks, Development Finance Institutions and Training Organisations to deliver high quality training courses.</p>
                </div>
                </div>
            </div>

        </div>
    </div>
</div>
</section>

