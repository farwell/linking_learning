
                <form method="POST" action="{{ route('general_corporate.store') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                            <input id="name" type="text" placeholder="Your Name*" class="form-control @error('name') is-invalid @enderror" name="corporate_name" required>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                <input id="email" type="email" placeholder="Your Work Email*" class="form-control @error('email') is-invalid @enderror" name="corporate_email" required>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                <input id="phone" type="tel" placeholder="Your Phone*" class="form-control @error('phone') is-invalid @enderror" name="corporate_phone" required>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                            <input id="organisation" type="text" placeholder="Your Organisation Name" class="form-control @error('organisation') is-invalid @enderror" name="organisation" required>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <select class="form-control" name="services" id="services">
                                      <option value="" selected disabled hidden>I am interested in....</option>
                                      <option value="platform provision">I am interested in your eLearning platform</option>
                                      <option value="content conversion">I am interested in course content conversion</option>
                                      <option value="other">Others</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                    <textarea class="form-control" placeholder="What are your training needs?" rows="5" id="comment" name="corporate_message"></textarea>
                                </div>
                            </div>
                        </div>
                         <div class="form-group row">
                              <div class="col-md-8 offset-md-2">
                                  <span>{!! captcha_img() !!}</span>
                                  <button type="button" class="btn-refresh btn btn-overall white" class="reload" id="reload">
                                    &#x21bb;
                                  </button>
                              </div>
                         </div>
                         <div class="form-group row">
                             <div class="col-md-8 offset-md-2">
                                 <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                             </div>
                         </div>
                        <div class="form-group row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-check">
                                <button type="submit"  class="btn btn-overall white">
                                    {{ __('Send') }}
                                </button>
                                </div>
                            </div>
                        </div>
                </form>
             
