
<form method="POST" action="{{ route('general_messages.store') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <div class="form-check">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Your Name*" autocomplete="off">
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
        <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <div class="form-check">
            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" placeholder="Your Phone Number*" autocomplete="off">
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <div class="form-check">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Your Email Address" autocomplete="off">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>



    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <div class="form-check input-group mb-3">
              <textarea name="message" id="message" class="form-control" placeholder="Go ahead, we are listening..."  rows="4" cols="50" required></textarea>
            @error('message')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <span>{!! captcha_img() !!}</span>
                <button type="button" class="btn-refresh btn btn-overall purple-borderd" class="reload" id="reload">
                    &#x21bb;
                </button>
        </div>
    </div>

    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <button type="submit" class="btn btn-overall purple-borderd">Send</button>
        </div>
    </div>
</form>