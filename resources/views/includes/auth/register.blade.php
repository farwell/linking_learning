@if(session()->has('register_error'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session()->get('register_error') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_error') @endphp
@endif
@if(session()->has('register_success'))
    <div class="form-group row">
        <div class="col-md-12">
            <div class="form-check">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session()->get('register_success') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            </div>
        </div>
    </div>
    @php session()->forget('register_success') @endphp
@endif
<form method="POST" action="{{ route('user.create') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
            <input id="fname" type="text" class="form-control" name="fname" placeholder="First Name" autocomplete="off">
            @error('fname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
            <input id="lname" type="text" class="form-control" name="lname" placeholder="Last Name" autocomplete="off">
            @error('lname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
            <input id="phone" type="tel" class="form-control" name="phone" placeholder="Phone Number" autocomplete="off">
            @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>    
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check">
            <input id="email" type="email" class="form-control" name="email" placeholder="Email Address" autocomplete="off">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check input-group mb-3">
            <input id="password" type="password" placeholder="Password" class="pass form-control" name="password" required autocomplete="new-password"> 
            <div class="input-group-append pass-view">
            <i class="far fa-eye"></i>
            <i class="far fa-eye-slash" style="display: none;"></i>
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <div class="col-md-10">
            <div class="form-check input-group mb-3">
            <input id="password-confirm" type="password" placeholder="Password Confirmation" class="pass form-control" name="password_confirmation" required autocomplete="new-password">
            <div class="input-group-append pass-view">
            <i class="far fa-eye"></i>
            <i class="far fa-eye-slash" style="display: none;"></i>
            </div>
            @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            </div>
        </div>
    </div>
    <div class="form-group row justify-content-center">
        <input type="hidden" id="platforminfo"name="register_receive_course_information[0]" value="0" />
        <input type="checkbox" id="platforminfo" name="register_receive_course_information[1]"  value="Bike">
        <label for="exampleCheck1" class="form-check-label" style="display: initial">I wish to receive course information and program updates from this site</label>

    </div>
    <div class="form-group row">
        <div class="col-md-8 offset-md-2">
            <button type="submit" class="btn btn-overall white">Register <i aria-hidden="true" class="fas fa-long-arrow-alt-right"></i></button>
        </div>
    </div>
</form>
