<div class="num-counter">
  <strong class="title">
    @if(isset($prefix))
    <span class="prefix">{{$prefix}}</span>
    @endif
    <div class="number counter-value" data-count="{{$number}}">0</div>
    @if(isset($suffix))
    <span class="suffix">{{$suffix}}</span>
    @endif
  </strong>
  <div class="content"><p>{{$content}}</p></div>
</div>
