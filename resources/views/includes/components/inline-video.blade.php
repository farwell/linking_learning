<!-- Background Image Parallax -->
<div class="inline-video-jarallax">
  <img class="jarallax-img" src="{{asset('images/social-teaharvest.jpg')}}" alt="Social tea harvest" />
  <div class="content">
        <div data-aos="zoom-in" class="modal-video-play">
          <span class="modal-trigger" data-toggle="modal" data-target="#youtube">
            <div class="outline">
              <span class="fa-wrap" data-toggle="tooltip" data-placement="top" title="Watch our Introduction Video">
                <i class="fas fa-play"></i>
              </span>
            </div>
          </span>
        </div>
    <div class="wrap">
      <h5>Check out this <b>short video</b> on the background of our courses</h5>
    </div>
  </div>
</div>