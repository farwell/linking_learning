<!-- Heading bordered -->
<div class="heading-bordered">
  {{ $slot }}
  <div class="heading-border">
    <div class="border-inner"></div>
  </div>
</div>
