<!-- Background Image Parallax -->
<div class="inline-jarallax">
    <img class="jarallax-img" src="{{ $image }}" alt="" >
    <div class="container">
      {{ $slot }}
    </div>
</div>
