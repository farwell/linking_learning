<?php
$pins = [
  ['title' => 'Occupational Health and Safety during construction', 'content' => 'Ignorant subcontractors'],
  ['title' => 'Environmental Management during operation', 'content' => 'water quality, minimum flow'],
  ['title' => 'Biodiversity', 'content' => 'fish migration interrupted'],
  ['title' => 'Biodiversity', 'content' => 'endemic species in national park'],
  ['title' => 'Community', 'content' => 'expectations with regard to jobs'],
  ['title' => 'Land issues', 'content' => '100 families to resettle'],
  ['title' => 'Cultural heritage', 'content' => 'ancient temple almost inundated'],
  ['title' => 'Community health and safety', 'content' => ' risk of landslides']
]
?>

<div class="container snapshot">
  <div id="toolcontainer"></div>
  <div class="snapshot-container">
    <div class="img-bg">
      <img src="{{asset('/images/hydro-power-dam.jpg')}}" />
    </div>
    <div class="pins">
      @foreach($pins as $index=>$pin)
      <div class="pin">
        <div class="pindicator d-none d-md-flex" data-container="#toolcontainer" data-toggle="tooltip" data-html="true" title="<b>{{$pin['title']}}</b><br/>{{$pin['content']}}">{{$index+1}}</div>
        <div class="pindicator d-flex d-md-none">{{$index+1}}</div>
        <div class="pulse"></div>
      </div>
      @endforeach
    </div>
  </div>

  <div class="pin-list d-block d-md-none">
    @foreach($pins as $index=>$pin)
    <div class="row">
      <div class="col-2">
        <div class="number">{{$index+1}}</div>
      </div>
      <div class="col-10">
        <div class="content">
          <div class="arrow-left"></div>
          <span class="title">{{$pin['title']}}</span> - {{$pin['content']}}
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>