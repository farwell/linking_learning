@php
    $configLms = config()->get("settings.lms.live");
@endphp

        <section class="detailed-tree">
          <div class="start">
            @if($course->start >= Carbon\Carbon::today()->toDateString())
            <p class="span-space"><strong>Starts:</strong> {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}</p>
            @else
            @if($course->end <= Carbon\Carbon::today()->toDateString())
          <p class="span-space"><strong>Ended:</strong> {{Carbon\Carbon::parse($course->end)->isoFormat('MMMM Do YYYY')}}</p>
            @else
          <p class="span-space"><strong>Started:</strong> {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}</p>
            @endif
            @endif
           <p class="span-space"><strong>Effort:</strong> {{$course->effort}}</p>
           <p class="span-space">
            <strong>Target group:</strong> @if(isset($course->job_group->name)) {{ $course->job_group->name }} @endif
          </p>
          <p class="span-space">
            <strong>Price:</strong> Ksh. {{$course['price']}}
          </p>
      @guest
         <p>
           <a href = "{{route('user.login')}}"
             class="btn btn-overall purple">Enrol for this course <i class="fas fa-long-arrow-alt-right" aria-hidden="true"></i>
           </a></p>
      @else

          @if(!$licensed)
          <p>
              <a style="color:#fff" data-val="{{$course->id}}" href="#rateModal" class="btn btn-overall purple open-AddBookDialog" title="Purchase this course"> Purchase this course </a>
          </p>

          @else
      @if($enrolled)
          @if($start === 0)
          <p>
          <a class="btn btn-overall purple" href="{{$configLms['LMS_BASE'] . '/courses/' . $course->id . '/courseware'}}" target="_blank">Take Course</a></p>
          @else
          <p>
          <a href="" data-toggle="modal" data-target="#courseUpcoming" class="btn btn-overall purple">Take Course</a></p>
          @endif
      @else<p>
          <a class="btn btn-overall purple" href="{{route('course.enroll',$course->id)}}/">Enroll into Course</a></p>
      @endif

      @endif
      @endguest





        <share-course-action email="{{$course->share['email']}}" facebook="{{$course->share['facebook']}}" linkedin="{{$course->share['linkedin']}}" url="{{route('course-detail',$course->id)}}"></share-course-action>

        </div>
        </section>

       <share-modal></share-modal>
       @section('js')
       <script >
       $(document).on("click", ".open-AddBookDialog", function (e) {

       	e.preventDefault();

       	var _self = $(this);

       	var myBookId = _self.data('val');

        $("#single_purchase").attr('href', '/profile/wishlist/purchase-direct/'+myBookId);

       	$("#rate_course").val(myBookId);


       	$(_self.attr('href')).modal('show');

       });

       </script>
       <script>
       $(document).on("click", ".open-CodeDialog", function (e) {
       	e.preventDefault();

         var _self = $(this);

         var courseId = _self.data('code');

         $("#confirm_course").val(courseId);

       $(_self.attr('href')).modal('show');

       });
       </script>
       @endsection
