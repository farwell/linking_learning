@if(!isset($main))
<br />
<br />
<br />
@else
<br />
@endif
<div class="pricing-column">
    <header>
        {{$col_header}}
    </header>
    <div class='pricing-body'>
        {{$offering}}
    </div>
    <footer class="text-center">
        @if(isset($footer_notes))
        <small>
            {{$footer_notes}}
        </small>
        @endif
        @if(isset($contact))
        <a href="{{route('contact')}}"><button class="btn-rufous">
                <i class="far fa-paper-plane"></i>
                &nbsp;
                Contact
            </button>
        </a>
        @endif
        @if(isset($subscribe))
      <a href="" data-toggle="modal" data-target="#flatRateModal"><button class="btn-rufous">
                <i class="far fa-paper-plane"></i>
                &nbsp;
                Subscribe
            </button></a>
        @endif
    </footer>
</div>