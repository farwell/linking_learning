@php
$configLms = config()->get("settings.lms.live");
 @endphp

<ppic current="{{ Auth::user()->ppic ?: asset('images/user.png')}}" style="width:130px;height:130px;margin:0 auto;"></ppic>

<ul>
  <li>
    {{ Auth::user()->first_name }}
    {{ Auth::user()->last_name }}
  </li>
</ul>
<div class="divider div-transparent div-dot"></div>
<ul>
  <li class="{{ \Route::current()->getName() == 'profile.courses' ? 'active' : '' }}">
    <a href="{{route('profile.courses')}}">My Courses</a>
  </li>
  <li class="{{ \Route::current()->getName() == 'profile.achievements' ? 'active' : '' }}">
    <a href="{{route('profile.achievements')}}">My Achievements</a>
  </li>
  @if(empty(Auth::user()->company))
  <li class="{{ \Route::current()->getName() == 'uwishlist.show' ? 'active' : '' }}">
    <a href="{{route('uwishlist.show')}}">Wishlist</a>
  </li>
  @endif
</ul>
<div class="divider div-transparent div-dot"></div>
<ul>
  <li class="{{ \Route::current()->getName() == 'profile.password.change' ? 'active' : '' }}">
    <a href="{{route('password.change')}}">Change Password</a>
  </li>


  <li class="{{ \Route::current()->getName() == 'profile.edit' ? 'active' : '' }}">
    <a href="{{route('profile.edit')}}">Edit My Profile</a>
  </li>
</ul>
<div class="divider div-transparent div-dot"></div>
<ul>
@if(Auth::user()->is_company_admin)
  <li>
    <a href="/admin/user_licenses/reports" target="_blank">Company Administration</a>
  </li>
  @endif
@if(Auth::user()->is_admin)
  <li>
    <a href="/admin/companies" target="_blank">Administrator Section</a>
  </li>
  @endif
@if(Auth::user()->is_ikadmin)
  <li>
    <a href="/admin/courses" target="_blank">Administrator Section</a>
  </li>
  @endif
  <li>
    <a data-logout-url="{{ $configLms['LMS_BASE'] }}/logout" href="{{ route('logout') }}" class="logout-edx">Logout</a>
  </li>
</ul>