<course-filter
  action="{{ route('courses') }}"
  filter_image="{{asset('images/icons/Filter.png')}}"
  job_groups="{{ json_encode($course_categories) }}"
>
</course-filter>
