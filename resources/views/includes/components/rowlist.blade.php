<div class="bg-ghost rowlist">

    <h1 class="special-heading"> SUSTAINABILITY TRAINING IS WELL SUITED FOR</h1>

    <div class="clearfix">
        <br />
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-8 col-sm-12 col-12">
            @foreach( $list as $key=>$item )
            <div class="row list-item no-gutters">
                <div class="col-3 list-icon">
                    <span data-aos="zoom-in">
                        {!! $item['icon'] !!}
                    </span>
                </div>
                <div class="col-9 list-content">
                    <span>
                        {!! $item['text'] !!}
                    </span>
                </div>
            </div>
            <div class="clearfix d-md-none"><br/></div>
            @endforeach
            <div class="row list-item no-gutters">
                <div class="col-3 list-icon">

                </div>
                <div class="col-9 list-content">
                    <a href="{{ route('benefit') }}">
                        <button class="btn-orange">
                            Find out more
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix">
        <br />
    </div>
</div>