  <section class="cs-category-tree">
    <div class="cs-category-root">Course Categories</div>
      @foreach($course_categories as $category)
    <div class="accordion" id="accordion{{$category->id}}">
        <div class="card category-card">
            <div class="card-header category-heading" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-category collapsed" type="button" data-toggle="collapse" data-target="#collapse{{$category->id}}" aria-expanded="false" aria-controls="collapse{{$category->id}}">

                        {{$category->name}} &nbsp;&nbsp;
                        <span class="icon"></span>
                    </button>
                </h2>
            </div>

            <div id="collapse{{$category->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion{{$category->id}}">
                <div class="card-body categorey-body">
                  @foreach($category->available_courses as $course)
                  <p><a href="/courses/detail/{{$course->slug}}">{{$course->name}}</a></p>
                  @endforeach

                </div>
            </div>
        </div>
    </div>
      @endforeach
  </section>