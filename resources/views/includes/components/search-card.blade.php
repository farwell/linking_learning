<div class="search-result row">
  <div class="col-md-3">
      <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" />
  </div>
  <div class="col-md-9">
      <a href="/courses/detail/{{$course->id}}" class="text-purple">{{$course['name']}}</a>
      <p class="description text-left text-grey">
        <?php
        $str = $course['short_description'];
        if (strlen($str) > 220) {
          $str = substr($str, 0, 220) . '...';
        }
        echo $str;
        ?>
      </p>
      <a href="/courses/detail/{{$course->id}}" class="text-purple">Read More <i class="fas fa-arrow-right"></i></a>
    </div>
</div>