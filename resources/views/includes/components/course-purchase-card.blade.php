
  <div class="row no-gutters my-course">
    <div class="col-2">
        <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" class="preview-img"/>
    </div>
    <div class="col-7">
    <div class="course-title">
      <div class="title">
        <a target="_blank" href="/courses/detail/{{$course->id}}">
        {{$course['name']}}
        </a>
      </div>
      <div class="sub-title">  
        @if($course->start >= Carbon\Carbon::today()->toDateString())
        Starts: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @else
        Started: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @endif  
        | 230 people have enrolled</div>
        <div class="row">
      <p class="description">
        <?php
        $str = $course['short_description'];
        if (strlen($str) > 150) {
          $str = substr($str, 0, 150) . '...';
        }
        echo $str;
        ?>
      </p>
        </div>
    </div>
    </div>
    <div class="col-3">
      @if(isset($wishlist))
        <purchase-action confirmed="{{$wishlist->purchase}}" action="{{ route('wishlist.togglepurchase',$course->id) }}" upcoming="{{$wishlist->upcoming}}" price="{{$course->price}}" remove="{{route('wishlist.remove',$course->id)}}"></purchase-action>
      @endif
    </div>
  </div>

