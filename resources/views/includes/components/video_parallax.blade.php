<!-- Background Local Video Parallax -->
<div class="jarallax-video">
  <header class="dark">
    @include('includes.general.header',['dark'=>true])
  </header>
  <header class="light sticky-navigation">
    @include('includes/general/header',['dark'=>false])
  </header>
  <div class="content">
    <div class="wrap">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
          <h1>E-LEARNING ON SUSTAINABLE FINANCE <span id="video-toggle-text" class="toggle-text">MODERN DIDACTICS,PRACTICAL,INTERACTIVITY,CASE STUDIES,BEST PRACTICES</span></h1>
          <div class="ik-hr-white">
            <span class="hr-inner">
              <span class="hr-inner-style"></span>
            </span>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 modal-video-play">
          <span class="modal-trigger" data-toggle="modal" data-target="#youtube">
            <div class="outline">
              <span class="fa-wrap" data-toggle="tooltip" data-placement="top" title="Watch our Introduction Video">
                <i class="fas fa-play"></i>
              </span>
            </div>
          </span>
        </div>
      </div>
    </div>
  </div>
  <a href="#next-section" title="" class="scroll-down-link d-none d-md-block" aria-hidden="true">
    <i class="fas fa-chevron-down"></i>
  </a>
</div>
<div id="next-section"></div>