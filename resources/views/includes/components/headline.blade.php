<div class="row headline justify-content-center">
  <div class="col-lg-7 col-md-7 col-sm-7 col-11 text-center">
    @if(isset($top_width))
    @for ($i = 0; $i < $top_width; $i++)
      <br/>
    @endfor
    @endif
    @if(isset($subheading))
      <h2>
        @if(isset($capitalize))
        {!! ucwords(strtolower($headline)) !!}
        @else
        {!! ($headline) !!}
        @endif
      </h2>
      <h3>
        @if(isset($capitalize))
        {!! ucwords(strtolower($subheading)) !!}
        @else
        {!! ($subheading) !!}
        @endif
      </h3>
    @else
      <h1>
        @if(isset($capitalize))
        {!! ucwords(strtolower($headline)) !!}
        @else
        {!! ($headline) !!}
        @endif
      </h1>
    @endif
    @if(isset($bottom_width))
    @for ($i = 0; $i < $bottom_width; $i++)
      <br/>
    @endfor
    @endif
    @if(isset($image))
    <img src="{{ $image['src'] }}" width="{{ $image['width'] }}"/>
    @endif
    <div class = "ik-hr-black">
      <span class="hr-inner">
        <span class="hr-inner-style"></span>
      </span>
    </div>
  </div>
</div>