<div class="team-member">
  <div class="img-container">
    <div class="ppic" style="background-image:url('{{ $image }}')">
    </div>
    @if(isset($linkedin))
    <div class="social">
      <div class="inner">
        <a class="linkedin" href="{{$linkedin}}" target="_blank">
          <span class="icon">
            <i class="fab fa-linkedin-in"></i>
          </span>
        </a>
      </div>
    </div>
    @endif
  </div>
  <h3 class="name">{{$name}}</h3>
  <div class="job-title">{{$title}}</div>
  <div class="description">
    <p>{{$description}}</p>
  </div>
</div>