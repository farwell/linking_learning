<div class="row">
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-retweet',
        'title'=>'INTERACTIVITY',
        'content'=>'Do not just scroll but click your way through and discover at your own speed and preference.'
        ])
    </div>
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-cloud',
        'title'=>'PRAXIS',
        'content'=>'Minimum theory, maximum praxis. Real cases and experiences from our extensive field experience.'
        ])
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-comments',
        'title'=>'ROLE PLAY',
        'content'=>'Meet different characters in our trainings each of which with a very specific job role. The characters interact with each other, discuss problems and jointly develop suitable solutions.'
        ])
    </div>
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-mobile-alt',
        'title'=>'RESPONSIVE DESIGN',
        'content'=>'Desktop, tablet, smartphone. Use our courses at your own convinience. Full functionality and design is guranteed.'
        ])
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-life-ring',
        'title'=>'TECHNICAL SUPPORT',
        'content'=>'Any technical issue? All courses come with a free 24/7 technical support.'
        ])
    </div>
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-file-alt',
        'title'=>'CERTIFICATES',
        'content'=>'Upon successful completion of each course, corporate trainees can recieve certificates of participation.'
        ])
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-paperclip',
        'title'=>'RESOURCES',
        'content'=>'Get access to specifically selected additional resources, and not bombarded with endless link lists.'
        ])
    </div>
    <div class="col-lg-6">
        @include('includes.components.feature-card',[
        'icon'=>'fas fa-signal',
        'title'=>'PROGRESS TRACKING',
        'content'=>'Our state of the art Learning Management System(LMS) allows corporate users to track progress of trainees inside their organisation.'
        ])
    </div>
</div>