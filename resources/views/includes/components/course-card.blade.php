<div class="course-card">
  <article>
    <div class="thumbnail">
      <a href="/courses/detail/{{$course->slug}}">
        <img src="{{asset($course['course_image_uri'])}}" alt="{{$course->name}}" />
        <div class="overlay">
          <div class="overlay-inside" data-toggle="tooltip" data-placement="top" title="{{$course['title']}}">
            <i class="fas fa-share"></i>
          </div>
        </div>
      </a>
<!--       @if($course->licensed)

      @else

      @if(empty(Auth::user()->company))

      <uwishlist-action carted="{{$course->inwishlist()}}" add-action="{{route('uwishlist.add',$course->id)}}" remove-action="{{route('uwishlist.remove',$course->id)}}" authenticated="{{!empty(Auth::user())}}">

      </uwishlist-action>
      @else

      @endif

      @endif -->
    <share-action email="{{$course->share['email']}}" facebook="{{$course->share['facebook']}}" linkedin="{{$course->share['linkedin']}}" url="{{route('course-detail',$course->id)}}"></share-action>
    </div>
    <div class="course-card-content">
      <p class="title text-center">
        <a href="/courses/detail/{{$course->slug}}" class="text-center text-purple">{{ $course['name'] }}</a></p>
      <p class="description text-grey text-center">
        <?php
        $str = $course['short_description'];
        if (strlen($str) > 60) {
          $str = substr($str, 0, 60) . '...';
        }
        echo $str;
        ?>
      </p>
      <p class="price text-grey  text-center">
        @if($course->start >= Carbon\Carbon::today()->toDateString())
        Starts: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @else
        Started: {{Carbon\Carbon::parse($course->start)->isoFormat('MMMM Do YYYY')}}
        @endif
      </p>
      <p class="readmore text-center">
        <a href="/courses/detail/{{$course->id}}">Read More <i class="fas fa-arrow-right"></i></a>
      </p>
    </div>


    @if(isset($uwishlist))
    <purchase-confirm confirmed="{{$uwishlist->purchase}}" action="{{ route('uwishlist.toggle-purchase',$course->id) }}"></purchase-confirm>

    @endif
  </article>
</div>

<share-modal></share-modal>
