  
<div class="modal fade bd-example-modal-lg" id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="notificationModal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content message-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationModalLabel">Notification</h5>
            </div>
            <div class="clearfix">
                <div class="divider div-dark div-dot-arsenic"></div>
            </div>
            <div class="modal-body">
                In the last two months, we have had some changes in this website. <b>Please clear cache on your browser</b> to experience the new features or use a different browser to access the website. Follow this  <a href="{{ route('faqs') }}"> link </a> on how to clear cache
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn dismiss-modal modal-accept"  data-dismiss="modal"  onclick="document.getElementById('notificationModal').style.display='none'">Dismiss</button>
               
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
