<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    {{-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script> --}}
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/css/star-rating.min.css" media="all"
        rel="stylesheet" type="text/css" />

    <!-- with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme CSS files as mentioned below (and change the theme property of the plugin) -->
    <link href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.css"
        media="all" rel="stylesheet" type="text/css" />

    <!-- important mandatory libraries -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/star-rating.min.js"
        type="text/javascript"></script>

    <!-- with v4.1.0 Krajee SVG theme is used as default (and must be loaded as below) - include any of the other theme JS files as mentioned below (and change the theme property of the plugin) -->
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/themes/krajee-svg/theme.js"></script>

    <!-- optionally if you need translation for your language then include locale file as mentioned below (replace LANG.js with your own locale file) -->
    <script src="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-star-rating@4.1.2/js/locales/LANG.js"></script>

    <link href="{{ asset('emojis/lib/css/nanoscroller.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/lib/css/emoji.css') }}" rel="stylesheet">


    <link href="{{ asset('css/app.css?t='.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.theme.default.min.css') }}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{ asset('css/emojionearea.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-multiselect.css') }}">
    @if ((new \Jenssegers\Agent\Agent())->isMobile())
    <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif
    <link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
    <!-- Google Tag Manager -->

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-H8TMVGQ4CN');
    </script>

    @yield('css')
</head>

<body>
    <div id="app">

        @include('layouts.partials._header')

        @include('includes.modals.courseerrors')
        @include('includes.modals.coursemessages')
        @include('includes.modals.ticket')
        @yield('content')

        <!-- Back to top button -->

        @include('layouts.partials._footer')
    </div>
    {{-- <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}"></script> --}}
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>

    <script src="{{ asset('emojis/lib/js/nanoscroller.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/tether.min.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/config.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/util.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/jquery.emojiarea.js') }}"></script>
    <script src="{{ asset('emojis/lib/js/emoji-picker.js') }}"></script>

    <script src="{{ asset('js/upload.js') }}"></script>
    <script src="{{ asset('js/side-menu.js') }}"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>

    @yield('js')

    <script>
        function makePayment() {

            $('#channelsModal').modal('show');

        }
    </script>
    <script>
        $(document).on("click", ".confrimpayment", function(e) {

            e.preventDefault();

            var _self = $(this);

            var url = _self.data('val');
            _self.css('display', 'none');
            window.open(url, '_self');
        });
    </script>
    <script>
        function confirmTransaction() {
            $('#img').show();
            setTimeout(function() {
                $('#img').hide();
                /*submit the form after 5 secs*/
                $('#transaction').submit();
            }, 15000)


        }
    </script>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRD4JKF" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->


       
      

</body>

</html>
