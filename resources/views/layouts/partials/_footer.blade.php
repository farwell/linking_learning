@php
use App\Models\Socialmedia;
$facebook = 'facebook';
$twitter = 'twitter';
$instagram = 'instagram';
$youtube = 'youtube';
$socialfacebook = Socialmedia::where('key', $facebook)
    ->published()
    ->first();
$socialtwitter = Socialmedia::where('key', $twitter)
    ->published()
    ->first();

$socialinstagram = Socialmedia::where('key', $instagram)
    ->published()
    ->first();

$socialyoutube = Socialmedia::where('key', $youtube)
    ->published()
    ->first();

@endphp
@if ((new \Jenssegers\Agent\Agent())->isDesktop())
    <footer>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9">
                            <a class="navbar-brand nav-brand-v1" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>
                            <a class="navbar-brand nav-brand-v2" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo_v2.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>

                        </div>

                        <div class="col-md-2 ">

                            <div class="footer-span ">
                                <span>Follow Us</span>
                                </br>

                                @if ($socialfacebook)
                                    <a href="{{ $socialfacebook->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @else
                                    <a href="#"><img src="{{ asset('images/icons/Facebook.svg') }}" alt="facebookicon"
                                            title="Follow us on Facebook"></a>
                                @endif

                                @if ($socialtwitter)
                                    <a href="{{ $socialtwitter->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @else
                                    <a href="#"><img src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"
                                            title="Follow us on Twitter"></a>
                                @endif


                                @if ($socialinstagram)
                                    <a href="{{ $socialinstagram->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/Instagram.svg') }}" alt="instragramicon"
                                            title="Follow us on Instagram"></a>
                                @else
                                    <a href="#"><img src="{{ asset('images/icons/Instagram.svg') }}"
                                            alt="instragramicon" title="Follow us on Instagram"></a>
                                @endif

                                @if ($socialyoutube)
                                    <a href="{{ $socialyoutube->description }}" target="_blank"><img
                                            src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @else
                                    <a href="#"><img src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"
                                            title="Follow us on Youtube"></a>
                                @endif
                            </div>
                        </div>

                    </div>


                </div>


            </div>
        </div>

        {{-- <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="footer-text">
                    <ul>
                        <li><small>Copyright &copy; <?php echo date('Y'); ?> Erevuka | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                    </ul>
                </div>
            </div>
        </div> --}}
        </div>
    </footer>
@endif

@if ((new \Jenssegers\Agent\Agent())->isMobile())
    <footer>
        <div class="container-fluid">
            <div class="row ">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-10">
                            <a class=" nav-brand-v1 footer-navbar-brand" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>
                            <a class=" nav-brand-v2 footer-navbar-brand-v2" href="{{ url('/') }}">
                                <img src="{{ asset('images/icons/logo_v2.png') }}"
                                    alt="{{ config('app.name', 'Laravel') }}">
                            </a>

                        </div>

                        <div class="col-md-2 ">

                            <div class="footer-span ">
                                <span>Follow Us</span>
                                </br>
                                <a href="#"><img src="{{ asset('images/icons/Facebook.svg') }}"
                                        alt="facebookicon"></a>
                                <a href="#"><img src="{{ asset('images/icons/Twitter.svg') }}" alt="twittericon"></a>
                                <a href="#"><img src="{{ asset('images/icons/Instagram.svg') }}"
                                        alt="instragramicon"></a>
                                <a href="#"><img src="{{ asset('images/icons/YouTube.svg') }}" alt="youtubeicon"></a>
                            </div>
                        </div>

                    </div>


                </div>


            </div>
        </div>

        {{-- <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="footer-text">
                <ul>
                    <li><small>Copyright &copy; <?php echo date('Y'); ?> Erevuka | All Rights Reserved</small>&nbsp;&nbsp;&nbsp;<small class="bordered-left">Powered By <a href="https://farwell-consultants.com"  aria-label="Farwell Innovations(opens in new tab)"  target="_blank">Farwell Innovations Ltd</a></small></li>
                </ul>
            </div>
        </div>
    </div> --}}
        </div>
    </footer>
@endif
