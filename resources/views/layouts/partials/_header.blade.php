<div class="mobiles">
    <nav class="navbar sticky-top navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('images/icons/logo.png') }}" alt="{{ config('app.name', 'Laravel') }}">
            </a>

            <a href="{{ url('/') }}" class="navbar-brand nav-brand-v2 d-none d-md-block"><img
                    src="{{ asset('images/icons/logo_v2.png') }}" alt="Erevuka Twill"></a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @if (!Auth::check())
                        <?php foreach (config('app-constants.nav') as $el) : ?>
                        <li class="nav-item">
                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                <a class="nav-link active" href="{{ route($el['route']) }}"> {{ $el['title'] }}</a>
                            @else
                                <a class="nav-link" href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                            @endif
                        </li>
                        <?php endforeach; ?>
                    @else
                        <?php foreach (config('app-constants.auth_nav') as $el) : ?>
                        <li class="nav-item xxxx" data-toggle="tooltip" data-placement="bottom"
                            title="About the Program">
                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                <a class="nav-link active" href="{{ route($el['route']) }}">
                                    {{ $el['title'] }}</a>
                            @else
                                <a class="nav-link" href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                            @endif
                        </li>
                        <?php endforeach; ?>
                        @if ((new \Jenssegers\Agent\Agent())->isDesktop())
                            <?php foreach (config('app-constants.auth_menu') as $el) : ?>

                            @if ($el['id'] == 4)
                                <li class="nav-item dropdown 222">

                                    @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                        <a class="nav-link active" href="{{ route($el['route']) }}"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ $el['title'] }}</a>
                                        <div class="dropdown-menu dropdown-menu-right-resources profile-menu connect-menu"
                                            aria-labelledby="navbarDropdown">
                                            <span class="dropdown-menu-arrow"></span>
                                            <?php foreach (config('app-constants.resources') as $el) : ?>

                                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                                <a class="nav-link active dropdown-item"
                                                    href="{{ route($el['route']) }}"> {{ $el['title'] }}</a>
                                            @else
                                                <a class="nav-link dropdown-item" href="{{ route($el['route']) }}">
                                                    {{ $el['title'] }}</a>
                                            @endif

                                            <?php endforeach; ?>
                                        </div>
                                    @else
                                        <a class="nav-link" href="{{ route($el['route']) }}"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            data-toggle="tooltip" data-placement="bottom"
                                            title="Find Programme Resources"> {{ $el['title'] }}</a>

                                        <div class="dropdown-menu dropdown-menu-right-resources profile-menu connect-menu resources"
                                            aria-labelledby="navbarDropdown">
                                            <span class="dropdown-menu-arrow"></span>
                                            <?php foreach (config('app-constants.resources') as $el) : ?>

                                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                                <a class="nav-link active dropdown-item"
                                                    href="{{ route($el['route']) }}"> {{ $el['title'] }}</a>
                                            @else
                                                <a class="nav-link dropdown-item" href="{{ route($el['route']) }}">
                                                    {{ $el['title'] }}</a>
                                            @endif

                                            <?php endforeach; ?>
                                        </div>

                                    @endif
                                </li>
                            @elseif($el['id'] == 6)
                                <li class="nav-item dropdown 111">
                                    @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                        <a class="nav-link active" href="{{ route($el['route']) }}"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            {{ $el['title'] }}</a>
                                        <div class="dropdown-menu dropdown-menu-right-connect profile-menu connect-menu"
                                            aria-labelledby="navbarDropdown">
                                            <span class="dropdown-menu-arrow"></span>
                                            <?php foreach (config('app-constants.connect') as $el) : ?>

                                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                                <a class="nav-link active dropdown-item"
                                                    href="{{ route($el['route']) }}"> {{ $el['title'] }}</a>
                                            @else
                                                <a class="nav-link dropdown-item"
                                                    href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                                            @endif

                                            <?php endforeach; ?>
                                        </div>
                                    @else
                                        <a class="nav-link" href="{{ route($el['route']) }}"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                                            data-toggle="tooltip" data-placement="bottom"
                                            title="Connect with other participants">{{ $el['title'] }}</a>

                                        <div class="dropdown-menu dropdown-menu-right-connect profile-menu connect-menu resources"
                                            aria-labelledby="navbarDropdown">
                                            <span class="dropdown-menu-arrow"></span>
                                            <?php foreach (config('app-constants.connect') as $el) : ?>

                                            @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                                <a class="nav-link active dropdown-item"
                                                    href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                                            @else
                                                <a class="nav-link dropdown-item"
                                                    href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                                            @endif

                                            <?php endforeach; ?>
                                        </div>

                                    @endif
                                </li>

                                @guest
                                    <li class="nav-item ">

                                        <a class="nav-link " href="#" title="Your notifications will show here"><i
                                                class="fas fa-bell"></i><span
                                                class="badge badge-danger badge-counter"></span>

                                        </a>

                                        <a class="nav-link " href="#" title="Your Message will show here"><i
                                                class="icon icon-messages"></i><span
                                                class="badge badge-danger badge-counter"></span>

                                        </a>
                                    </li>
                                @else
                                    @php
                                        $messages = [];
                                        $notify = [];
                                        
                                    @endphp
                                    @foreach (auth()->user()->unreadNotifications as $notification)
                                        @if ($notification->data['type'] == 3)
                                            @php $messages[] = $notification; @endphp
                                        @elseif($notification->data['type'] == 1)
                                            @php $notify[] = $notification; @endphp
                                        @endif
                                    @endforeach

                                    <li class="nav-item dropdown no-arrow  notification">


                                        @if ($messages)
                                            <a style="padding-top: 8px;" class="nav-link " href="#" id="alertsDropdown"
                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false" title="Your notifications will show here">

                                                <span class="header-icon icon-messages"></span>
                                                <!-- Counter - Alerts -->
                                                <span
                                                    class="badge badge-primary badge-counter">{{ count($messages) }}</span>
                                            </a>
                                        @else
                                            <a style="padding-top: 8px;" class="nav-link "
                                                href="{{ route('message.index') }}">

                                                <span class="header-icon icon-messages" data-toggle="tooltip"
                                                    data-placement="bottom" title="Read and write messages "></span>
                                                <!-- Counter - Alerts -->
                                                <span class="badge badge-primary badge-counter"></span>
                                            </a>
                                        @endif

                                        @if ($messages)
                                            <!-- Dropdown - Alerts -->
                                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in messages-menu"
                                                aria-labelledby="alertsDropdown" id="noti-drop">
                                                <span class="dropdown-menu-arrow"></span>
                                                <h6 class="dropdown-header">
                                                    Notifications
                                                </h6>
                                                @foreach ($messages as $notification)
                                                    <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                                        href="{{ route('single.notification', [$notification->id]) }}"
                                                        title="Click to View">
                                                        <div class="mr-3">
                                                            <div class="icon-circle bg-primary">
                                                                <i class="fas fa-file-alt text-white"></i>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="small text-gray-500">
                                                                {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                            </div>
                                                            <span
                                                                class="font-weight-bold">{{ $notification->data['title'] }}
                                                            </span>
                                                        </div>
                                                    </a>
                                                @endforeach

                                                <a class="dropdown-item text-center small text-gray-500"
                                                    href="{{ route('markAsRead') }}">Mark all as read</a>
                                            </div>
                                        @else
                                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                                aria-labelledby="alertsDropdown" id="noti-drop">

                                                <span class="dropdown-menu-arrow"></span>
                                                <h6 class="dropdown-header">
                                                    No new notification
                                                </h6>
                                            </div>
                                        @endif

                                    </li>

                                    <li class="nav-item dropdown no-arrow  notification">
                                        @if ($notify)
                                            <a style="padding-top: 8px;" class="nav-link " href="#" id="alertsDropdown"
                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false" title="Your notifications will show here">

                                                <span class="header-icon icon-notifications"></span>


                                                <!-- Counter - Alerts -->

                                                <span
                                                    class="badge badge-danger badge-counter">{{ count($notify) }}</span>

                                            </a>
                                        @else
                                            <a style="padding-top: 8px;" class="nav-link " href="#">

                                                <span class="header-icon icon-notifications" data-toggle="tooltip"
                                                    data-placement="bottom" title="View notifications"></span>


                                                <!-- Counter - Alerts -->

                                                <span class="badge badge-danger badge-counter"></span>
                                            </a>
                                        @endif
                                        @if ($notify)
                                            <!-- Dropdown - Alerts -->
                                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in notifications-menu"
                                                aria-labelledby="alertsDropdown" id="noti-drop">
                                                <span class="dropdown-menu-arrow"></span>
                                                <h6 class="dropdown-header">
                                                    Notifications
                                                </h6>
                                                @foreach ($notify as $notification)
                                                    @if ($notification->data['type'] == 1)
                                                        <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                                            href="{{ route('single.notification', [$notification->id]) }}"
                                                            title="Click to View">
                                                            <div class="mr-3">
                                                                <div class="icon-circle bg-primary">
                                                                    <i class="fas fa-file-alt text-white"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="small text-gray-500">
                                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                                </div>
                                                                <span
                                                                    class="font-weight-bold">{{ $notification->data['title'] }}
                                                                </span>
                                                            </div>
                                                        </a>
                                                    @elseif($notification->data['type'] == 2)
                                                        <a class="dropdown-item d-flex align-items-center dropdown-wrap"
                                                            href="{{ route('single.notification', [$notification->id]) }}"
                                                            title="Click to View">
                                                            <div class="mr-3">
                                                                <div class="icon-circle bg-primary">
                                                                    <i class="fas fa-file-alt text-white"></i>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <div class="small text-gray-500">
                                                                    {{ Carbon\Carbon::parse($notification->created_at)->format('M jS Y') }}
                                                                </div>
                                                                <span
                                                                    class="font-weight-bold">{{ $notification->data['data'] }}
                                                                    {{ $notification->data['type'] }}</span>
                                                            </div>
                                                        </a>
                                                    @endif
                                                @endforeach

                                                <a class="dropdown-item text-center small text-gray-500"
                                                    href="{{ route('markAsRead') }}">Mark all as read</a>
                                            </div>
                                        @else
                                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                                aria-labelledby="alertsDropdown" id="noti-drop">

                                                <span class="dropdown-menu-arrow"></span>
                                                <h6 class="dropdown-header">
                                                    No new notification
                                                </h6>
                                            </div>
                                        @endif

                                    </li>
                                @endguest
                            @else
                                <li class="nav-item" data-toggle="tooltip" data-placement="bottom"
                                    title="Past and Upcoming Sessions">
                                    @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                        <a class="nav-link active" href="{{ route($el['route']) }}">
                                            {{ $el['title'] }}</a>
                                    @else
                                        <a class="nav-link"
                                            href="{{ route($el['route']) }}">{{ $el['title'] }}</a>
                                    @endif
                                </li>
                            @endif
                            <?php endforeach; ?>
                        @else
                            <?php foreach (config('app-constants.mobile_auth_menu') as $el) : ?>
                            <li class="nav-item">
                                @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                                    <a class="nav-link active" href="{{ route($el['route']) }}">
                                        {{ $el['title'] }}</a>
                                @else
                                    <a class="nav-link" href="{{ route($el['route']) }}">
                                        {{ $el['title'] }}</a>
                                @endif
                            </li>
                            <?php endforeach; ?>

                        @endif
                    @endif
                    {{-- @if ((new \Jenssegers\Agent\Agent())->isMobile())
                  <?php foreach (config('app-constants.auth') as $el) : ?>
                  <li class="nav-item">
                    @if (\Route::current()->getName() == $el['route'] || strpos(\Request::url(), $el['route']) !== false)
                    <a class="nav-link active" href="{{route($el['route'])}}"> {{$el['title']}}</a>
                    @else
                    <a class="nav-link" href="{{route($el['route'])}}"> {{$el['title']}}</a>
                    @endif
                  </li>
                <?php endforeach; ?>
                  @endif --}}

                    @if (!Auth::check())
                        <li class="nav-item section-auth">

                            @if (Route::current()->getName() === 'register')
                                <button type="button" class="btn btn-overall btn_register_active"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                            @else
                                <button type="button" class="btn btn-overall btn_register"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                                {{-- <a class="nav-link" href="{{ route('login') }}/">{{ __('Register') }}</a> --}}
                            @endif
                        </li>

                        <li class="nav-item section-auth">

                            @if (Route::current()->getName() === 'login')
                                <button type="button" class="btn btn-overall btn_login_active"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @else
                                <button type="button" class="btn btn-overall btn_login"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown profile-dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                @if (Auth::user()->profile_pic != null)
                                    @php $ppic = Auth::user()->profile_pic; @endphp
                                @else
                                    @php $ppic = 'images.jpg'; @endphp
                                @endif
                                <img src="{{ asset('uploads/' . $ppic) }}" alt="{{ Auth::user()->name }}"> &nbsp;
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right profile-menu"
                                aria-labelledby="navbarDropdown">
                                <span class="dropdown-menu-arrow"></span>

                                <?php foreach (config('app-constants.profile') as $el) : ?>
                                <a class="dropdown-item"
                                    href="{{ route($el['route']) }}/">{{ $el['title'] }}</a>
                                <?php endforeach; ?>

                                <a class="dropdown-item menu-breaker" href=""><span></span></a>
                                <a class="dropdown-item" href="{{ route('profile.edit') }}/">Edit Profile</a>
                                @if (Auth::user()->is_admin)
                                    <a class="dropdown-item" href="/admin">Admin Section</a>
                                @endif
                                @if (Auth::user()->is_company_admin)
                                    <a class="dropdown-item " href="/admin">Faclilitator Dashboard</a>
                                @endif
                                <a class="dropdown-item " href="{{ route('logout') }}/" onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                    <span>{{ __('Logout') }}</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="mobile">
                <ul class="navbar-nav ml-auto">

                    @if (Auth::guest())
                        <li class="nav-item section-auth-2">

                            @if (Route::current()->getName() === 'register')
                                <button type="button" class="btn btn-overall btn_register_active"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                            @else
                                <button type="button" class="btn btn-overall btn_register"
                                    onclick="location.href='{{ route('register') }}'">Register</i></button>
                                {{-- <a class="nav-link" href="{{ route('login') }}/">{{ __('Register') }}</a> --}}
                            @endif
                        </li>
                        <li class="nav-item section-auth">

                            @if (Route::current()->getName() === 'login')
                                <button type="button" class="btn btn-overall btn_login_active"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @else
                                <button type="button" class="btn btn-overall btn_login"
                                    onclick="location.href='{{ route('login') }}'">Login </button>
                            @endif
                        </li>
                    @else
                        <li class="nav-item dropdown profile-dropdown">
                            @if (Auth::user()->profile_pic != null)
                                @php $ppic = Auth::user()->profile_pic; @endphp
                            @else
                                @php $ppic = 'images.jpg'; @endphp
                            @endif
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                <img src="{{ asset('uploads/' . $ppic) }}"
                                    alt="{{ Auth::user()->name }}"><br>{{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right profile-menu"
                                aria-labelledby="navbarDropdown">
                                <span class="dropdown-menu-arrow"></span>




                                <?php foreach (config('app-constants.profile') as $el) : ?>
                                <a class="dropdown-item" href="{{ route($el['route']) }}/"><span
                                        class="text-underline">{{ $el['title'] }}<span></a>
                                <?php endforeach; ?>

                                <a class="dropdown-item menu-breaker" href=""><span></span></a>

                                <a class="dropdown-item" href="{{ route('profile.edit') }}/"><span
                                        class="text-underline">Edit Profile<span></a>
                                <a class="dropdown-item" href="{{ route('profile.password.set') }}/"><span
                                        class="text-underline">Reset Password<span></a>
                                @if (Auth::user()->is_admin)
                                    <a class="dropdown-item" href="/admin" ><span
                                            class="text-underline">Admin Section<span></a>
                                @endif
                                @if (Auth::user()->is_company_admin)
                                    <a class="dropdown-item" href="/admin"
                                        ><span class="text-underline">Faclilitator Dashboard<span></a>
                                @endif
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                                    <span>{{ __('Logout') }}</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>



        </div>




    </nav>

</div>
