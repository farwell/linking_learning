<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-store, no-cache, must-revalidate" />
    <meta http-equiv="expires" content="-1" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
     
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js?t='.time()) }}"></script>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/27089a3c31.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/app.css?t='.time()) }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/owl-carousel/assets/owl.theme.default.min.css') }}">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />

    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">
    <link href="{{ asset('emojis/emoji-picker-main/lib/css/emoji.css') }}" rel="stylesheet">

    @if ((new \Jenssegers\Agent\Agent())->isMobile())
        <link href="{{ asset('css/mobile.css') }}" rel="stylesheet" media="screen and (max-width: 840px)">
    @endif
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-TBPNEMETTR"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-TBPNEMETTR');
</script>

    <!-- Google Tag Manager -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VB3PPJR9N7"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-VB3PPJR9N7');
    </script>

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-T9MTP76');
    </script>

    @yield('css')
</head>

<body>
    <div id="app">
        @include('layouts.partials._header')
       
        @include('includes.modals.disclaimer')
        @include('includes.modals.courseerrors')
        @include('includes.modals.coursemessages')
        @include('includes.modals.ticket')
        @include('includes.modals.notification')

        @yield('content')

        <!-- Back to top button -->
        <a id="support" data-toggle="modal" data-target="#ticketModal"
            title="Technical Support"><span>Support</span></a>
        @include('layouts.partials._footer')
    </div>
    <script src="{{ asset('assets/jquery-ui-1.12.1/external/jquery/jquery.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/jquery-ui-1.12.1/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/owl-carousel/owl.carousel.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


    <script src="{{ asset('emojis/emoji-picker-main/lib/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/util.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/jquery.emojiarea.js') }}" type="text/javascript"></script>
    <script src="{{ asset('emojis/emoji-picker-main/lib/js/emoji-picker.js') }}" type="text/javascript"></script>

    @if (Session::has('course_success'))
    <script>
        jQuery(document).ready(function($) {

            $("#CourseSuccess").addClass('show');
        });
    </script>
@elseif(Session::has('course_error'))
    <script>
        jQuery(document).ready(function($) {
            $("#CourseErrors").addClass('show');
        });
    </script>
@endif

    @yield('js')


    <script>
        function myFunction() {
            var elmnt = document.getElementById("home-scroll");
            elmnt.scrollIntoView();
        }
    </script>
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9MTP76" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!--Pop up setup needs-->
    <style>
        #popup 
        {
          display: inline-block;
          opacity: 0;
          position: fixed;
          top: 20%;
          left: 50%;
          padding: 1em;
          transform: translateX(-50%);
          background: #fff;
          border: 1px solid #888;
          box-shadow: 1px 1px .5em 0 rgba(0, 0, 0, .5);
          transition: opacity .3s ease-in-out;
        }
        #popup.hidden
        {
          display: none;
        }

        #popup.fade-in 
        {
          opacity: 1;
        }

    
    </style>


{{-- <script>
    jQuery(document).ready(function($) {
        window.setTimeout(function () {
  location.reload(true);
  }, 30000);
    });
  </script> --}}
  @if(!Auth::check())
  <script>
    jQuery(document).ready(function($) {
   function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }
  
  was_reloaded = getCookie('was_reloaded')
  console.log(was_reloaded)
  if (was_reloaded != 'yes') {
          document.cookie = "was_reloaded=yes; path=/; max-age=3600;"
      location.reload();
  } 
});
</script>


<script>
    $(document).ready(function() {

        // Get current page URL
        var url = window.location.href;



        // remove # from URL
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

        // remove parameters from URL
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

        // select file name
        url = url.split('/')[3];


         

         if (url === 'login') {

// Add active class
$('#notificationModal').addClass('show');
}

    
    });
</script>
   
  
  @endif

</body>
</html>
