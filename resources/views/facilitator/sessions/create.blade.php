@extends('layouts.dashboard')

@section('title','Create Zoom Meeting')


@section('content')

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Create
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form" method="POST" action="{{ route('sessions.store') }}" enctype="multipart/form-data">
        <div class="kt-portlet__body">
        
            <input type="hidden" name="_token" value="{{ csrf_token() }}">


            <input type="hidden" name="host_video" value="1">

            <input type="hidden" name="participant_video" value="0">

            <div class="form-group">
                <label for="name">{{ __('Topic') }}</label>
                <input  id="name" type="text" class="form-control" value="" name="topic" required>

            </div>

            <div class="form-group">
                <label for="name">{{ __('Agenda') }}</label>
                <textarea id="agenda" class="form-control" name="agenda" required rows="10"></textarea>

            </div>

            <div class="form-group">
                <label for="number">{{ __('Start Time') }}</label>
                <div class='input-group date' id='datetime'>
                    <input type='text' class="form-control" name="start_time" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

            </div>


            <div class="form-group">
                <label for="name">{{ __('Duration') }}</label>
                <input  id="name" type="text" class="form-control" value="" name="duration" required>

            </div>


            <div class="form-group" >
                <label for="Interview_referee_1" >{{ __('Session Image') }}</label>
              <input  type='file' class="form-control" name="session_image" />
           </div>
            
        </div>

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
             </div>
        </div>
    </form>
    <!--end::Form-->




@endsection

@section('footer-js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 
<script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">
 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script>
$(document).ready(function(){

    //Start daterange plugin
    $('#datetime').datetimepicker();
});  
</script>

<script>
     CKEDITOR.replace( 'agenda');
</script>
@endsection