@extends('layouts.dashboard')

@section('title','Groups')

@section('content')

<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Add a new Group
      </h3>
    </div>
  </div>
  <!--begin::Form-->
  <form class="kt-form" method="POST" action="{{ route('group.update',[$group->id]) }}" enctype="multipart/form-data">
    <div class="kt-portlet__body">

      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="form-group">
        <label for="name">{{ __('Title') }}</label>
        <input id="title" type="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ $group->title }}" required autofocus>

        @if ($errors->has('title'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('title') }}</strong>
        </span>
        @endif
      </div>


      <div class="form-group">
        <textarea id="description" class="form-control" name="description" required rows="10">{{$group->description}}</textarea>

        @if ($errors->has('description'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('description') }}</strong>
        </span>
        @endif
      </div>

      <div class="form-group">
        <label for="email">{{ __('Group Type') }}</label>
        <select name="type" id="" class="form-control">
            
            @foreach($types as $type)
             @if($type['id'] == $group->type)
             <option value="{{$type['id']}}" selected>{{$type['name']}}</option>
             @else
             <option value="{{$type['id']}}">{{$type['name']}}</option>
             @endif
            @endforeach
        </select>
        @if ($errors->has('type'))
        <span class="invalid-feedback" role="alert">
          <strong>{{ $errors->first('type') }}</strong>
        </span>
        @endif
      </div>

      <div class="form-group" >
        <label for="Interview_referee_1" >{{ __('Image') }}</label>
      <input  type='file' class="form-control" name="image" />
   </div>


   <div class="form-group">
    <label for="email">{{ __('Users to invite') }}</label>
    <select name="users[]" id="" class="form-control" multiple>
        @foreach($users as $user)
         <option value="{{$user->id}}">{{$user->name}}</option>

        @endforeach
    </select>
    @if ($errors->has('users'))
    <span class="invalid-feedback" role="alert">
      <strong>{{ $errors->first('users') }}</strong>
    </span>
    @endif
  </div>


    </div>

    <div class="kt-portlet__foot">
      <div class="kt-form__actions">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
      </div>
    </div>
  </form>
  <!--end::Form-->


  @endsection

  @section('footer-js')

  <script>
    CKEDITOR.replace('description');
</script>
  @endsection