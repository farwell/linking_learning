<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('name')->nullable();
            $table->text('short_description')->nullable();
            $table->text('overview')->nullable();
            $table->text('more_info')->nullable();
            $table->string('effort')->nullable();
			$table->dateTime('start')->nullable();
			$table->dateTime('end')->nullable();
			$table->dateTime('enrol_start')->nullable();
            $table->dateTime('enrol_end')->nullable();
            $table->float('price')->default(0);
            $table->string('course_image_uri')->nullable();
            $table->string('course_video_uri')->nullable();
            $table->bigInteger('course_category_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->string('slug')->nullable();
            $table->bigInteger('order_id')->unsigned();
            $table->string('course_video')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
