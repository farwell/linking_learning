<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumsTables extends Migration
{
    public function up()
    {
        Schema::create('forums', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
            $table->string('title')->nullable();
			$table->text('description')->nullable();
            $table->string('forum_type')->nullable();
			$table->text('forum_topic')->nullable();
            $table->string('created_by')->nullable();
            $table->integer('position')->unsigned()->nullable();
            
            // add those 2 columns to enable publication timeframe fields (you can use publish_start_date only if you don't need to provide the ability to specify an end date)
            // $table->timestamp('publish_start_date')->nullable();
            // $table->timestamp('publish_end_date')->nullable();
        });

        Schema::create('forum_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'forum');
            $table->string('title', 200)->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('forum_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'forum');
        });

        Schema::create('forum_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'forum');
        });
    }

    public function down()
    {
        Schema::dropIfExists('forum_revisions');
        Schema::dropIfExists('forum_translations');
        Schema::dropIfExists('forum_slugs');
        Schema::dropIfExists('forums');
    }
}
