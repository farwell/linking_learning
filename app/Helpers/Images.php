<?php

/* ---------- Responsive Images Helpers ---------- */

if (!function_exists('responsiveImageBreakpoint')) {
    function responsiveImageBreakpoint() {
        return array(
            'xxlarge' => '(min-width: 1480px)',
            'xlarge' => '(min-width: 1280px) and (max-width:1479px)',
            'large' => '(min-width: 960px) and (max-width:1279px)',
            'medium' => '(min-width: 730px) and (max-width:959px)',
            'small' => '(max-width: 729px)',
        );
    }
}

if (!function_exists('responsiveImageSource')) {
    function responsiveImageSource($options)
    {
        $src = $options['src'];

        // list of the excluded parameters
        $excluded_params = ["media", "src", "ratio", "ratioType", "lazyload", "videoW"]; // all other parameters are permitted, see full doc here : https://docs.imgix.com/apis/url

        if (!isset($options['fm'])):
            // format the image to png
            $options['fm'] = "jpg";
        endif;

        // tweaking parameters values : q, auto, crop, bg
        if (!isset($options['q'])):
            $options['q'] = 70;
        endif;

        if (!isset($options['auto'])):
            $options['auto'] = "format"; /* "false" */
        endif;

        if (isset($options['crop'])):
            $options['fit'] = "crop";
        endif;

        if (isset($options['bg'])):
            $options['bg'] = preg_replace('/#/', '', $options['bg']);
        endif;

        // Playing with ratios : calculate w and h based on ratios
        if (isset($options['ratio'])):
            $options_sizes = responsiveImageSizes($options);

            $options['w'] = $options_sizes[0];
            $options['h'] = $options_sizes[1];
        endif;

        // Add params to the url
        $paramCounter = 0;
        foreach ($options as $key => $value) {
            if (!in_array($key, $excluded_params)) {
                $srcHasParamsAlready = strstr($src, "?");
                $prefix = $paramCounter === 0 ? ($srcHasParamsAlready ? '&' : '?') : '&';
                $src .= $prefix . $key . '=' . $value;
                $paramCounter++;
            }
        }

        return $src;
    }
}

if (!function_exists('responsivePictureSource')) {
    function responsivePictureSource($options, $default)
    {
        $imageBreakpoints = responsiveImageBreakpoint();

        $sources = [];

        if (isset($options['sizes'])) :
            foreach ($options['sizes'] as $key => $newOptions) :
                if (isset($newOptions['w']) && isset($newOptions['media'])) {

                    // inherit src, ratio, fit, lazyload and crop from default if no set
                    $newOptions = extendSizeFromDefault(['src', 'ratio', 'fit', 'crop', 'lazyload'], $newOptions, $default);

                    $media = isset($imageBreakpoints[$newOptions['media']]) ? $imageBreakpoints[$newOptions['media']] : $newOptions['media'];
                    //$srcsetAttr = "srcset";
                    $srcsetAttr = isset($newOptions['lazyload']) && $newOptions['lazyload'] ? "data-srcset" : "srcset"; // "srcset=\"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7\" data-srcset"

                    $newOptionsDPR2 = $newOptions;
                    $newOptionsDPR2['q'] = 50;

                    $newOptionsDPR3 = $newOptions;
                    $newOptionsDPR3['q'] = 50;

                    $source = "<source media=\"screen and " . $media . "\" " . $srcsetAttr . "=\"" . responsiveImageSource($newOptions) . ", " . responsiveImageSource($newOptionsDPR2) . "&dpr=2 2x\">";
                    // Do we need DPR 3x : imgSrc($sizeDPR3) . "&dpr=3 3x"
                    $sources[] = $source;
                }
            endforeach;
        endif;

        return join('', $sources);
    }
}

if (!function_exists('responsiveImageSrcset')) {
    function responsiveImageSrcset($options, $default)
    {
        $imageBreakpoints = responsiveImageBreakpoint();

        $sources = [];
        $sizes = [];

        if (isset($options['sizes'])):
            foreach ($options['sizes'] as $key => $newOptions):
                if (isset($newOptions['w']) && isset($newOptions['media'])) {

                    // inherit src, ratio, fit, lazyload and crop from default if no set
                    $newOptions = extendSizeFromDefault(['src', 'ratio', 'fit', 'crop', 'lazyload'], $newOptions, $default);

                    $media = isset($imageBreakpoints[$newOptions['media']]) ? $imageBreakpoints[$newOptions['media']] : $newOptions['media'];

                    $sourceStr = responsiveImageSource($newOptions) . " " . $newOptions['w'] . "w";
                    $sizeStr = $media . " " . $newOptions['w'] . "px";
                    // Do we need DPR 3x : imgSrc($sizeDPR3) . "&dpr=3 3x"
                    $sources[] = $sourceStr;
                    $sizes[] = $sizeStr;
                }
            endforeach;
        endif;

        //$srcsetAttr = "srcset";
        $srcsetAttr = isset($default['lazyload']) ? "data-lazyload data-srcset" : "srcset";
        $srcsetAttr = isset($default['srcset']) ? $default['srcset'] : $srcsetAttr;

        return $srcsetAttr .'="'. join(',', $sources) . '" sizes="'. join(',', $sizes) . '"';
    }
}

if (!function_exists('extendSizeFromDefault')) {
    function extendSizeFromDefault($props, $size, $default)
    {
        foreach ($props as $prop):
            if (!isset($size[$prop]) && isset($default[$prop])) {
                $size[$prop] = $default[$prop];
            }

        endforeach;

        return $size;
    }
}

if (!function_exists('responsiveImageSizes')) {

    function responsiveImageSizes($options)
    {
        $sizes = [];

        $img_w = isset($options['w']) ? $options['w'] : null;
        $img_h = isset($options['h']) ? $options['h'] : null;

        // get ratio type and specified ratio
        $ratioType = (isset($options['ratioType'])) ? $options['ratioType'] : 'max';
        $ratio = $options['ratio'];
        // if its a fixed ratio we're looking for, work out what target image size we want
        if ($ratioType === 'fixed') {
            $max = max($img_w, $img_h);
            if ($max === $img_w) {
                $img_h = $img_w * $ratio;
            }
            if ($max === $img_h) {
                $img_w = $img_h / $ratio;
            }
        }
        // if the ratio is a max type (image can be any height as long as its not too tall)
        if ($ratioType === 'max') {
            $max_h = $img_w * $ratio;
            $img_h = ($img_h > $max_h) ? $max_h : $img_h;
        }

        // If no height, we calculate it using the width and the ratio
        if (!$img_h) {
            $img_h = $img_w * $ratio;
        }

        $sizes[] = round($img_w);
        $sizes[] = round($img_h);

        return $sizes;
    }
}

if (!function_exists('responsiveImage')) {
    function responsiveImage($options)
    {
        $default = $options['options'];
        $isGif = strpos(strtok($default['src'], '?'), '.gif');

        if (!isset($options['html'])) $options['html'] = [];

        return $isGif ? videoTag($options, $default, $options['html']) : responsiveImageTag($options, $default, $options['html']);
    }
}


if (!function_exists('videoTag')) {
    function videoTag($options, $default, $attributes)
    {
        // Get attributes (alt, classes etc...)
        $attr = join(' ', array_map(function ($key) use ($attributes) {
            if (is_bool($attributes[$key])) {
                return $attributes[$key] ? $key : '';
            }
            return $key . '="' . $attributes[$key] . '"';

        }, array_keys($attributes)));

        $sizes = $options['sizes'];

        $defaultOption = $default;
        if ($sizes) {
            $lastSize = (count($sizes) - 1);
            $defaultOption['w'] = $sizes[$lastSize]['w'];
            $defaultOption['h'] = null;
        }

        $defaultOption['fm'] = 'webm';
        $webmSrc = responsiveImageSource($defaultOption);

        $defaultOption['fm'] = 'mp4';
        $mp4src = responsiveImageSource($defaultOption);

        return '<video class="image__video" autoplay muted playsinline loop ' . $attr . '>
                    <source src="' . $webmSrc . '"
                            type="video/webm">
                    <source src="' . $mp4src . '"
                            type="video/mp4">
                </video>';

    }
}


if (!function_exists('responsiveImageTag')) {
    function responsiveImageTag($options, $default, $attributes)
    {
        // Get attributes (alt, classes etc...)
        $attr = join(' ', array_map(function ($key) use ($attributes) {
            if (is_bool($attributes[$key])) {
                return $attributes[$key] ? $key : '';
            }

            return $key . '="' . $attributes[$key] . '"';

        }, array_keys($attributes)));

        $srcAttr = isset($default['lazyload']) ? "data-src" : "src";
        $sizes = $options['sizes'];

        $defaultOption = $default;
        if($sizes) {
            $lastSize = (count($sizes) - 1);
            if(isset($sizes[$lastSize]['max-w'])) {
                $defaultOption['max-w'] = $sizes[$lastSize]['max-w'];
            } elseif(isset($sizes[$lastSize]['w'])) {
                $defaultOption['w'] = $sizes[$lastSize]['w'];
            }

            $defaultOption['h'] = null;
        }

        return '<img ' . $srcAttr . '="' . responsiveImageSource($defaultOption) . '" ' . responsiveImageSrcset($options, $default) . ' ' . $attr . ' />';
    }
}

if (!function_exists('imageTag')) {
    function imageTag($options, $attributes)
    {
        // Get attributes (alt, classes etc...)
        $attr = join(' ', array_map(function ($key) use ($attributes) {
            if (is_bool($attributes[$key])) {
                return $attributes[$key] ? $key : '';
            }

            return $key . '="' . $attributes[$key] . '"';

        }, array_keys($attributes)));

        // flickity lazyloading please ?
        if (isset($options['lazyload']) && $options['lazyload']) {
            return '<img data-lazyload data-src="' . responsiveImageSource($options) . '" ' . $options['lazyload'] . '="' . responsiveImageSource($options) . '" ' . $attr . ' />';
        } else {
            return '<img src="' . responsiveImageSource($options) . '" ' . $attr . ' />';
        }
    }
}

/**
 * Get the picture tag content
 *
 * @param array   $options  The options to be applied to the query string. https://www.imgix.com/docs/reference. The sizes array is optional.
 *
 * @return string   Containing the full html of the img + sources of your picture elementt
 */
if (!function_exists('responsivePicture')) {
    function responsivePicture($options)
    {
        $default = $options['options'];
        if (!isset($options['html'])) {
            $options['html'] = [];
        }

        $sourceTag = responsivePictureSource($options, $default);
        $imageTag = imageTag($default, $options['html']);

        return $sourceTag . $imageTag;
    }
}
