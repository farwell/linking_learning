<?php

if (!function_exists('adminDashboard')) {
    function adminDashboard() {

		return  [
			'modules' => [
				'users' => [
					'name' => 'users',
					'label' => 'Userlist',
					'label_singular' => 'Userlist',
					'search' => true,
					'count' => true,
					'create' => false,
				],

			],
			
		];
    }
}
