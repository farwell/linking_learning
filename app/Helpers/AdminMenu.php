<?php
if (!function_exists('adminMenu')) {
    function adminMenu() {
        return [

		
			'content'=> [
				'title' =>'Content',
				 'route' => 'admin.content.pages.index',
				 'primary_navigation' => [
					 'pages' => [
						 'title' => 'Pages',
						  'module' => true,
		
					 ],
					 
					'events' => [
						'title' => 'Events',
						 'module' => true,
		
					],
		
					'blogs' => [
						'title' => 'Blogs',
						 'module' => true,
		
					],
		
					 'socialmedia' => [
						'title' => 'Social Media Links',
						 'module' => true,
		
					],
					'projecttopics' => [
						'title' => 'Project Topics',
						 'module' => true,
		
					],
				],
			],

			
			'sessions'=> [
				'title' =>'Sessions',
				 'route' => 'admin.sessions.sessions.index',
				 'primary_navigation' => [
					 'sessions' => [
						 'title' => 'Sessions',
						  'module' => true,
		
					 ],

					 'sessionResources' => [
						 'title' => 'Sessions Resources',
						  'module' => true,
		
					 ],
				 ]
			 ],

			 'resources'=> [
				'title' =>'Resources',
				 'route' => 'admin.resources.resources.index',
				 'primary_navigation' => [
					
					 'resources' => [
						'title' => 'Resources',
						 'module' => true,
		
					],
		            'resourceTypes' => [
						'title' => 'Resource Types',
						 'module' => true,
		
					],

					'resourceThemes' => [
						'title' => 'Resource Themes',
						 'module' => true,
		
					],
					
				],
			],

			'groups'=> [
				'title' =>'Groups',
				 'route' => 'admin.groups.groups.index',
				 'primary_navigation' => [
					 'groups' => [
						 'title' => 'Groups',
						  'module' => true,
		
					 ],

					 'groupTypes' => [
						 'title' => 'Group Types',
						  'module' => true,
		
					 ],
					 'groupFormats' => [
						'title' => 'Group Formats',
						 'module' => true,
	   
					],
					'groupThemes' => [
						'title' => 'Group Themes',
						'module' => true
					]
				 ]
			 ],

			 'forums'=> [
				'title' =>'IBCLN',
				 'route' => 'admin.forums.forums.index',
				 'primary_navigation' => [
					 'forums' => [
						 'title' => 'IBCLN',
						  'module' => true,
		
					 ],

					 'forumTopics' => [
						 'title' => 'IBCLN Topics',
						  'module' => true,
		
					 ],
					 
				 ]
			 ],

			// 'Sessions' => [
			// 	'title' => 'Sessions',
			// 	'route' => 'admin.sessionmeetings.index',
				
			// ],
		
			// 'courses' => [
			// 	'title' => 'Courses',
			// 	'route' => 'admin.courses.index',
				
			// ],
		];

    }
}

if (!function_exists('facilitatorAdminMenu')) {
    function facilitatorAdminMenu() {
        return [
			
			'sessions'=> [
				'title' =>'Sessions',
				 'route' => 'admin.sessions.sessions.index',
				 'primary_navigation' => [
					 'sessions' => [
						 'title' => 'Sessions',
						  'module' => true,
		
					 ],

					 'sessionResources' => [
						 'title' => 'Sessions Resources',
						  'module' => true,
		
					 ],
				 ]
			 ],

			 'groups'=> [
				'title' =>'Groups',
				 'route' => 'admin.groups.groups.index',
				 'primary_navigation' => [
					 'groups' => [
						 'title' => 'Groups',
						  'module' => true,
		
					 ],

					 'groupTypes' => [
						 'title' => 'Group Types',
						  'module' => true,
		
					 ],
					 'groupFormats' => [
						'title' => 'Group Formats',
						 'module' => true,
	   
					],
				 ]
			 ],

			 'resources'=> [
				'title' =>'Resources',
				 'route' => 'admin.resources.resources.index',
				 
				
			],

	
			'forums'=> [
				'title' =>'IBCLN',
				 'route' => 'admin.forums.forums.index',
				 'primary_navigation' => [
					 'forums' => [
						 'title' => 'IBCLN',
						  'module' => true,
		
					 ],

					 'forumTopics' => [
						 'title' => 'IBCLN Topics',
						  'module' => true,
		
					 ],
					 
				 ]
			 ]
		
			// 'courses' => [
			// 	'title' => 'Courses',
			// 	'route' => 'admin.courses.index',
				
			// ],

			
		
		];

    }
}
