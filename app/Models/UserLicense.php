<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\MyCommunity;
use App\Models\TrainingLike;
use App\Models\TrainingComment;
use Auth;
use App\Edx\EdxAuthUser;
use App\Edx\StudentCourseEnrollment;
use App\Edx\StudentCourseware;

class UserLicense extends Model
{
    //

    protected $fillable = [
        'course_id',  'user_id', 'enrolled_at', 'company_license_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['enrolled_at'];

    protected $appends = ['resource_url'];
     /**
     * Get the company that this user belongs to .
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    /**
     * Get the company that this user belongs to .
     */
    public function course()
    {
        return $this->belongsTo('App\Models\Course','course_id','id');
    }


    public function getResourceUrlAttribute()
    {
        return url('/admin/user-licenses/'.$this->getKey());
    }

    public function getEnrollments($course){
        $enrollments = self::where('course_id', $course)->count();

        return $enrollments;
  }

  
  public function getGrades($email)
  {
       if ($this->enrolled_at) {
          //  $result = $this->getCourseProgresses($this->course->id,$email);
           //var_dump($result);die;
          //  return $result['grade'];
          return 1;
       } else {
           return 0;
       }
  }

  public function getStatuses($email)
  {

       if ($this->enrolled_at) {
          //  $result = $this->getCourseProgresses($this->course->id,$email);
          //  return $result['status'];
          return 1;
       } else {
           return 0;
       }
  }


  public function getActionAttribute()
{
  $configLms = config()->get("settings.lms.live");

    $email= '';
    if ($this->enrolled_at) {
         $result = $this->getCourseProgress($this->course->id,$email);
         if($result['grade'] >= 0.80 && $result['status'] == "Completed"){
            return route('session.certificate', $this->course->id);
         }
      return $configLms['LMS_BASE'] . '/courses/' . $this->course->id . '/courseware';
    } else {
        return route('course.enroll', $this->course->id);
    }
}

  public function getCommunityStatus($follow){

    $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();

    if(empty($status['status']) || $status['status'] == 0 ){

      $status['status'] = 0;
    }

    return $status['status'];

  }


  public function getCommunityAction($follow)
  {
    $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();

  if(empty($status['status']) || $status['status'] == 0){
      $id = 0;
    return route('profile.myFollow', [$follow,$id]);
  }elseif( $status['status'] == 1 && $status['follow_id'] == Auth::user()->id){
    $id = 1;
  return route('profile.myFollow', [$follow,$id]);

}elseif( $status['status'] == 2){
    $id = 2;
  return route('profile.myFollow', [$follow,$id]);
  }

}


public function getCommunityBlockAction($follow)
{
  $status = MyCommunity::where('user_id',Auth::user()->id)->where('follow_id',$follow)->first();
  if(!empty($status)){
    if( $status['status'] == 2){
        $id = 3;
      return route('profile.myFollow', [$follow,$id]);
      }elseif( $status['status'] == 3){
        $id = 4;
      return route('profile.myFollow', [$follow,$id]);
      }
  }
  

}

public function getLikes($like,$courseid)
{
  $likes =TrainingLike::where('like_id',$like)->where('course_id',$courseid)->get();

    if(empty($likes)){
      return 0;
    }
  return count($likes);

}

public function getLikeAction($like,$courseid)
{

  return route('profile.myLike', [$like,$courseid]);

}

public function getFollowers($follow){

 $followers = Mycommunity::where('follow_id',$follow)->get();

  if(empty($followers)){

    return 0;
  }

  return count($followers);

}


public function getCommentCount($follow,$courseid){

 $comments = TrainingComment::where('comment_id',$follow)->where('course_id',$courseid)->whereNull('parent_id')->get();

  if(empty($comments)){
    return 0;
  }
  return count($comments);

}

public function getCommentAction(){
    return route('profile.myComment');

}


private function getCourseProgress($courseId,$email)
{
   if(!empty($email)){
     $user = EdxAuthUser::where('email', $email)->firstOrFail();
   }else{
     $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
   }
    $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
    if ($enrollment) {

        $gradeApiObject = $enrollment->getGenCert();
   //var_dump($gradeApiObject);
      if($gradeApiObject[0]->percent >= 0.80){
           $gradeApiObject[0]->letter_grade = 'Completed';
        }elseif($gradeApiObject[0]->percent <= 0.79 && $gradeApiObject[0]->percent >= 0.10){

         $gradeApiObject[0]->letter_grade = 'Completed but Failed';
         }else{
           $gradeApiObject[0]->letter_grade = 'In Progress';
       }

        return [
            'status' => $gradeApiObject[0]->letter_grade,
            'grade' => (float)$gradeApiObject[0]->percent,
        ];
    } else {
        return ['status' => 'Pending', 'grade' => '0'];
    }
}

private function getCourseProgresses($courseId,$email)
{
  $grades=array();
  $max = array();
   if(!empty($email)){
     $user = EdxAuthUser::where('email', $email)->firstOrFail();
   }else{
     $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
   }

     $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseId])->first();
    if ($enrollment) {

        $gradeApiObject = $enrollment->getGenCerts($user->id, $courseId);
       
        


        if($gradeApiObject){
          foreach($gradeApiObject as $data){
             $grades[] = $data->grade;
             $max[] = $data->max_grade;
          }
            
     
          if(array_sum($max) > 0){
            $g = (array_sum($grades)/ array_sum($max));
            $this->grade = ((array_sum($grades)/array_sum($max)) * 100). '%';
          }else{
            $g = 0;
            $this->grade = 0;
          }
            $this->grade = ((array_sum($grades)/ 5) * 100). '%';
            if ($g >= 0.80){
               $status = 'Completed';
            }elseif($g <= 0.79 && $g >= 0.20){
             $status = 'Completed but Failed';
           }else{
              $status = 'In Progress';

           }
        }
      return [
              'status' => $status,
              'grade' => $g,
          ];
    }
     else {
         return ['status' => 'Pending', 'grade' => '0'];
    }
}



}
