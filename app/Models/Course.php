<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //


    protected $fillable = [
        'id',
        'name',
        'short_description',
        'overview',
        'more_info',
        'effort',
        'start',
        'end',
        'enrol_start',
        'enrol_end',
        'price',
        'course_image_uri',
        'course_video_uri',
        'course_category_id',
        'status',
        'slug',
        'order_id',
        'course_video',
        'sessions_id',
    
    ];

   
    //Primary key
    protected $primaryKey = 'id';

    // Tell laravel primary key isn't an integer
    public $incrementing = false;

    //Tell laravel more dates
    protected $dates = ['start', 'end', 'enrol_start', 'enrol_end','created_at','updated_at'];

    
    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute()
    {
        return url('/admin/courses/'.$this->getKey());
    }



    public function Session()
    {
        return $this->hasOne(SessionMeeting::class,  'id' , 'sessions_id');
    }
}
