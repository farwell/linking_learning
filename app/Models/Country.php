<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class Country extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'code',
        'description',
        'position',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    
    public $mediasParams = [
        'cover' => [
            'desktop' => [
                [
                    'name' => 'desktop',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];

    public static function participatingCountries(){

        $pCountries = array(
            array('id' =>'UG', 'name'=>'UGANDA','code'=>'256'),
            array('id' => 'ET', 'name'=>'ETHIOPIA','code'=>'251'),
            array('id' => 'KE', 'name'=>'KENYA','code'=>'254'),
            array('id' => 'SS', 'name'=>'South Sudan','code'=>'211'),
            array('id' => 'SD', 'name'=>'SUDAN','code'=>'249'),
            array('id' => 'SO', 'name'=>'SOMALIA','code'=>'252'),
            array('id' => 'TZ', 'name'=>'TANZANIA','code'=>'255'),
            
           );
           return  $pCountries;
    }
}
