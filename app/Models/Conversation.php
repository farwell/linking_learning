<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $guarded = [];

    public static function createConversation($details = [])
    {
        //$message = self::create($details);
        //dd($details);

        foreach($details['receiver_id'] as $receiver){

            $messages = new Conversation();
            $messages->message = $details['message'];
            $messages->sender_id = $details['sender_id'];
            $messages->images = $details['images'];
            $messages->gifs = $details['gifs'];
            $messages->attachments = $details['attachments'];
            $messages->receiver_id = $receiver;
            $messages->user_message_id = $details['user_message_id'];

            $messages->save();

            $receiver = User::where('id', $messages->receiver_id)->first();
            $sender = User::where('id', $messages->sender_id)->first();
            $message = [
                'type' => Notification::MESSAGE,
                'subject' => 'New Message',
                'model_id' => $messages->id,
                'message' => 'You have recieved a new message from' . $sender->name,
            ];

            $receiver->notify(new \App\Notifications\NewMessageNotification($message));

        }
        

        return $message;
    }


    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }
}
