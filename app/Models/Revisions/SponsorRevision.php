<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SponsorRevision extends Revision
{
    protected $table = "sponsor_revisions";
}
