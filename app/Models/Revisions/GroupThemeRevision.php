<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class GroupThemeRevision extends Revision
{
    protected $table = "group_theme_revisions";
}
