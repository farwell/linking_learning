<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class SessionRevision extends Revision
{
    protected $table = "session_revisions";
}
