<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumReply extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function replyReplies()
    {
        return $this->hasMany(ForumReplyReplies::class)->latest();
    }

    public function replyReactions()
    {
        return $this->hasMany(ForumReplyReaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('images/forum', $fileName);
            return $fileName;
        });
    }
}
