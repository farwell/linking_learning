<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;
use App\Models\GroupType;

class GroupTypeTranslation extends Model
{
    protected $baseModuleModel = GroupType::class;
}
