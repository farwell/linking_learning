<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserActivity extends Model
{
    use SoftDeletes;

    protected $guarded = [];



    public function user()
    {
        return $this->belongsTo(User::class, 'posted_by', 'id');
    }

    public function replies()
    {
        return $this->hasMany(UserActivityReply::class)->latest();
    }

    public function reactions()
    {
        return $this->hasMany(UserActivityReaction::class);
    }

    /**
     * Process files eg. Images & Gif and save them in publick directory
     * @param files $files
     */
    public static function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('images/activity', $fileName);
            return $fileName;
        });
    }
}
