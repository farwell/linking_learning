<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class SessionResourceSlug extends Model
{
    protected $table = "session_resource_slugs";
}
