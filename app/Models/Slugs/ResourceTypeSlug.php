<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class ResourceTypeSlug extends Model
{
    protected $table = "resource_type_slugs";
}
