<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class SessionSlug extends Model
{
    protected $table = "session_slugs";
}
