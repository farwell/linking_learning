<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class SponsorSlug extends Model
{
    protected $table = "sponsor_slugs";
}
