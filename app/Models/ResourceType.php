<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;

class ResourceType extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
    ];

    public $translatedAttributes = [
        'title',
        'description',
        'active',
    ];

    public $slugAttributes = [
        'title',
    ];

    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 16 / 9,
                ],
            ],
            'mobile' => [
                [
                    'name' => 'mobile',
                    'ratio' => 1,
                ],
            ],
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
    ];


    public function groupResource()
    {
        return $this->hasMany(GroupResource::class, 'resource_type_id', 'id')->latest();
    }


    public function getContent()
    {
        $output = '';

        foreach ($this->groupResource as $resource) {
            $output = '<tr>
        <td style="width: 611px;"><strong>' . $resource->title . '</td>
        <td style="width: 683.183px;">' . $resource->description . '</td>';
            if ($resource->resource_type_id == 1) {
                $output .= ' <td style="width: 325.817px;">&nbsp;
            <p><a class="link" href="" target="_blank" rel="noopener"><i class="fa fa-file"></i> Read Article</a></p>
            </td>';
            } else {
                if (!empty($resource->audio_file)) {
                    $link =  asset("Groups/audio_file/" . $resource->audio_file);
                } elseif (!empty($resource->video_file)) {
                    $link =  asset("Groups/video_file/" . $resource->video_file);
                } elseif (!empty($resource->downloable_file)) {
                    $link = asset("Groups/downloable_file/" . $resource->downloable_file);
                }


                $output .= ' <td style="width: 325.817px;">&nbsp;
            <p><a class="link" href="' . $link . '" target="_blank" rel="noopener"><i class="fa fa-download"></i> Download Resource</a></p>
            </td>';
            }

            $output .= '</tr>';
        }


        return $output;
    }


    public function sessionResource()
    {
        return $this->hasMany(SessionResource::class, 'resource_type', 'id')->latest();
    }
    public function getResource()
    {
        $output = '';

        foreach ($this->sessionResource as $resource) {
            // dd(auth()->user()->id);
            $resource->refresh();
            $link =  asset('sessionResources/' . $resource->filename);
            $output .= '<tr class="m-0 p-0">
        <td class="py-0 table-header" style="width: 611px;"><p class="table-header">' . $resource->title . '</td>
        <td class="py-0 table-header" style="width: 683.183px;">' . $resource->description . '</td>';
            if ($resource->resource_type == 1) {
                $output .= ' <td style="width: 325.817px;">&nbsp;
            <p><a class="link table-header" href="' . $link . '" target="_blank" rel="noopener"><i class="fa fa-file"></i> Read</a></p>
            </td>';
            } else {
                if ($resource->resource_type == 2) {
                    $link =  asset('sessionResources/' . $resource->filename);
                } elseif ($resource->resource_type == 3) {
                    $link =  asset('sessionResources/' . $resource->filename);
                } elseif ($resource->resource_type == 4) {
                    $link = asset('sessionResources/' . $resource->filename);
                }


                $output .= ' <td style="width: 325.817px;">&nbsp;
            <p><a class="link table-header" href="' . $link . '" target="_blank" rel="noopener"><i class="fa fa-download"></i>Download</a></p>
            </td>';
            }
            if ($resource->user_id == auth()->user()->id) {
                $output .= '<td class="table-header" style="width: ;"><i class="fas fa-edit"data-toggle="modal"data-id ="' . $resource->id . '" 
                                                            data-target="#practice_modal-' . $resource->id . '" title="Edit"></i></td></tr>';
            } else {
                $output .= '<td class="table-header" style="width: ;"></td></tr>';
            }
        }

        return $output;
    }
}
