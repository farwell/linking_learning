<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCommentReplies extends Model
{
    //

    protected $fillable = [
        'reply', 'comment_id', 'user_id'
    ]; 


    public function user()
    {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }
}
