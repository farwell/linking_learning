<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasFiles;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Behaviors\HasPosition;
use A17\Twill\Models\Behaviors\Sortable;
use A17\Twill\Models\Model;
use willvincent\Rateable\Rateable;


class Resource extends Model implements Sortable
{
    use HasBlocks, HasTranslation, HasSlug, HasMedias, HasFiles, HasRevisions, HasPosition,Rateable;

    protected $fillable = [
        'published',
        'title',
        'description',
        'position',
        'resource_type_id',
        'resource_theme_id',
        'resource_category',
        'resource_country',
        'cover_image',
        'audio_file',
        'video_file',
        'downloable_file',
        'external_link',
    ];
    
    public $translatedAttributes = [
        'title',
        'description',
        'external_text',
        'external_link',
        'active',
    ];
    
    public $slugAttributes = [
        'title',
    ];
    public $filesParams = ['download_resource','resource_audio','resource_video'];
    
    protected $presenter = 'App\Presenters\Front\ResourcePresenter';

    protected $presenterAdmin = 'App\Presenters\Front\ResourcePresenter';

	protected $with = ['translations', 'medias','files'];

    public $mediasParams = [


        'resource_image' => [
            'default' => [
                [
                    'name' => 'landscape',
                    
                ]
            ]
        ]
    ];


    public function type()
    {
        return $this->hasOne(ResourceType::class,'id','resource_type_id');
    }
}
