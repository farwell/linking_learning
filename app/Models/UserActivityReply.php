<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserActivityReply extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function replyReplies()
    {
        return $this->hasMany(UserActivityReplyReplies::class)->latest();
    }

    public function replyReactions()
    {
        return $this->hasMany(UserActivityReplyReaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
