<?php

namespace App\Http\Requests\Front;

use A17\Twill\Http\Requests\Admin\Request;

class RegisterRequest extends Request
{
    public function rulesForCreate()
    {
        return [
          'first_name' => ['required','max:255'],
          'last_name' => ['required'],
          'registration_email' => ['required', 'string', 'email', 'max:255', 'unique:twill_users,email'],
          'mobile_number'=>['required'],
		  'mobile_number_country'=>['required','not_in:0'],
		  'password' => ['required', 'string', 'min:8','confirmed'],
          'register_receive_information' => [],
          'contact_consent' => [],
		 
        ];
    }

    public function rulesForUpdate()
    {
        return [];
    }
}
