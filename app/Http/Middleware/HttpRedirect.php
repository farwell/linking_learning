<?php

namespace App\Http\Middleware;

use Closure;

class HttpRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->secure() && App::environment(['staging', 'production'])) {
            return redirect()->secure($request->getRequestUri(), 301);
    }
        return $next($request);
    }
}
