<?php

namespace App\Http\Controllers\Front;

// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PageRepository;
use App\Repositories\ResourceRepository;
use A17\Twill\Http\Controllers\Front\Controller;
use App\Models\Session as SessionMeeting;
use App\Models\Resource;
use App\Models\SessionResource;
use App\Models\User;
use App\Models\Event;
use App\Models\Blog;
use App\Models\ResourceType;
use App\Models\ResourceTheme;
use Carbon\Carbon;
use Auth;
use Validator;
use Mail;
use Redirect;
use Session;
use App;
use Spatie\GoogleCalendar\Event as Events;
use Monarobase\CountryList\CountryListFacade as CountryList;
use Str;

class PageController extends Controller
{
    //

    protected $pageKey;

    public function __construct()
    {
        $this->homeKey = PageRepository::PAGE_HOMEPAGE;
        $this->aboutKey = PageRepository::PAGE_ABOUT;
        $this->sessionKey = PageRepository::PAGE_SESSION;
        $this->resourceKey = PageRepository::PAGE_RESOURCE;
        $this->eventKey = PageRepository::PAGE_EVENT;
        $this->blogKey = PageRepository::PAGE_BLOG;
        $this->faqsKey = PageRepository::PAGE_FAQS;

        $this->repository = $this->getRepository();

        parent::__construct();
    }

    public function index(Request $request)
    {
       
        $itemPage = app(PageRepository::class)->getPage($this->homeKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

        return view(
            'site.pages.home',
            [
            'pageItem' => $itemPage,

             ]
        );
    }




    public function about(Request $request)
    {
        $itemPage = app(PageRepository::class)->getPage($this->aboutKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

        $upcoming = SessionMeeting::where('start_time', '>=', Carbon::now())->first();
        $previous = SessionMeeting::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->orderBy('position', 'desc')->get();



        return view(
            'site.pages.about',
            [
            'pageItem' => $itemPage,
            'upcoming' => $upcoming,
            'previous' => $previous,

             ]
        );
    }


    public function faqs(Request $request)
    {
        $itemPage = app(PageRepository::class)->getPage($this->faqsKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

        return view(
            'site.pages.faqs',
            ['pageItem' => $itemPage]
        );
    }



    public function sessions(Request $request)
    {
        $itemPage = app(PageRepository::class)->getPage($this->sessionKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

        $upcoming = SessionMeeting::where('start_time', '>=', Carbon::now())->get();
        $previous = SessionMeeting::where('start_time', '<', Carbon::today()->toDateString())->get();

        return view(
            'site.pages.sessions',
            [
            'pageItem' => $itemPage,
            'upcoming' => $upcoming,
            'previous' => $previous,

             ]
        );
    }


    public function sessionDetail(Request $request, $id)
    {
        $sessionResource = SessionResource::all();
        $itemPage = SessionMeeting::where('id', '=', $id)->with('comments.replies')->first();
        $upcoming = SessionMeeting::where('start_time', '>=', Carbon::now())->first();
        $previous = SessionMeeting::where('start_time', '<', Carbon::today()->toDateString())->limit(1)->orderBy('position', 'desc')->get();
        $resources = SessionResource::all();
        $resourceForm = ResourceType::where('id', '!=', 6)->where('id', '!=', 5)->published()->orderBy('position', 'asc')->get();
        $articles = SessionResource::where('resource_type', '==', 2)->get();
        // dd($sessionResource);
        return view(
            'site.pages.session_details',
            [
            'pageItem' => $itemPage,
            'upcoming' => $upcoming,
            'previous' => $previous,
            'resources' => $resources,
            'resourceForm' => $resourceForm,
            'articles' => $articles,
            'sessionResource' => $sessionResource,
            ]
        );
    }

    public function sessionResource(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
            'title' => 'required',
            'description' => 'required',
            'session_id' => 'required',
            'resource_type_id' => 'required',
            'fileUpload' => 'required|mimes:pdf,doc,docx,jpeg,png,jpg,gif,svg,mp3,mpeg,mp4,3gp|max:81920',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        $image = $request->file('fileUpload');
        $FileName = $image->getClientOriginalName();
        $image->move(public_path('sessionResources'), $FileName);
        $imageUpload = new SessionResource();
        $imageUpload->title = $request->input('title');
        $imageUpload->description = $request->input('description');
        $imageUpload->user_id = Auth::user()->id;
        $imageUpload->session_id = $request->input('session_id');
        $imageUpload->filename = $FileName;
        $imageUpload->resource_type = $request->input('resource_type_id');
        $imageUpload->active = 1;
        $imageUpload->published = 1;
        $imageUpload->save();


        $session = SessionMeeting::where('id', '=', $request->input('session_id'))->first();


        $user = User::where('email', '=', $session->host_email)->first();
        $uploader = User::where('id', '=', $imageUpload->user_id)->first();

        // if ($user) {
        //     Mail::send(
        //         'emails.resource_upload_mail',
        //         ['name' => $user->first_name . ' ' . $user->last_name, 'uploader' => $uploader->name, 'topic' => $session->topic, 'url' => route('login')],
        //         function ($message) use ($user) {
        //             $message->from('support@farwell-consultants.com', 'The Challenging Patriarchy Program');
        //             $message->to($user->email, $user->first_name);
        //             $message->subject('The Challenging Patriarchy Program Resource Upload');
        //         }
        //     );
        // }


        return response()->json(['success' => 'Resource has been uploaded successfully.']);
    }
    /**
     * Delete SessionDetail 
     *
     * @param model $userActivity
     */
    public function deleteResource(SessionResource $sessionResource)
    {
        $sessionResource->delete();

        return redirect()->back();
    }
    /**
     * Update SessionDetail
     *
     * @param request $request
     */
    public function updateSession(
        Request $request,
        $id
    ) {
        // dd($request->file('fileUpload'));

        $validator = Validator::make(
            $request->all(),
            [
            'title' => 'required',
            'description' => 'required',
            'session_id' => 'required',
            'resource_type_id' => 'required',
            'fileUpload' => 'required|mimes:pdf,doc,docx,jpeg,png,jpg,gif,svg,mp3,mpeg,mp4,3gp|max:81920',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->all()]);
        }

        // dd($request);
        $resourceDetail = SessionResource::find($id);
        // if ($request->hasFile('fileUploadModal')) {
        $image = $request->file('fileUpload');
        $FileName = $image->getClientOriginalName();
        $image->move(public_path('sessionResources'), $FileName);
        $resourceDetail->filename = $FileName;

        // }
        $resourceDetail->title = $request->input('title');
        $resourceDetail->description = $request->input('description');
        $resourceDetail->resource_type = $request->input('resource_type_id');

        // $resourceDetail->body = $request->input('body');
        $resourceDetail->save();
        return redirect()->back()->with('success', 'Resource has been updated successfully!');
    }
    public function sessionCalendar(Request $request, $id)
    {
        $session = SessionMeeting::where('id', '=', $id)->first();
        $events = Events::get();


        if (!empty($events)) {
            foreach ($events as $even) {
                if ($even->name === $session->topic && $even->startDateTime === Carbon::parse($session->start_time)) {
                    $even->save();
                } else {
                    $event = new Events;
                    $event->name = $session->topic;
                    $event->description =  $session->join_url;
                    $event->startDateTime = Carbon::parse($session->start_time);
                    $event->endDateTime = Carbon::parse($session->start_time)->addMinutes(40);
                    $event->save();
                }
            }
        } else {
            $event = new Events;
            $event->name = $session->topic;
            $event->description =  $session->join_url;
            $event->startDateTime = Carbon::parse($session->start_time);
            $event->endDateTime = Carbon::parse($session->start_time)->addMinutes(40);
            $event->save();
        }


        $events = Events::get();
        dd($events);

        $course_success = [
        'title' => 'Congratulations!',
        'content' => 'You have successfully set the session on your google calendar.',
        ];

        Session::flash('course_success', $course_success);
        return Redirect::back();
    }

    public function resources(Request $request)
    {
        if ($request->isMethod('post')) {
           
            if(empty($request->input('country')) && empty($request->input('theme')) && empty($request->input('uploader')) && empty($request->input('keyword'))){
            
            $articles  = Resource::where('resource_type_id', '=', 1)->published()->orderBy('position', 'desc')->get();
            $videos  = Resource::where('resource_type_id', '=', 3)->published()->orderBy('position', 'desc')->get();
            $audios  = Resource::where('resource_type_id', '=', 2)->published()->orderBy('position', 'desc')->get();
            $webinars  = Resource::where('resource_type_id', '=', 5)->published()->orderBy('position', 'desc')->get();
            $publications  = Resource::where('resource_type_id', '=', 4)->published()->orderBy('position', 'desc')->get();

            $resources_all = Resource::published()->get();


            }
        
            if ($request->input('country')) {
                $country = $request->input('country');
               
                
                $articles  = Resource::where('resource_type_id', '=', 1)->where('resource_country', '=', $country)->published()->orderBy('position', 'desc')->paginate(12);
                $videos  = Resource::where('resource_type_id', '=', 3)->where('resource_country', '=', $country)->published()->orderBy('position', 'desc')->paginate(12);
                $audios  = Resource::where('resource_type_id', '=', 2)->where('resource_country', '=', $country)->published()->orderBy('position', 'desc')->paginate(12);
                $webinars  = Resource::where('resource_type_id', '=', 5)->where('resource_country', '=', $country)->published()->orderBy('position', 'desc')->paginate(12);
                $publications  = Resource::where('resource_type_id', '=', 4)->where('resource_country', '=', $country)->published()->orderBy('position', 'desc')->paginate(12);
                $resources_all = Resource::where('resource_country', '=', $country)->published()->get();
            }  
            if ($request->input('theme')) {
                $theme = $request->input('theme');

                $articles  = Resource::where('resource_type_id', '=', 1)->where('resource_theme_id', '=', $theme)->published()->orderBy('position', 'desc')->paginate(12);
                $videos  = Resource::where('resource_type_id', '=', 3)->where('resource_theme_id', '=', $theme)->published()->orderBy('position', 'desc')->paginate(12);
                $audios  = Resource::where('resource_type_id', '=', 2)->where('resource_theme_id', '=', $theme)->published()->orderBy('position', 'desc')->paginate(12);
                $webinars  = Resource::where('resource_type_id', '=', 5)->where('resource_theme_id', '=', $theme)->published()->orderBy('position', 'desc')->paginate(12);
                $publications  = Resource::where('resource_type_id', '=', 4)->where('resource_theme_id', '=', $theme)->published()->orderBy('position', 'desc')->paginate(12);
                $resources_all = Resource::where('resource_theme_id', '=', $theme)->published()->get();
                
            }
            if ($request->input('uploader')) {
             

                if($request->input('uploader') === '1'){

                $articles  = Resource::where('resource_type_id', '=', 1)->where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->orderBy('position', 'desc')->paginate(12);
                $videos  = Resource::where('resource_type_id', '=', 3)->where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->orderBy('position', 'desc')->paginate(12);
                $audios  = Resource::where('resource_type_id', '=', 2)->where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->orderBy('position', 'desc')->paginate(12);
                $webinars  = Resource::where('resource_type_id', '=', 5)->where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->orderBy('position', 'desc')->paginate(12);
                $publications  = Resource::where('resource_type_id', '=', 4)->where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->orderBy('position', 'desc')->paginate(12);
                $resources_all = Resource::where('resource_category', '=', 'SUPERADMIN')->orWhere('resource_category', '=', 'ADMIN')->published()->get();

                }else{

                    $articles  = Resource::where('resource_type_id', '=', 1)->where('resource_category', '=','Learner')->published()->orderBy('position', 'desc')->paginate(12);
                    $videos  = Resource::where('resource_type_id', '=', 3)->where('resource_category', '=','Learner')->published()->orderBy('position', 'desc')->paginate(12);
                    $audios  = Resource::where('resource_type_id', '=', 2)->where('resource_category', '=','Learner')->published()->orderBy('position', 'desc')->paginate(12);
                    $webinars  = Resource::where('resource_type_id', '=', 5)->where('resource_category', '=','Learner')->published()->orderBy('position', 'desc')->paginate(12);
                    $publications  = Resource::where('resource_type_id', '=', 4)->where('resource_category', '=','Learner')->published()->orderBy('position', 'desc')->paginate(12);
                    $resources_all = Resource::where('resource_category', '=','Learner')->published()->get();

                }
                
            }
            if ($request->input('keyword')) {
                
               $keyword = $request->input('keyword');
            //    dd($keyword);
                $articles  = Resource::where('resource_type_id', '=', 1)->where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->orderBy('position', 'desc')->paginate(12);
                $videos  = Resource::where('resource_type_id', '=', 3)->where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->orderBy('position', 'desc')->paginate(12);
                $audios  = Resource::where('resource_type_id', '=', 2)->where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->orderBy('position', 'desc')->paginate(12);
                $webinars  = Resource::where('resource_type_id', '=', 5)->where(function ($query) use ($keyword){ $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->orderBy('position', 'desc')->paginate(12);
                $publications  = Resource::where('resource_type_id', '=', 4)->where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->orderBy('position', 'desc')->paginate(12);
                $resources_all = Resource::where(function ($query) use ($keyword) { $query->where('title', 'LIKE', '%' .$keyword. '%');})->published()->get();
            }

            $post = 1; 
        } else {
            $articles  = Resource::where('resource_type_id', '=', 1)->published()->orderBy('position', 'desc')->get();
            $videos  = Resource::where('resource_type_id', '=', 3)->published()->orderBy('position', 'desc')->get();
            $audios  = Resource::where('resource_type_id', '=', 2)->published()->orderBy('position', 'desc')->get();
            $webinars  = Resource::where('resource_type_id', '=', 5)->published()->orderBy('position', 'desc')->get();
            $publications  = Resource::where('resource_type_id', '=', 4)->published()->orderBy('position', 'desc')->get();




            $resources_all = Resource::published()->get();

            $post = 0;
        }

        $itemPage = app(PageRepository::class)->getPage($this->resourceKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;

        $resourceType = ResourceType::published()->orderBy('position', 'asc')->get();
        $resourceForm = ResourceType::where('id', '!=', 6)->where('id', '!=', 5)->published()->orderBy('position', 'asc')->get();
        $resourceTheme = ResourceTheme::published()->orderBy('position', 'asc')->get();

        $all_articles = Resource::where('resource_type_id', '=', 1)->get();
        $all_audios = Resource::where('resource_type_id', '=', 2)->get();
        $all_videos = Resource::where('resource_type_id', '=', 3)->get();
        $all_webinars = Resource::where('resource_type_id', '=', 5)->get();
        $all_publications = Resource::where('resource_type_id', '=', 4)->get();


        $start_articles  = Resource::where('resource_type_id', '=', 1)->published()->orderBy('position', 'desc')->paginate(4);
        $start_videos  = Resource::where('resource_type_id', '=', 3)->published()->orderBy('position', 'desc')->paginate(4);
        $start_audios  = Resource::where('resource_type_id', '=', 2)->published()->orderBy('position', 'desc')->paginate(4);
        $start_webinars  = Resource::where('resource_type_id', '=', 5)->published()->orderBy('position', 'desc')->paginate(4);
        $start_publications  = Resource::where('resource_type_id', '=', 4)->published()->orderBy('position', 'desc')->paginate(4);


        $countries = CountryList::getList(app()->getLocale());
         

        
        return view(
            'site.pages.resources',
            [
            'pageItem' => $itemPage,
            'resourceType' => $resourceType,
            'resourceTheme' => $resourceTheme,
            'countries' => $countries,
            'articles' => $articles,
            'videos' => $videos,
            'audios' => $audios,
            'webinars' => $webinars,
            'publications' => $publications,
            'resourceForm' => $resourceForm,
            'all_articles' => $all_articles,
            'all_audios' => $all_audios,
            'all_videos' => $all_videos,
            'all_webinars' => $all_webinars,
            'all_publications' => $all_publications,
            'start_articles' => $start_articles,
            'start_videos' => $start_videos,
            'start_audios' => $start_audios,
            'start_webinars' => $start_webinars,
            'start_publications' => $start_publications,
            'resources_all' => $resources_all,
            'post' => $post,
            ]
        );
    }


    public function participantsUpload(Request $request)
    {
        if ($request->hasFile('resource_image')) {
            $image = $request->file('resource_image');
            $FileName = $image->getClientOriginalName();
            $image->move(public_path('resources/cover_image'), $FileName);
        }

        if ($request->hasFile('publications_resource_image')) {
            $image = $request->file('publications_resource_image');
            $FileName = $image->getClientOriginalName();
            $image->move(public_path('resources/cover_image'), $FileName);
        }

        if ($request->hasFile('video_file')) {
            $image = $request->file('video_file');
            $VideoName = $image->getClientOriginalName();
            $image->move(public_path('resources/video_file'), $VideoName);
        }
        if ($request->hasFile('audio_file')) {
            $image = $request->file('audio_file');
            $AudioName = $image->getClientOriginalName();
            $image->move(public_path('resources/audio_file'), $AudioName);
        }
        if ($request->hasFile('downloable_file')) {
            $image = $request->file('downloable_file');
            $DownloadName = $image->getClientOriginalName();
            $image->move(public_path('resources/downloable_file'), $DownloadName);
        }
        if ($request->input('article_title')) {
            $title = $request->input('article_title');
            $description = $request->input('article_description');
        }

        if ($request->input('audio_title')) {
            $title = $request->input('audio_title');
            $description = $request->input('audio_description');
        }
        if ($request->input('video_title')) {
            $title = $request->input('video_title');
            $description = $request->input('video_description');
        }

        if ($request->input('publication_title')) {
            $title = $request->input('publication_title');
            $description = $request->input('publication_description');
        }

        $resource = new Resource();
        $resource->published = 1;
        $resource->title = $title;
        $resource->description = $description;
        $resource->external_link = $request->input('external_link');
        $resource->resource_type_id = $request->input('resource_type_id');
        $resource->resource_theme_id = $request->input('resource_theme_id');
        $resource->resource_category = $request->input('resource_category');
        $resource->resource_country = $request->input('resource_country');
        $resource->cover_image = (!empty($FileName)) ? $FileName : null;
        $resource->audio_file = (!empty($AudioName)) ? $AudioName : null;
        $resource->video_file = (!empty($VideoName)) ? $VideoName : null;
        $resource->downloable_file = (!empty($DownloadName)) ? $DownloadName : null;
        $resource->active = 1;

        $resource->save();

        // $resource->translations()->create([
        //    'locale'=> substr(rand(),0,5),
        //   'title' => $title,
        //   'description' => $description,
        //   'active'=>1,
        // ]);
        $course_success = [
        'title' => 'Congratulations!',
        'content' => 'You have successfully uploaded a resource.',
        ];

        Session::flash('course_success', $course_success);
        return Redirect::back();
    }


    public function resourcesRating(Request $request)
    {
        request()->validate(['rate' => 'required']);
        $post = Resource::find($request->id);
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = Auth::user()->id;
        $post->ratings()->save($rating);
        return redirect()->route("resources");
    }



    public function resourceDetails(Request $request, $id)
    {
        $itemPage = Resource::where('id', '=', $id)->first();

        return view(
            'site.pages.resources_details',
            [
            'pageItem' => $itemPage,



            ]
        );
    }

    public function resourceArticle($id)
    {
        $itemPage = Resource::where('id', '=', $id)->first();

        return view(
            'site.pages.resources_details',
            [
            'pageItem' => $itemPage,



            ]
        );
    }


    public function events(Request $request)
    {
        if ($request->method() === "POST") {
            if ($request->input('event_filter') === "1") {
                $events = Event::whereMonth('event_date', date('m'))->where('event_date', '>', Carbon::now())->published()->orderBy('event_date','ASC')->paginate(12);

            } elseif($request->input('event_filter') === "2"){

                $events = Event::whereYear('event_date', '=', date('Y'))->whereMonth('event_date','>', date('m'))->published()->orderBy('event_date','ASC')->paginate(12);

            }elseif($request->input('event_filter') === "3"){

                $events = Event::where('event_date', '<', Carbon::now())->published()->orderBy('event_date','DESC')->paginate(12);

            }else {
                $events = Event::paginate(12);
            }
        } else {
            $events = Event::whereYear('event_date', '>=', date('Y'))->whereMonth('event_date','>=', date('m'))->where('event_date', '>', Carbon::now())->published()->orderBy('event_date','ASC')->paginate(12);
        }

        $itemPage = app(PageRepository::class)->getPage($this->eventKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;



        return view(
            'site.pages.events',
            [
            'pageItem' => $itemPage,
            'events' => $events,

            ]
        );
    }


    public function eventDetails(Request $request, $id)
    {
        $itemPage = Event::where('id', '=', $id)->first();

        return view(
            'site.pages.event_details',
            [
            'pageItem' => $itemPage,
             ]
        );
    }



    public function blogs(Request $request)
    {
        $itemPage = app(PageRepository::class)->getPage($this->blogKey);
        $this->seo->title = !empty($title = $itemPage->seo_title) ? $title : "{$itemPage->title}";
        $this->seo->canonical = $itemPage->seo_canonical;
        $this->seo->description = mb_substr($itemPage->present()->seo_description, 0, 255);
        $this->seo->image = $itemPage->present()->imageSeo;
        return view(
            'site.pages.blogs',
            [
            'pageItem' => $itemPage,

             ]
        );
    }



    public function blogDetails(Request $request, $id)
    {
        $itemPage = Blog::where('id', '=', $id)->first();

        $blogs = Blog::published()
            ->orderBy('position', 'desc')->get();

        return view(
            'site.pages.blog_details',
            [
            'pageItem' => $itemPage,
            'blogs' => $blogs
            ]
        );
    }


    /**
     * Upload Blog
     *
     * @param request $request
     */
    public function uploadBlog(
        Request $request
    ) {
        // dd($request);
        if ($request->hasFile('blog_image')) {
            $image = $request->file('blog_image');
            $FileName = $image->getClientOriginalName();
            $image->move(public_path('resources/blog_image'), $FileName);
        }

        $blog = new Blog();
        $blog->images = (!empty($FileName)) ? $FileName : null;
        $blog->title = $request->input('title');
        $blog->description = $request->input('description');
   

        $blog->save();
        return redirect()->back()->with('success', 'Blog has been uploaded successfully!');
    }

    protected function getRepository()
    {
        return App::make("App\\Repositories\\ResourceRepository");
    }


    public function ckUpload(Request $request){
        if($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();
       
            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
       
            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();
       
            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;
       
            //Upload File
            $request->file('upload')->storeAs('public/uploads', $filenametostore);
  
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/'.$filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
              
            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }
    }
}
