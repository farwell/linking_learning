<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\UserActivity;
use App\Models\UserActivityReaction;
use App\Models\UserActivityReply;
use App\Models\UserActivityReplyReaction;
use App\Models\UserActivityReplyReplies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserActivityReactionsController extends Controller
{
    public function reaction(Request $request, $userActivityId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = UserActivityReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'user_activity_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => UserActivityReaction::REACTION,
                    'dislikes' => 0
                ]);
            }

        } elseif (!empty($request->data == 'dislike')) {
            $reactions = UserActivityReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'user_activity_id' => $userActivityId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => UserActivityReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => UserActivityReaction::where('user_activity_id', $userActivityId)->get()->sum('likes'),
            'dislikes' => UserActivityReaction::where('user_activity_id', $userActivityId)->get()->sum('dislikes')
        ];
    }

    public function replyOnActionReply(Request $request, UserActivity $userActivity, UserActivityReply $userActivityReply)
    {
        $images = $request->file('images');
        $gifs = $request->file('gifs');

        $imagesToSave = $this->saveFiles($images);
        $gifsToSave = $this->saveFiles($gifs);

        $reply = new UserActivityReplyReplies([
            'user_activity_id' => $userActivity->id,
            'user_activity_reply_id' => $userActivityReply->id,
            'user_id' => Auth::id(),
            'comment' => $request->input('comment'),
            'images' => json_encode($imagesToSave),
            'gifs' => json_encode($gifsToSave)
        ]);

        $reply->save();

        return redirect()->back();
    }

    public function reactOnReply(Request $request, $userActivityId, $userActivityReplyId)
    {
        if (!empty($request->data == 'like')) {
            $reactions = UserActivityReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'user_activity_id' => $userActivityId,
                'user_activity_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => UserActivityReaction::REACTION,
                    'dislikes' => 0
                ]);
            }
        } elseif (!empty($request->data == 'dislike')) {
            $reactions = UserActivityReplyReaction::firstOrCreate([
                'user_id' => Auth::id(),
                'user_activity_id' => $userActivityId,
                'user_activity_reply_id' => $userActivityReplyId
            ]);
            if ($reactions->save()) {
                $reactions->update([
                    'likes' => 0,
                    'dislikes' => UserActivityReaction::REACTION
                ]);
            }
        }

        return [
            'likes' => UserActivityReplyReaction::where('user_activity_id', $userActivityId)
                    ->where('user_activity_reply_id', $userActivityReplyId)->get()->sum('likes'),
            'dislikes' => UserActivityReplyReaction::where('user_activity_id', $userActivityId)
                    ->where('user_activity_reply_id', $userActivityReplyId)->get()->sum('dislikes')
        ];
    }

    /**
     * Process files eg. Images & Gif and save them in publick directory
     * @param files $files
     */
    public function saveFiles($files)
    {
        return collect($files)->map(function ($file) {
            $fileName = uniqid().$file->getClientOriginalName();
            $fileName = strtolower(str_replace(' ', '_', $fileName));
            $file->move('images/activity', $fileName);
            return $fileName;
        });
    }

}
