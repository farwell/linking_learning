<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\CreateMessageRequest;
use App\Models\Conversation;
use App\Models\BulkMessage;
use App\Models\User;
use App\Models\UserActivity;
use App\Models\UserMessages;
use App\User as AppUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserMessagesController extends Controller
{
    /**
     * Get all user messages
     */
    public function index()
    {
        $messages = UserMessages::allMessages()->get();
        // dd($messages);
        $received = UserMessages::allMessages()
            ->with(['sender', 'conversation'])->latest()->get()->groupBy(
                function ($query) {
                    return $query->created_at->format('Y-m-d');
                }
            );
            $user= User::all();
            $admin = $user[0]['id'];
        $fields = [Auth::id(), $admin];
        $users = User::where('id','!=',1)->orderBy('name')->published()->get();
        

        return view('site.pages.connect.messages', ['messages' => $messages, 'received' => $received, 'users' => $users]);
    }

    /**
     * Delete a message
     */

    public function deleteMessage($id)
    {
    
        $messages = UserMessages::where('id', $id)->firstorfail()->delete();

        return redirect()->back()->withSuccess('Message deleted successfully');
    }
    public function deleteConvo($id)
    {

        $messages = Conversation::where('id', $id)->firstorfail()->delete();

        return redirect()->back()->withSuccess('Message deleted successfully');
    }
    /**
     * Create/Post a message
     */
    public function sentMessage(CreateMessageRequest $request)
    {
       
        $users = User::all();
      
    //dd($request);
        $files = [
            'images' => json_encode(UserActivity::saveFiles($request->file('images'))),
            'gifs' => json_encode(UserActivity::saveFiles($request->file('gifs'))),
            'attachments' => json_encode(UserActivity::saveFiles($request->file('attachments')))
        ];
     
        if (empty($request->input('user_message_id'))) {
            $senderId = ['sender_id' => Auth::id()];

     

             UserMessages::createMessage(array_merge($request->validated(), $senderId, $files));
        } else {
            $senderId = [
                'sender_id' => Auth::id(),
                'user_message_id' => $request->input('user_message_id')
            ];

            Conversation::createConversation(array_merge($request->validated(), $senderId, $files));
        }


        return redirect()->back()->withSuccess('Message created successfully');
    }

    /**
     * Get User Chat
     */
    public function getUserChat(User $user)
    {
        $messages = UserMessages::where('sender_id', $user->id)
            ->with('conversation')->get();

        return $messages;
    }

    /**
     * Search for user
     */
    public function searchForUser(Request $request)
    {
        return User::filterUsers($request->input('name'))->get();
    }
}
