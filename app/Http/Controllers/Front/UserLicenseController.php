<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserLicense;
use App\Models\Course;
use App\Models\MyCommunity;
use App\Models\User;
use App\Models\TrainingComment;
use App\Models\TrainingLike;
use App\Models\Country;
use Auth;
use Redirect;
use Session;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\Response;
use PDF;
use A17\Twill\Models\Enums\UserRole;
use DB;
use Monarobase\CountryList\CountryListFacade as CountryList;
use PragmaRX\Countries\Package\Countries;
use PragmaRX\Countries\Package\Services\Config;
use App\Models\Projecttopic;

class UserLicenseController extends Controller
{
    //
    public $data ;
    public $country;
    public function __construct()
    {
        $this->data = [
            'ug'=>  '',
            'ng'=>  '',
            'st'=>  '',
            'tz'=>  '',
            'sl'=>  '',
            'gw'=>  '',
            'cv'=>  '',
            'sc'=>  '',
            'tn'=>  '',
            'mg'=>  '',
            'ke'=>  '',
            'cd'=>  '',
            // 'fr'=>  '',
            'mr'=>  '',
            'dz'=>  '',
            'er'=>  '',
            'gq'=>  '',
            'mu'=>  '',
            'sn'=>  '',
            'km'=>  '',
            'et'=>  '',
            'ci'=>  '',
            'gh'=>  '',
            'zm'=>  '',
            'na'=>  '',
            'rw'=>  '',
            'sx'=>  '',
            'so'=>  '',
            'cm'=>  '',
            'cg'=>  '',
            'eh'=>  '',
            'bj'=>  '',
            'bf'=>  '',
            'tg'=>  '',
            'ne'=>  '',
            'ly'=>  '',
            'lr'=>  '',
            'mw'=>  '',
            'gm'=>  '',
            'td'=>  '',
            'ga'=>  '',
            'dj'=>  '',
            'bi'=>  '',
            'ao'=>  '',
            'gn'=>  '',
            'zw'=>  '',
            'za'=>  '',
            'mz'=>  '',
            'sz'=>  '',
            'ml'=>  '',
            'bw'=>  '',
            'sd'=>  '',
            'ma'=>  '',
            'eg'=>  '',
            'ls'=>  '',
            'ss'=>  '',
            'cf'=>  '',
            'is'=>  '',
        ];
    }




    public function profile()
    {
        // if (!isset($_COOKIE['edinstancexid'])) {
        //     Auth::logout();
        //     return false;
        // }

        $courses = Course::orderBy('name')->get();

        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }
        //$licensed = UserLicense::where('user_id', Auth::user()->id)->delete();
        $licensed = UserLicense::where('user_id', Auth::user()->id)->get();
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $upcoming = [];
        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }
            if (!empty($_GET['year'])) {
                if ($license->created_at->year != $_GET['year']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                if ($license->created_at->month != $_GET['month']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }
        //dd($upcoming);
        return view('site.pages.user_licenses.courses', ['licenses' => $licenses, 'coursess' => $courses, 'upcoming' => $upcoming]);
    }
    /**
     * Display a listing of achievements by the profile
     *
     * @return \Illuminate\Http\Response
     */
    public function achievements()
    {
        $future_courses = Course::where('start', '>', Carbon::today()->toDateString())->get();
        $coursesid = [];
        foreach ($future_courses as $id => $course) {
            $coursesid[] = $course->id;
        }
        $licensed = UserLicense::where('user_id', Auth::user()->id)->get();
        $licenseid = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->course->id)) {
                $licenseid[] = $license->course->id;
            } else {
                unset($licensed[$key]);
            }
        }

        $result = array_diff($licenseid, $coursesid);

        $licenses = [];

        foreach ($licensed as $key => $license) {
            if (in_array($license->course->id, $result)) {
                $licenses[] = $license;
            } else {
                $upcoming[] = $license;
            }

            if (!empty($_GET['year'])) {
                if ($license->created_at->year != $_GET['year']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['month'])) {
                if ($license->created_at->month != $_GET['month']) {
                    unset($licenses[$key]);
                }
            }

            if (!empty($_GET['status'])) {
                if ($license->status != $_GET['status']) {
                    unset($licenses[$key]);
                }
            }
        }

        return view('site.pages.user_licenses.achievements', ['licenses' => $licenses]);
    }


    public function community()
    {
        $pc = [];
        // (new \App\Helpers\Front)->getCountriesList() as $key => $value
        $countries = Country::participatingCountries();

        foreach ($countries as $country) {
            $pc[] = $country['id'];
         }

        $participants =User::where('published','=',1)->where('id','!=',1)->where('id','!=',13)->get();
        
        
        $issues = Projecttopic::all();
        $countriesArray=array();
        foreach ($this->data as $key => $value) {
            foreach ((new \App\Helpers\Front())->getCountriesList() as $keys => $country) {
                if ($key == strtolower($keys)) {
                    $users = User::where('phone_locale', '=', $keys)->where('published','=',1)->where('id','!=',1)->where('id','!=',13)->get();
                    $countryFlag = Countries::where('postal', $keys)->first();
                    if (!empty($countryFlag->flag)) {
                        $country_flag =$countryFlag->flag['svg_path'];
                    } else {
                        $country_flag = "";
                    }


                    $flag = substr($country_flag, strpos($country_flag, "data") + 5);



                    // $this->data[$key] = $country->common_name;
                    $this->data[$key] = ['common_name'=> $country['name'], 'users' => count($users),'country_flag' => $flag];
                }
            }
        }

        $data = $this->data;
        //  dd($data);
        return view('site.pages.connect.community_map', compact('data', 'participants', 'countries', 'issues'));
    }

    public function communityParticipants()
    { 
        $data = [];
        $pc = [];

        $countries = Country::participatingCountries();
         
        foreach ($countries as $country) {
           $pc[] = strtolower($country['id']);
        }
     
       
        $countries =  DB::table('twill_users')->groupBy('phone_locale')->where('published','=',1)->where('id','!=',1)->where('id','!=', 13)->get();

        foreach ($countries as $country) {
            if(in_array(strtolower($country->phone_locale), $pc)){

                array_push($data, strtolower($country->phone_locale));
            }
        }

        return $data;
    }


    public function communityCountries()
    {
        $data = [];

        $countries = Country::participatingCountries();

        foreach ($countries as $country) {
            array_push($data, strtolower($country['id']));
        }
      
        return $data;
    }
    public function communityCountry($name)
    {
       if($name ==="COTE D IVOIRE"){
           $name = 'Côte d’Ivoire';
       }
        foreach ((new \App\Helpers\Front())->getCountriesList() as $key => $country) {
           
            if ($country['name'] == $name) {
                
                $this->country = $key ;
            }
        }
        // dd(Countries::all());
         if($this->country === "CD"){
             $this->country = "DRC";
         }elseif($this->country === "SZ"){

            $this->country = "SW";
         }
          //dd($this->country);
        // "adm0_a3" => "KEN"
        $countryFlag = Countries::where('postal',  $this->country)->first();
       

        //  dd($countryFlag);


        $country_flag =$countryFlag->flag['svg_path'];

        

        $flag = substr($country_flag, strpos($country_flag, "data") + 5);



        $licensed = User::where('phone_locale', '=', $this->country)->where('published','=',1)->where('id','!=','1')->where('id','!=','13')->get();

        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        $receive = count($req);

        return view('site.pages.connect.community_country_users', ['licenses' => $licensed,'receive' => $receive, 'name'=>$name, 'flag'=>$flag]);

     
    }

    public function members()
    {

        
        //$countryFlag = Countries::where('postal',  $this->country)->first();

        $licensed = User::where('published','=',1)->where('id','!=',1)->where('id','!=',13)->get();

        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        $receive = count($req);

        return view('site.pages.connect.all_members', ['licenses' => $licensed,'receive' => $receive]);

     
    }

    public function myFollow($follow, $id)
    {
        $status = MyCommunity::where('user_id', Auth::user()->id)->where('follow_id', $follow)->first();

        if ($status && $id == 0) {
            $follows = MyCommunity::where(['id' => $status->id])->first();
            $follows->user_id = Auth::user()->id;
            $follows->follow_id = $follow;
            $follows->status = 1;
            $follows->update();
        } elseif ($status && $id == 1) {
            $follo = MyCommunity::where(['id' => $status->id])->first();
            $follo->user_id = Auth::user()->id;
            $follo->follow_id = $follow;
            $follo->status = 2;
            $follo->update();
        } elseif ($status && $id == 2) {
            $foll = MyCommunity::where(['id' => $status->id])->first();
            $foll->user_id = Auth::user()->id;
            $foll->follow_id = $follow;
            $foll->status = 0;
            $foll->update();
        } elseif ($status && $id == 3) {
            $fol = MyCommunity::where(['id' => $status->id])->first();
            $fol->user_id = Auth::user()->id;
            $fol->follow_id = $follow;
            $fol->status = 3;
            $fol->update();
        } elseif ($status && $id == 4) {
            $fo = MyCommunity::where(['id' => $status->id])->first();
            $fo->user_id = Auth::user()->id;
            $fo->follow_id = $follow;
            $fo->status = 0;
            $fo->update();
        } else {
            $f = new MyCommunity();
            $f->user_id = Auth::user()->id;
            $f->follow_id = $follow;
            $f->status = 1;
            $f->save();
        }

        return Redirect::back();
    }


    public function myFollowRequests()
    {
        $details = [];
        $i = 0;
        $req = MyCommunity::where('follow_id', Auth::user()->id)->where('status', 1)->get();

        foreach ($req as $request) {
            $user = User::where('id', $request->user_id)->first();

            if (isset($user->id)) {
                $details[$i]['id'] = $request->id;
                $details[$i]['user_id'] = $user->id;
                $details[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
                $i++;
            }
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }


    public function myFollowAccept($id, $status)
    {
        $following = MyCommunity::where('id', $id)->where('status', 1)->first();

        $following->status = $status;
        $following->update();

        if ($status == 2) {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for accepting follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        } else {
            $redirectMessage = [
               'title' => 'Success',
               'content' => 'Thank you for responding follow request.',
           ];

            Session::flash('course_success', $redirectMessage);
        }


        return Redirect::back();
    }


    public function trainingFeed()
    {
        $following = MyCommunity::where('user_id', Auth::user()->id)->where('status', 2)->get();

        $folo = [];

        foreach ($following as $f) {
            $folo [] = $f->follow_id;
        }

        $licensed = UserLicense::all();

        $licenses = [];
        foreach ($licensed as $key => $license) {
            if (isset($license->user->id)) {
                if (in_array($license->user->id, $folo)) {
                    $licenses[] = $license;
                }
                if ($license->user->id == Auth::user()->id) {
                    unset($licenses[$key]);
                }
                if (!empty($_GET['year'])) {
                    if ($license->created_at->year != $_GET['year']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['month'])) {
                    if ($license->created_at->month != $_GET['month']) {
                        unset($licenses[$key]);
                    }
                }

                if (!empty($_GET['status'])) {
                    if ($license->status != $_GET['status']) {
                        unset($licenses[$key]);
                    }
                }
            } else {
                unset($licensed[$key]);
            }
        }


        return view('site.pages.user_licenses.trainingfeed', ['licenses' => $licenses]);
    }




    public function myComment(Request $request)
    {
        if ($request->input('unique')) {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->where('parent_id', $request->input('unique'))->first();
        } else {
            $comment = TrainingComment::where('user_id', Auth::user()->id)->where('comment_id', $request->input('follow'))->where('course_id', $request->input('course'))->first();
        }

        if ($comment) {
            $comments = TrainingComment::where('id', $comment->id)->first();
            $comments->user_id = Auth::user()->id;
            $comments->comment_id = $request->input('follow');
            $comments->course_id = $request->input('course');
            $comments->parent_id = $request->input('unique');
            $comments->comment = $request->input('comment');
            $comments->update();
        } else {
            $commen = new TrainingComment();
            $commen->user_id = Auth::user()->id;
            $commen->comment_id = $request->input('follow');
            $commen->course_id = $request->input('course');
            $commen->parent_id = $request->input('unique');
            $commen->comment = $request->input('comment');
            $commen->save();
        }

        $site_success = [
        'title' => '',
        'content' => 'Thank you for keeping the conversation going!',
    ];

        Session::flash('redirectMessage', $site_success);

        return Redirect::route('profile.trainingFeed');
    }

    public function myLike($follow, $id)
    {
        $likes = TrainingLike::where('user_id', Auth::user()->id)->where('like_id', $follow)->where('course_id', $id)->first();

        if ($likes) {
            $redirectMessage = [
            'title' => 'Thank you for like',
            'content' => 'You can only like a feed once.',
        ];

            Session::flash('redirectMessage', $redirectMessage);
            return Redirect::route('profile.trainingFeed');
        } else {
            $like = new TrainingLike();
            $like->user_id = Auth::user()->id;
            $like->like_id = $follow;
            $like->course_id = $id;
            $like->save();
        }

        return Redirect::route('profile.trainingFeed');
    }


    public function mycomments($follow, $course)
    {
        $details = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->whereNull('parent_id')->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $details[$i]['id'] = $comment->id;
            $details[$i]['user_id'] = $user->id;
            $details[$i]['course_id'] = $comment->course_id;
            $details[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
            $details[$i]['position'] = $user->affiliation;
            $details[$i]['course_name'] = $course->name;
            $details[$i]['comment'] = $comment->comment;
            $details[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $i++;
        }
        return response()->json(['details' => $details], Response::HTTP_OK);
    }

    public function subCommentCount($follow, $course, $id)
    {
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();
        $count = count($comments);
        return response()->json(['count' => $count], Response::HTTP_OK);
    }


    public function subComment($follow, $course, $id)
    {
        $detail = [];
        $i = 0;
        $comments = TrainingComment::where('comment_id', $follow)->where('course_id', $course)->where('parent_id', $id)->get();

        foreach ($comments as $comment) {
            $user = User::where('id', $comment->user_id)->first();
            $course = Course::where('id', $comment->course_id)->first();

            $detail[$i]['id'] = $comment->id;
            $detail[$i]['user_id'] = $user->id;
            $detail[$i]['course_id'] = $comment->course_id;
            $detail[$i]['name'] = ucwords($user->first_name.' '.$user->last_name);
            $detail[$i]['position'] = $user->affiliation;
            $detail[$i]['course_name'] = $course->name;
            $detail[$i]['comment'] = $comment->comment;
            $detail[$i]['created_at'] = $comment->created_at->format('M d Y H:i:s');
            $i++;
        }
        return response()->json(['subdetails' => $detail], Response::HTTP_OK);
    }



    public function evaluate($id)
    {
    }

    public function certificate($courseid)
    {
        // $user = EdxAuthUser::where('email', Auth::user()->email)->firstOrFail();
        $users = User::where('email', Auth::user()->email)->firstOrFail();
        // $enrollment = StudentCourseEnrollment::where(['user_id' => $user->id, 'course_id' => $courseid])->first();
        // $course = Course::findOrFail($enrollment->course_id);
        $name = ucfirst($users->first_name).' '.ucfirst($users->last_name);
        $date = Carbon::today()->format('d-m-Y');

        $data = [
        'name' => $name,
        'date' => $date,
        'course_name' => 'Test Course',
         ];

        //  return view('site.pages.user_licenses.certificate', compact('data'))


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('site.pages.user_licenses.certificate', $data)->setPaper('a4', 'landscape');





        //return $pdf->download('Certificate_'.$name.'.pdf');
        return $pdf->stream();

        //return  $pdf->stream();
    }
}
