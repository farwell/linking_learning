<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Front\RegisterRequest;
use App\Http\Requests\Front\VerifyRequest;
use App\Http\Requests\Front\LoginRequest;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Helpers\Front;
use App\Models\TwillUsersAuth;
use App\Models\User;
use App\Models\Projecttopic;
use App\Models\ProjectUser;
use Carbon\Carbon;
use AfricasTalking\SDK\AfricasTalking;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Redirect;

use App\Edx\EdxAuthUser;

use Auth;
use Toastr;
use Session;
use Mail;
use App;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->repository = $this->getRepository();
    }
    

    /**
     * @param RegisterRequest $request
     */

    public function showRegistrationForm()
    {
        $request->session()->invalidate();

        $request->session()->regenerateToken();
        return view('site.auth.register');
    }


    public function create(RegisterRequest $request)
    {
        $code = array();
        $data=$request->validated();

        $user = $this->repository->createForRegister($data);
    
        $edxResponse =  $this->edxRegister($user);
        if ($edxResponse !== true) {
            $user->delete();
            return Redirect::back()->with('register_error', 'An error occured while submitting your registration. Please refresh the page and try again.');
        }
        
        $validation = $this->validationToken($user, $length = 6);
        $a = Front::getCountriesList();
        foreach ($a as $key => $value) {
            if ($key === $user->phone_locale) {
                $code[] = $value['code'];
            }
        }

        $phone_number = $code[0].''.$user->phone;
         
        $username   = "linklearning";
        $apiKey     = "ccf31a95a727a1cb5c91ca077c20fa36cd8573d4459d4379509bea6cce405179";

        $AT       = new AfricasTalking($username, $apiKey);
        // Get one of the services
        $sms      = $AT->sms();
        $text = 'Hello , Use this code to verify your account '. $validation->token.' ';

        // Use the service
        $result   = $sms->send([
              'to'      => $phone_number,
              'message' => $text,
              
            ]
        );
         
        Mail::send(
            'emails.token',
            ['name' => $user->first_name.' '.$user->last_name,  'code' => $validation->token ],
            function ($message) use ($user) {
                  $message->from('support@farwell-consultants.com', 'Welcome to The Challenging Patriarchy Program');
                  $message->to($user->email, $user->first_name);
                  $message->subject('The Challenging Patriarchy Program Account Verification');
            }
        );


        $userData = [
      'otp_requested'    => true,
            'otp_for_user' => $user->email,
            'user_id' => $user->id,
        ];
        //dd($user, $user->profile, $token, $notification);
        session($userData);

        return redirect('/user/verify');
    }

    /**
     * Verify Form
     */

    public function verifyForm()
    {
        if (request()->session()->get('otp_requested')==null) {
            abort(403, 'You are not allowed on this page');
        }
        $user_id = request()->session()->get('user_id');
        
        return view('site.auth.verify', [
            'user_id' => $user_id
        ]);
    }


    /**
      * @param VerifyRequest $request
      */
    public function verify(VerifyRequest $request)
    {
        $data = $request->validated();
        // $user = $this->repository->createForValidation($data);
        $verify = TwillUsersAuth::where('user_id', $data['user'])->where('token', $data['token'])->first();

        if (!empty($verify)) {
            $user = User::where('id', $verify->user_id)->first();

            if (!$user) {
                $course_error = [
                    'title' => 'Error!',
                    'content' => 'The user account does not exsist please register on the platform',
                ];
                Session::flash('course_error', $course_error);
                return redirect('/');
            }else{
              
                if(!empty($user->activated_at)){

                    $course_success = [
                        'title' => 'Account Activated!',
                        'content' => 'You have successfully activated your account. 
                         Please note the programme facilitator will need to publish the account before you can login.
                         A notification will be sent to your registration phone number and email.',
                    ];
                  
                      Session::flash('course_success', $course_success);

                      return redirect('/');

                }

            }

    $edxuser = EdxAuthUser::where('username', $user->username)->first();


    if ($edxuser === null || $edxuser->authRegistration->activation_key === null) {
      
     return redirect()->route('back')->with('login_error','There was a problem updating your account. Please try again later or report to support');
   }
   //Update edxuser names and active
   $edxuser->first_name = $user->first_name;
   $edxuser->last_name = $user->last_name;
   $edxuser->email = $user->email;
   $edxuser->is_active =  1;
   $edxuser->save();

   $user->activated_at = Carbon::now();
   $user->save();

    // session()->forget('otp_requested');
	// 	session()->forget('otp_for_user');

    $course_success = [
      'title' => 'Congratulations!',
      'content' => 'You have successfully activated your account. 
       Please note the programme facilitator will need to publish the account before you can login.
       A notification will be sent to your registration phone number and email.',
  ];

  
  
    Session::flash('course_success', $course_success);
       return redirect()->back();


    }else{
        $course_error = [
            'title' => 'Error!',
            'content' => 'The verification code used does not exsist please proceed to the registration page and request for a new code',
        ];
        Session::flash('course_error', $course_error);
        return redirect('/');

    }

    }

    public function resendCode(Request $request)
    {
        return view('site.auth.resend');
    }


    public function resendStore(Request $request)
    {
        $user = User::where('email', $request->input('email'))->first();

        if ($user) {
            if ($user->published != 1) {
                $verify = TwillUsersAuth::where('user_id', $user->id)->first();

                $a = Front::getCountriesList();
                foreach ($a as $key => $value) {
                    if ($key === $user->phone_locale) {
                        $code[] = $value['code'];
                    }
                }

                $phone_number = $code[0].''.$user->phone;
        
                $username   = "linklearning";
                $apiKey     = "ccf31a95a727a1cb5c91ca077c20fa36cd8573d4459d4379509bea6cce405179";

                $AT       = new AfricasTalking($username, $apiKey);
                // Get one of the services
                $sms      = $AT->sms();
                $text = 'Hello , Use this code to verify your account '. $verify->token.' ';

                // Use the service
                $result   = $sms->send([
                      'to'      => $phone_number,
                      'message' => $text,
                      
                    ]
                );
        
                Mail::send(
                    'emails.token',
                    ['name' => $user->first_name.' '.$user->last_name,  'code' => $verify->token ],
                    function ($message) use ($user) {
                        $message->from('support@farwell-consultants.com', 'Welcome to The Challenging Patriarchy Program');
                        $message->to($user->email, $user->first_name);
                        $message->subject('The Challenging Patriarchy Program Account Verification');
                    }
                );


                $userData = [
                'otp_requested'    => true,
                'otp_for_user' => $user->email,
                'user_id' => $user->id,
              ];
                //dd($user, $user->profile, $token, $notification);
                session($userData);

                return redirect('/user/verify');
            } else {
                return Redirect::route('login')->with('login_success', 'The account is activated please proceed and login');
            }
        } else {
            return Redirect::back()->with('login_error', 'The email you entered did not match our records. Please confirm you have registered');
        }




       
        $verify = UserVerify::where('user_id', $id)->first();
      

        if ($verify->status === "0") {
            $profile = Profile::where('user_id', $id)->first();
            $user = User::where('id', $id)->first();

            $phone = str_replace('+', '', $profile->phone_number);

            $vcode = 'Verification Code '.$verify->verification_code;


            $response = Curl::to('https://api.uwaziimobile.com/api/v2/SendSMS')
                ->asJson()
                ->withHeaders(array( 'Content-Type' => 'application/json', 'Accept' => 'application/json' ))
                ->withData(
                    [
                    'ApiKey'=> 'YpKiA4e/7vchsEHf8/0pzKOJb2QBG22wJ2JL8ONe8cs=',
                    'ClientId'=> 'e37751e3-010c-41cd-881d-bf097629c477',
                    'Message'=> $vcode,
                    'MobileNumbers'=> $phone,
                    "SenderId"=> "InukaMSME"]
                )
                ->post();
  
            Mail::send(
                'emails.verification_resend',
                ['name' => $profile->name.' '.$profile->lastname, 'code' => $verify->verification_code, 'username'=> $user->username, 'url' => route('site-account_verify', $user->auth_key)],
                function ($message) use ($profile, $user) {
                $message->from('support@farwell-consultants.com', 'Welcome to InukaMSME');
                $message->to($user->email, $profile->name);
                $message->subject('The InukaMSME Platform Account Verification');
                    }
            );
  
            Auth::logout();
            return redirect()->route("site-account_verify", [$user->auth_key])
                ->with('modalMsg', true)
                ->with('modalType', 'account_resend');
        } else {
            return redirect()->route("site-register")
                ->with('verified', true)
                ->with('modalMsg', true)
                ->with('modalType', 'confirm_failed')
                ->with("message", "This Account has been verified, Please contact support to assist! ");
        }
    }




    public function profile(Request $request)
    {
        //Get user
        $user = Auth::user();
  
        //dd($user);
        if ($request->isMethod('post')) {
  
        //Validate
            $request->validate(
                [
                'first_name' => ['required', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:twill_users,email,' . $user->id]
                ]
            );

            if ($request->input('topics')) {
                foreach ($request->input('topics') as $topic) {
                    $project = new ProjectUser();
                    $project->twill_user_id = $user->id;
                    $project->project_topic_id = $topic;
                    $project->save();
                }
            }
  
            //Get updates
            $updates = $request->all();

  
            //Save changes
            $user = User::find($user->id);
            if ($user->update($updates)) {
                return Redirect::back()->with('register_success', 'Your profile has been successfully updated.');
            } else {
                //Show session message
  
                return Redirect::back()->with('register_error', 'There was a problem updating your profile.Please refresh the page and try again.');
            }
        }

        $project_topics = projecttopic::all();
  
  
        if (strpos(Auth::user()->email, 'noemail')) {
            $user->email = '';
        }
  
        return view('site.pages.user.profile', ['user' => $user,'project_topics'=>$project_topics]);
    }


    public function picture(Request $request)
    {
        if (!Auth::check()) {
            return response('Unauthorized', 403);
        }

        $user = Auth::user();

        // //Save to storage
        // $ppic = $request->file('ppic');
        // $extension = $ppic->getClientOriginalExtension();
        // $filename = $ppic->getFilename() . '.' . $extension;

    
        // Storage::disk('public')->put($filename,  File::get($ppic));

        // //Delete old file
        // Storage::disk('public')->delete($user->profile_pic);

        //Save to user
        $user->profile_pic = $this->Fileupload($request, 'ppic', 'uploads');
        if ($user->save()) {
            return Redirect::back()->with('register_success', 'Your profile photo has been successfully updated.');
        } else {
            return Redirect::back()->with('register_error', 'Profile photo updating failed. Please refresh the page and try again.');
        }
    }

    

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * @return \A17\Twill\Repositories\ModuleRepository
     */
    protected function getRepository()
    {
        return App::make("App\\Repositories\\UserRepository");
    }

    protected function validationToken($user, $length)
    {
        $validation = new TwillUsersAuth();
        $validation->user_id  = $user->id;
        $validation->token = $this->generateRandomString($length);
        $validation->save();


        return $validation;
    }


    public function Fileupload($request, $file = 'file', $folder = null)
    {
        $upload = null;
        //create folder if none
        if (!File::exists($folder)) {
            File::makeDirectory($folder);
        }
        //chec fo file
        if (!is_null($request->file($file))) {
            $extension = $request->file($file)->getClientOriginalExtension();
            $fileName = rand(11111, 99999).uniqid().'.' . $extension;
            $upload = $request->file($file)->move($folder, $fileName);
            if ($upload) {
                $upload = $fileName;
            } else {
                $upload = null;
            }
        }
        return $upload;
    }
    private function edxRegister($user)
    {

        $configLms = config()->get("settings.lms.live");
        $configApp = config()->get("settings.app");

        //Validate user
        if ($user === null) {
            return;
        }
        //Package data to be sent
        if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
            $email = $user->email;
        } else {
            $email = $user->email.'@local.com';
        }
        $data = [
          'email' => $email,
          'name' => $user->first_name . ' ' . $user->last_name,
          'username' => $user->username,
          'honor_code' => 'true',
          'password' => request()->get('password'),
          'country'=> 'KE',
          'terms_of_service'=> 'true',
        ];
    
        $headers = array(
          'Content-Type'=>'application/x-www-form-urlencoded',
          'cache-control' => 'no-cache',
          'Referer'=> $configApp['APP_URL'].'/register',
        );
    
        $client = new \GuzzleHttp\Client();
    
        try {
            $response = $client->request('POST', $configLms['LMS_REGISTRATION_URL'], [
                'form_params' => $data,
                'headers'=>$headers,
                ]
            );
    
            return true;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseJson = $e->getResponse();
            $response = json_decode($responseJson->getBody()->getContents(), true);
            //Error, delete user
            $user->delete();
    
            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            $errors = [];
            foreach ($response as $key => $error) {
                //Return error
                $errors[] = $error;
            }
            //echo "CATCH 1";
            //dd($errors);
            return $errors[0];
        } catch (\Exception $e) {
    
            //Error, delete user
            $user->delete();
            //Delete password resets
            //PasswordReset::where('email', '=', $user->email)->delete();
            //echo "CATCH 2";
            //echo $e->getMessage();
            //die;
            return $e->getMessage();
        }
    }

    protected function generateRandomString($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }

        return $randomString;
    }
}
