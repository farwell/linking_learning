<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\PasswordReset;
use Mail;
use Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;



    public function sendResetLinkEmail(Request $request){
    
        $user = User::where('email', $request->input('email'))->first();
        
        $token = self::generateRandomString(16);

        $reset = new PasswordReset();
        $reset->email = $user->email;
        $reset->token = $token;
        $reset->save();
       

        Mail::send('emails.reset',['name' => $user->first_name.' '.$user->last_name, 'username' => $user->username, 'url' => route('password.reset', $reset->token)],
        function ($message) use ($user) {
          $message->from('support@farwell-consultants.com', 'The Challenging Patriarchy Program');
          $message->to($user->email, $user->first_name);
          $message->subject('The Challenging Patriarchy Program Reset Password');
        });
       
        return back()->with('status', 'An email has been sent to your email address. Please follow the instructions in the email to reset your password.If you do not see the email in your inbox, please check your junk, spam or promotions folder.');
    }



    




protected function generateRandomString($length = 10)
{
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $charactersLength = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[ rand(0, $charactersLength - 1) ];
  }

  return $randomString;
}
}
