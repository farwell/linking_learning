<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class ResourceTypeController extends ModuleController
{
    protected $moduleName = 'resourceTypes';
    
}
