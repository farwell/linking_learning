<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GroupThemeController extends ModuleController
{
    protected $moduleName = 'groupThemes';
    
}
