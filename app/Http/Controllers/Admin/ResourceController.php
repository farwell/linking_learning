<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\ResourceTypeRepository;
use App\Repositories\ResourceThemeRepository;
use Monarobase\CountryList\CountryListFacade as CountryList;
use App\Support\Constants;

class ResourceController extends ModuleController
{
    protected $moduleName = 'resources';

    protected $indexWith = ['type'];


    protected $indexColumns = [
        
        'title' => [ // field column
            'title' => 'Title',
            'field' => 'title',
        ],
        
        'resource_type_id' => [ // relation column
            'title' => 'Resource Type',
            'sort' => true,
            'relationship' => 'type',
            'field' => 'title'
        ],
        'created_at' => [
            'title' => 'Uploaded On',
            'field' => 'created_at',
            'present' => true, 
        ],
    ];


    protected function formData($request)
    {

    
        return [
			'resourceTypeList' => app(ResourceTypeRepository::class)->listAll('title'),
			'resourceThemeList' => app(ResourceThemeRepository::class)->listAll('title'),
            'countryList' => CountryList::getList(app()->getLocale())
        ];

	}


    
}
