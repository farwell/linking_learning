<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;
use App\Repositories\GroupTypeRepository;
use App\Repositories\GroupFormatRepository;
use App\Repositories\GroupThemeRepository;
use App\Repositories\UserRepository;
use App\Models\Group;
use App\Models\User;
use Auth;

class GroupController extends ModuleController
{
    protected $moduleName = 'groups';


    protected function formData($request)
    {

    
        return [
			'groupTypeList' => app(GroupTypeRepository::class)->listAll('title'),
            'groupFormatList' => app(GroupFormatRepository::class)->listAll('title'),
            'groupThemeList' => app(GroupThemeRepository::class)->listAll('title'),
            'users' => app(UserRepository::class)->listAll('name'),
			
        ];

	}

    
}
