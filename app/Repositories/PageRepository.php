<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Page;

class PageRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;
    const PAGE_HOMEPAGE = 'home';
    const PAGE_ABOUT = 'about';
    const PAGE_SESSION = 'sessions';
    const PAGE_RESOURCE = 'resources';
    const PAGE_EVENT = 'events';
    const PAGE_BLOG = 'blogs';
    const PAGE_FAQS = 'faqs';

    protected $defaultTitle = [
        self::PAGE_HOMEPAGE => 'Home',
        self::PAGE_ABOUT => 'About',
        self::PAGE_SESSION => 'Sessions',
        self::PAGE_RESOURCE => 'Resources',
        self::PAGE_EVENT => 'Events',
        self::PAGE_BLOG => 'Blogs',
        self::PAGE_FAQS => 'FAQs',

    ];
    public function __construct(Page $model)
    {
        $this->model = $model;
    }
    public function getPage($key, $with = [])
    {
        return $this->getByKey($key, $with);
    }
    public function getByKey($key, $with = [])
    {
        if (
            ($page = $this->model
                ->with($with)
                ->where('key', $key)
                ->first()) == null
        ) {
            if (isset($this->defaultTitle[$key])) {
                $title = $this->defaultTitle[$key];
            } else {
                $title = 'Untitled';
            }

            $page = Page::create([
                'key' => $key,
                'title' => $title,
                'published'=>1,
                'active' => 1
            ]);
        }
        return $page;
    }
}
