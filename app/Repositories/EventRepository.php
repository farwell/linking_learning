<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleFiles;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Event;
use DB;
use Mail;
use Illuminate\Support\Arr;
use App\Models\User;

class EventRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleFiles, HandleRevisions;

    public function __construct(Event $model)
    {
        $this->model = $model;
    }


  /**
     * @param mixed $id
     * @param array $fields
     * @return void
     */
    public function update($id, $fields)
    {
        //dd($fields['cmsSaveType']);

        if($fields['cmsSaveType'] === "save"){
            DB::transaction(function () use ($id, $fields) {
                $object = $this->model->findOrFail($id);
    
                $this->beforeSave($object, $fields);
    
                $fields = $this->prepareFieldsBeforeSave($object, $fields);
    
                $object->fill(Arr::except($fields, $this->getReservedFields()));
    
                $object->save();
    
                $this->afterSave($object, $fields);
            }, 3);

        }elseif($fields['cmsSaveType'] === "publish"){
            DB::transaction(function () use ($id, $fields) {
                $object = $this->model->findOrFail($id);
    
                $this->beforeSave($object, $fields);
    
                $fields = $this->prepareFieldsBeforeSave($object, $fields);
    
                $object->fill(Arr::except($fields, $this->getReservedFields()));
    
                $object->save();
    
                $this->afterSave($object, $fields);
            }, 3);

            
        $users = User::where('published','=', 1)->where('id', '=',1)->get();

          foreach($users as $user){
            Mail::send('emails.event_notification', ['name' => $user->first_name.' '.$user->last_name, 'topic'=> $fields['title']['en'], 'url' => route('login') ],
            function ($message) use ($user) {
                  $message->from('support@farwell-consultants.com', 'The Challenging Patriarchy Program');
                  $message->to($user->email, $user->first_name);
                  $message->subject('The Challenging Patriarchy Program Event Notification');
            }
             );
          }

        
        }
       
    }

    /**
     * @param mixed $id
     * @param array $values
     * @param array $scopes
     * @return mixed
     */
    public function updateBasic($id, $values, $scopes = [])
    {
        return DB::transaction(function () use ($id, $values, $scopes) {
            // apply scopes if no id provided
            if (is_null($id)) {
                $query = $this->model->query();

                foreach ($scopes as $column => $value) {
                    $query->where($column, $value);
                }

                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            // apply to all ids if array of ids provided
            if (is_array($id)) {
                $query = $this->model->whereIn('id', $id);
                $query->update($values);

                $query->get()->each(function ($object) use ($values) {
                    $this->afterUpdateBasic($object, $values);
                });

                return true;
            }

            if (($object = $this->model->find($id)) != null) {
                $object->update($values);
                $this->afterUpdateBasic($object, $values);
                return true;
            }

            return false;
        }, 3);
    }
}
