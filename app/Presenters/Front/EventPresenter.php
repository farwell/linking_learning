<?php

namespace App\Presenters\Front;

class EventPresenter extends BasePresenter
{
    public function day()
    {
        return $this->entity->event_date != null ? $this->entity->event_date->formatLocalized("%e") : "";
	}

    public function month()
    {
        return $this->entity->event_date != null ? $this->entity->event_date->formatLocalized("%B %Y") : "";
	}


    public function date()
    {
        return $this->entity->event_date != null ? $this->entity->event_date->formatLocalized("%B %e, %Y") : "";
	}

    public function time()
    {
        return $this->entity->event_time != null ? $this->entity->event_time->formatLocalized("%I:%M:%S %p") : "";
	}
}