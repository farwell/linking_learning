function readFile(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      e.preventDefault();
      e.stopPropagation();
      var htmlPreview =
        '<p>' + input.files[0].name + '</p>';
      var wrapperZone = $(input).parent();
      var previewZone = $(input).parent().parent().find('.preview-zone');
      var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

      wrapperZone.removeClass('dragover');
      previewZone.removeClass('hidden');
      boxZone.empty();
      boxZone.append(htmlPreview);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function reset(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

$(".dropzone").change(function () {
  readFile(this);
});

$('.dropzone-wrapper').on('dragover', function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).addClass('dragover');
});

$('.dropzone-wrapper').on('dragleave', function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).removeClass('dragover');
});

$('.remove-preview').on('click', function () {
  var boxZone = $(this).parents('.preview-zone').find('.box-body');
  var previewZone = $(this).parents('.preview-zone');
  var dropzone = $(this).parents('.form-group').find('.dropzone');
  boxZone.empty();
  previewZone.addClass('hidden');
  reset(dropzone);
});


$(".fa-edit").on("click", function () {
  console.log($(this).attr("data-id"));
var  id = $(this).attr("data-id");


function readFiles(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      e.preventDefault();
      e.stopPropagation();
      var htmlPreview =
        '<p>' + input.files[0].name + '</p>';
      var wrapperZone = $(input).parent();
      // var previewZone = $(input).parent().parent().find('.preview-zone-modal');
      var previewZone = $(input).parent().parent().find('#preview-zone-modal-' + id);
      console.log(previewZone)
      // var boxZone = $(input).parent().parent().find('.preview-zone-modal').find('.box-modal').find('.box-body-modal');
      var boxZone = $(input).parent().parent().find('.preview-zone-modal').find('.box-modal').find('#box-'+id);
      // console.log(boxZone);
      // console.log(box2);
      wrapperZone.removeClass('dragover');
      previewZone.removeClass('hidden');
      boxZone.empty();
      boxZone.append(htmlPreview);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function reset(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

$(".dropzone-modal-"+id).change(function () {
  readFiles(this);
});

$('#dropzone-wrapper-modal-'+id).on('dragover', function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).addClass('dragover');
});

$('#dropzone-wrapper-modal-'+id).on('dragleave', function (e) {
  e.preventDefault();
  e.stopPropagation();
  $(this).removeClass('dragover');
});

$('#remove-preview-modal-'+id).on('click', function () {
  var boxZone = $(this).parents('.preview-zone-modal').find('.box-body-modal');
  var previewZone = $(this).parents('.preview-zone-modal');
  var dropzone = $(this).parents('.form-group').find('.dropzone-modal');
  boxZone.empty();
  previewZone.addClass('hidden');
  reset(dropzone);
});
});