<?php

return [
    'app' => [
        'APP_DOMAIN' => 'advocacyaccelerator.org',
        'VERIFY_SSL' => true,
        'APP_URL'=>'challengingpatriarchy.advocacyaccelerator.org',
        'APP_NAME'=>"The Challenging Patriarchy Program"
    ],

    'lms' => [
        'status' => 'test',
        'live' => [
            'EDX_KEY' => 'd2af15cf46d10f6be4ce',
            'EDX_SECRET' => '6e59d9b83569bd5904c4124238aaf9fe4e430b84',
            'EDX_CONNECT' => true,
            'MICROSITE_BASE'=>'challengingpatriarchy.advocacyaccelerator.org',
            'MICROSITE_URL'=> 'https://challengingpatriarchy.advocacyaccelerator.org',
            'LMS_BASE' => 'https://courses.advocacyaccelerator.org',
            'CMS_BASE' => 'https://studio.advocacyaccelerator.org',
            'LMS_REGISTRATION_URL' => 'https://courses.advocacyaccelerator.org/user_api/v1/account/registration/',
            'LMS_LOGIN_URL' => 'https://courses.advocacyaccelerator.org/user_api/v1/account/login_session/',
            'LMS_RESET_PASSWORD_PAGE' => 'https://courses.advocacyaccelerator.org/user_api/v1/account/password_reset/',
            'LMS_RESET_PASSWORD_API_URL' => 'https://courses.advocacyaccelerator.org/user_api/v1/account/password_reset/',
            'edxWebServiceApiToken' => 'c90685ce8bc75a8e03ad35deb28a5fade4a4cc87',
            'default_token' => '133ad7f0ecd269b63637a522dbb529c50475a493'
        ],
        
    ],

    'zoom' =>[
        'ZOOM_API_URL'=>"https://api.zoom.us/v2/",
        'ZOOM_API_KEY'=>"2AO2mlY2RhaLBtGt5ev7A",
        'ZOOM_API_SECRET'=>"X886m1dXCeUrV8KSaXFXY0P33EU7XB7E",

    ],

    'fix_page_css' => [
        'case-study-index'
    ],


];
