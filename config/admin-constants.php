<?php

return [

  /**
   * |Navbar for hr admin
   */
 'company_admin_nav'=>[
    'Navigation'=>[
      ['title'=>'Back to Website','icon'=>'<i class="fas fa-undo-alt"></i>','route'=>'home'],
      // ['title'=>'Back to Profile','icon'=>'<i class="fas fa-undo-alt"></i>','route'=>'profile.edit'],
    ],
    'Groups'=>[
      ['title'=>'Groups','icon'=>'<i class="fas fa-users"></i>','route'=>'group.index'],
    ],
    
    'Sessions'=>[
      ['title'=>'Sessions','icon'=>'<i class="fas fa-users"></i>','route'=>'sessions.index'],
      ['title'=>'Sessions Resources','icon'=>'<i class="fas fa-users"></i>','route'=>'sessionsResource.index'],
    ]
    
    
  ],
  /*
    Let's create the nav structure of the admin section.
  */
  // 'admin_nav' => [
  //   'Navigation'=>[
  //     ['title'=>'Back to Website','icon'=>'<i class="fas fa-undo-alt"></i>','route'=>'home'],
  //     // ['title'=>'Back to Profile','icon'=>'<i class="fas fa-undo-alt"></i>','route'=>'profile.edit'],
  //   ],
  //   'Courses'=>[
  //     ['title'=>'Course Categories','icon'=>'<i class="fas fa-book"></i>','route'=>'courseCategories.index'],
  //     ['title'=>'Courses','icon'=>'<i class="fas fa-book"></i>','route'=>'course.index'],
  //     ['title'=>'Course Reports','icon'=>'<i class="fas fa-chart-line"></i>','route'=>'user_licenses.reports'],
  //   ],
  //   'FAQ'=>[
  //     ['title'=>'Categories','icon'=>'<i class="fas fa-grip-horizontal"></i>','route'=>'faq_categories.index'],
  //     ['title'=>'Questions','icon'=>'<i class="fas fa-question-circle"></i>','route'=>'faqs.index'],
  //   ],
  //   // 'Transactions'=>[
  //   //   ['title'=>'Paypal','icon'=>'<i class="fab fa-cc-paypal"></i>','route'=>'paypal_transactions.index'],
  //   // ],
  //   'Corporate'=>[
  //     ['title'=>'Companies','icon'=>'<i class="fas fa-building"></i>','route'=>'companies.index'],
  //     ['title'=>'Company Invoices','icon'=>'<i class="fas fa-file-invoice-dollar"></i>','route'=>'invoices.index'],
  //     // ['title'=>'Job Groups','icon'=>'<i class="fas fa-users"></i>','route'=>'job_groups.index'],
  //   ],

  //   'Licenses'=>[
  //     ['title'=>'User','icon'=>'<i class="fas fa-id-badge"></i>','route'=>'user_licenses.index'],
  //     ['title'=>'Company Licenses','icon'=>'<i class="fas fa-book"></i>','route'=>'company_licenses.index'],
  //   ],
  //   // 'Enquiries'=>[
  //   //   ['title'=>'General','icon'=>'<i class="fas fa-envelope"></i>','route'=>'general_messages.index'],
  //   //   ['title'=>'Institution','icon'=>'<i class="far fa-envelope"></i>','route'=>'institution_messages.index'],
  //   // ],

  //   'Evaluation'=>[
  //     ['title'=>'Evaluation Feedback','icon'=>'<i class="fas fa-envelope"></i>','route'=>'user.evaluation'],
  //     ['title'=>'Evaluation Options','icon'=>'<i class="fas fa-envelope"></i>','route'=>'evaluation_options'],
  //     ['title'=>'Evaluation Sections','icon'=>'<i class="fas fa-envelope"></i>','route'=>'evaluation_sections'],
  //     ['title'=>'Evaluation Questions','icon'=>'<i class="fas fa-envelope"></i>','route'=>'evaluation_forms'],
  //   ],

  //   'General'=>[
  //     ['title'=>'Users','icon'=>'<i class="fas fa-users"></i>','route'=>'users.index'],
  //   ],

  //   'Training Groups'=>[
  //     ['title'=>'Training Group','icon'=>'<i class="fas fa-users"></i>','route'=>'trainingGroups.index'],
  //   ]
  // ],

  /*
    Theme specific styles
  */
  'styles'=>[
  	//Global Mandatory Vendors
  	"dashboard/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css",
  	//Global Theme Styles(used by all pages)
  	"dashboard/assets/demo/default/base/style.bundle.css",
  	//Layout Skins(used by all pages)
  	"dashboard/assets/demo/default/skins/header/base/light.css",
  	"dashboard/assets/demo/default/skins/header/menu/light.css",
  	"dashboard/assets/demo/default/skins/brand/dark.css",
  	"dashboard/assets/demo/default/skins/aside/dark.css",
    "dashboard/assets/vendors/custom/datatables/datatables.bundle.min.css",
  ],

  /*
    Theme specific js
  */
  'js'=>[
    //Global Mandatory Vendors
    "dashboard/assets/vendors/general/jquery/dist/jquery.js",
    "dashboard/assets/vendors/general/popper.js/dist/umd/popper.js",
    "dashboard/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js",
    "dashboard/assets/vendors/general/js-cookie/src/js.cookie.js",
    "dashboard/assets/vendors/general/moment/min/moment.min.js",
    "dashboard/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js",
    "dashboard/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js",
    "dashboard/assets/vendors/general/sticky-js/dist/sticky.min.js",
    "dashboard/assets/vendors/general/wnumb/wNumb.js",
    "dashboard/assets/vendors/custom/datatables/datatables.bundle.js",
    //Global Theme Bundle(used by all pages)
    "dashboard/assets/demo/default/base/scripts.bundle.js",
    //Global App Bundle(used by all pages)
    "dashboard/assets/app/bundle/app.bundle.js",

  ],

];
